package com.epam.newsmanagement.command;

import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.domain.ExtendedNews;
import com.epam.newsmanagement.domain.SearchCriteria;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.ExtendedNewsService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.Timestamp;

class PostCommentCommand implements Command {

    public String execute(HttpServletRequest request) throws ServiceException {
        ExtendedNewsService extendedNewsService = (ExtendedNewsService) context.getBean("extendedNewsService");
        CommentService commentService = (CommentService) context.getBean("commentService");
        NewsService newsService = (NewsService) context.getBean("newsService");

        Long newsId = ((ExtendedNews) request.getSession(true).getAttribute("news")).getNews().getId();
        commentService.create(new Comment(0L, newsId, request.getParameter("commentText"),
                new Timestamp(new java.util.Date().getTime())));

        ExtendedNews news = extendedNewsService.findById(newsId);

        HttpSession session = request.getSession(true);

        request.getSession(true).setAttribute("news", news);

        SearchCriteria searchCriteria = (SearchCriteria) session.getAttribute("searchCriteria");
        if (searchCriteria == null) {
            searchCriteria = new SearchCriteria();
            session.setAttribute("searchCriteria", searchCriteria);
        }
        session.setAttribute("searchCriteria", searchCriteria);
        Long prevNewsId = newsService.findPrev(newsId, searchCriteria);
        Long nextNewsId = newsService.findNext(newsId, searchCriteria);
        if (newsService.findById(prevNewsId) != null) {
            request.setAttribute("prevNewsId", prevNewsId);
        }
        if (newsService.findById(nextNewsId) != null) {
            request.setAttribute("nextNewsId", nextNewsId);
        }

        return request.getContextPath() + "/news-client?command=shownews&newsId="+newsId;
    }

    @Override
    public boolean needsRedirect() {
        return true;
    }
}
