<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <fmt:setLocale value="${sessionScope.lang}" scope="session"/>
    <fmt:setBundle basename="label"/>

    <link rel="stylesheet"
          type="text/css"
          href="<c:url value="/resources/css/ui.dropdownchecklist.standalone.css" />"/>

    <link rel="stylesheet" type="text/css"
          href="<c:url value="/resources/css/styles.css" />"/>

    <script type="text/javascript"
            src="<c:url value="/resources/js/jquery.min.js" />">
    </script>
    <script
            type="text/javascript"
            src="<c:url value="/resources/js/jquery-ui.js" />">
    </script>
    <script type="text/javascript"
            src="<c:url value="/resources/js/ui.dropdownchecklist-1.4-min.js" />">
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#multi-select").dropdownchecklist(
                    {
                        width: 200,
                        maxDropHeight: 100,
                        emptyText: "<fmt:message key="news_list.please_select_tags" />"
                    }
            );
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#" + 'page' + ${page}).attr('style', 'background: #363636;border-color: #363636;color:white;');
        });
    </script>

    <title>News Portal</title>
</head>
<body>
<jsp:include page="header.jsp"/>
<table border="1" class="layout-table"
       style="margin-top: 5%;margin-bottom:1.9%;">
    <tr class="body-tr">
        <td class="body-td">
            <div class="content">
                <form action="news-client" method="POST">
                    <input type="hidden" name="command" value="filter">
                    <table class="filter-table">
                        <tr>
                            <td>
                                <select value="${searchCriteria.authorId}"
                                        name="authorId">
                                    <option value="0">
                                        <fmt:message
                                                key="news_list.please_select_author"/>
                                    </option>
                                    <c:forEach var="authorItem"
                                               items="${authorList}">
                                        <option value="<c:out value="${authorItem.id}" />"
                                                <c:if test="${authorItem.id == searchCriteria.authorId}">
                                                    selected
                                                </c:if>
                                        >
                                            <c:out value="${authorItem.name}"/>
                                        </option>
                                    </c:forEach>
                                </select>
                            </td>
                            <td>
                                <select id="multi-select" name="tagIds"
                                        multiple="multiple">
                                    <c:forEach var="tagItem" items="${tagList}">
                                        <option value="<c:out value="${tagItem.id}" />"
                                                <c:forEach
                                                        items="${searchCriteria.tagsId}"
                                                        var="id">
                                                    <c:if test="${tagItem.id == id}">selected</c:if>
                                                </c:forEach>
                                        >
                                            <c:out value="${tagItem.name}"/>
                                        </option>
                                    </c:forEach>
                                </select>
                            </td>
                            <td>
                            <td>
                                <input type="submit"
                                       value="<fmt:message key="news_list.filter" />">
                            </td>
                            <td>
                                <input type="submit"
                                       name="reset"
                                       value="<fmt:message key="news-list.reset"/>"
                                       form="resetForm"
                                       formaction="news-client"/>
                            </td>
                        </tr>
                    </table>
                </form>
                <form id="resetForm" action="/news-client" method="POST" style="display: inline-block;">
                    <input type="hidden" name="command" value="reset">
                </form>

                <table class="news-table">
                    <tbody>
                    <c:forEach var="news" items="${newsList}">
                        <tr>
                            <td>
                                <div class="news-in-table">
                                    <span class="news-header">
                                        <span class="title">
                                            <c:out value="${news.news.title}"/>
                                        </span>
                                        <span class="date">
                                            <c:set var="format">
                                                <fmt:message key="date.pattern"/>
                                            </c:set>
                                            <fmt:formatDate
                                                    value="${news.news.modificationDate}"
                                                    var="modificationDateString"
                                                    pattern="${format}"/>
                                            <c:out value="${modificationDateString}"/>
                                        </span>
                                        <span class="author">
                                            (<fmt:message key="news_list.by"/> <c:out
                                                value="${news.author.name}"/>)
                                        </span>
                                    </span>
                                    <br/>
                                    <span class="news-text">
                                        <c:out value="${news.news.shortText}"/>
                                    </span>
                                    <div class="news-info">
                                        <span class="tags">
                                            <c:forEach var="tagItem"
                                                       items="${news.tags}"
                                                       varStatus="loop">
                                                <c:out value="${tagItem.name}"/><c:if
                                                    test="${!loop.last}">, </c:if>
                                            </c:forEach>
                                        </span>
                                        <span class="comments">
                                            <fmt:message key="news_list.comments"/>:
                                            (<c:out
                                                value="${fn:length(news.comments)}"/>)
                                        </span>
                                        <form action="news-client" style="display: inline-block;" class="edit-link">
                                        <input type="hidden" name="command" value="shownews">
                                        <button type="submit"
                                                class="submitLink"
                                                name="newsId"
                                                value="<c:out value="${news.news.id}" />" >
                                            <fmt:message key="news_list.view"/>
                                        </button>
                                    </form>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
            <div class="pagination">
                <span class="helper-inline"></span>
                <form action="/news-client" style="display: inline-block;">
                    <input type="hidden" name="command" value="shownewslist">
                    <c:forEach var="count" begin="1" end="${countPages}">
                        <button type="submit"
                                id="page${count}"
                                name="page"
                                value="${count}"
                                class="page-button">${count}</button>
                    </c:forEach>
                </form>
            </div>
        </td>
    </tr>
</table>
<jsp:include page="footer.jsp"/>
</body>
</html>