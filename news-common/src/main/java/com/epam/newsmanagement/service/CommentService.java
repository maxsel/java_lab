package com.epam.newsmanagement.service;

import com.epam.newsmanagement.domain.Comment;

import java.util.List;

/**
 * Service interface with methods
 * encapsulating business logic concerned with {@link Comment}s
 * and transaction management.
 */
@SuppressWarnings("unused")
public interface CommentService {

    /**
     * Creates new comment.
     *
     * @param comment comment to create.
     * @return id of created comment.
     * @throws ServiceException if an error occurred when performing operation
     */
    Long create(Comment comment) throws ServiceException;

    /**
     * Edits existing comment by id.
     *
     * @param comment comment to update.
     * @throws ServiceException if an error occurred when performing operation
     */
    void update(Comment comment) throws ServiceException;

    /**
     * Deletes comment by id.
     *
     * @param id id of comment to delete.
     * @throws ServiceException if an error occurred when performing operation
     */
    void delete(Long id) throws ServiceException;

    /**
     * Returns all comments for the {@link com.epam.newsmanagement.domain.News}
     * with the given id.
     *
     * @param newsId id of news.
     * @return comment of the news with the given id.
     * @throws ServiceException if an error occurred when performing operation
     */
    List<Comment> findByNewsId(Long newsId) throws ServiceException;

    /**
     * Retrieves an comment with the given id.
     *
     * @param id id of comment to retrieve.
     * @return an comment with the given id.
     * @throws ServiceException if an error occurred when performing operation
     */
    Comment findById(Long id) throws ServiceException;
}
