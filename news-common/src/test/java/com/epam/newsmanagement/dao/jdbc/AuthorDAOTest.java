package com.epam.newsmanagement.dao.jdbc;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.dao.DAOException;
import com.epam.newsmanagement.domain.Author;
import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.annotation.ExpectedDataSet;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.unitils.spring.annotation.SpringBeanByType;

import static org.junit.Assert.assertEquals;

@DataSet("authordao/dataset.xml")
@SpringApplicationContext("test-context.xml")
public class AuthorDAOTest extends UnitilsJUnit4 {

    @SpringBeanByType
    private AuthorDAO authorDAO;

    @Test
    @ExpectedDataSet("authordao/dataset-expected-insert.xml")
    public void testInsert() throws DAOException {
        Author author = new Author(0L, "New author", null);
        authorDAO.insert(author);
    }

    @Test(expected=DAOException.class)
    public void testInsertWithEmptyName() throws DAOException {
        Author author = new Author(0L, "", null);
        authorDAO.insert(author);
    }

    @Test
    @ExpectedDataSet("authordao/dataset-expected-update.xml")
    public void testUpdate() throws DAOException {
        Author author = authorDAO.findById(1L);
        author.setName("Updated");
        authorDAO.update(author);
    }

    @Test(expected=DAOException.class)
    public void testUpdateWithEmptyName() throws DAOException {
        Author author = authorDAO.findById(1L);
        author.setName("");
        authorDAO.update(author);
    }

    @Test
    @ExpectedDataSet("authordao/dataset-expected-delete1.xml")
    public void testDelete() throws DAOException {
        authorDAO.delete(3L);
    }

    @Test
    @ExpectedDataSet("authordao/dataset-expected-delete2.xml")
    public void testDeleteWithUnlinking() throws DAOException {
        authorDAO.unlink(1L);
        authorDAO.delete(1L);
    }

    @Test(expected = DAOException.class)
    public void testDeleteWithConstraintViolation() throws DAOException {
        authorDAO.delete(1L);
    }

    @Test
    public void testFindAll() throws DAOException {
        assertEquals(3, authorDAO.findAll().size());
    }

    @Test
    public void testFindById() throws DAOException {
        assertEquals("Author 1", authorDAO.findById(1L).getName());
    }

    @Test
    public void testFindByNewsId() throws DAOException {
        assertEquals("Author 1", authorDAO.findByNewsId(1L).getName());
    }
}
