package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.inject.Inject;
import java.sql.Timestamp;

@Controller
@RequestMapping("/admin/news/{newsId}/comments")
public class CommentController {
    private static final Logger LOG = LogManager.getLogger(CommentController.class);

    private final CommentService commentService;

    @Inject
    public CommentController(CommentService commentService) {
        Assert.notNull(commentService, "CommentService must be not null!");
        this.commentService = commentService;
    }

    @RequestMapping("/post")
    public String post(@PathVariable("newsId") Long newsId,
                       @RequestParam("commentText") String commentText) {
        try {
            Timestamp now = new Timestamp(System.currentTimeMillis());
            Comment comment = new Comment(0L, newsId, commentText, now);
            commentService.create(comment);
        } catch (ServiceException e) {
            LOG.error(e);
        }
        return String.format("redirect:/admin/news/%d/view", newsId);
    }

    @RequestMapping("/{commentId}/delete")
    public String delete(@PathVariable("newsId") Long newsId,
                         @PathVariable("commentId") Long commentId) {
        try {
            commentService.delete(commentId);
        } catch (ServiceException e) {
            LOG.error(e);
        }
        return String.format("redirect:/admin/news/%d/view", newsId);
    }
}
