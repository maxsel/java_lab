package com.epam.newsmanagement.editor;

import com.epam.newsmanagement.domain.Tag;

import java.beans.PropertyEditorSupport;

public class TagEditor extends PropertyEditorSupport {

    // Converts a String to a Tag (when submitting form)
    @Override
    public void setAsText(String text) {
        Tag tag = new Tag();
        tag.setId(Long.valueOf(text));
        this.setValue(tag);
    }

    // Converts a Tag to a String (when displaying form)
    @Override
    public String getAsText() {
        Tag tag = (Tag) this.getValue();
        return tag == null ? "" : tag.toString();
    }
}