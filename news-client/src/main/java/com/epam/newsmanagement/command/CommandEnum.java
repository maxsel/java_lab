package com.epam.newsmanagement.command;

enum CommandEnum {
    EMPTY {
        {
            command = new EmptyCommand();
        }
    },


    SHOWNEWSLIST {
        {
            command = new ShowNewsListCommand();
        }
    },

    SHOWNEWS {
        {
            command = new ShowNewsCommand();
        }
    },

    POSTCOMMENT {
        {
            command = new PostCommentCommand();
        }
    },

    FILTER {
        {
            command = new FilterCommand();
        }
    },

    RESET {
        {
            command = new ResetCommand();
        }
    },

    CHANGELANGUAGE {
        {
            command = new ChangeLanguageCommand();
        }
    };

    protected Command command;

    public Command getCommand() {
        return command;
    }
}
