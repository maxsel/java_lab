package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.DAOException;
import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.service.ServiceException;
import com.epam.newsmanagement.service.TagService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings("Duplicates")
public class TagServiceImplTest {
    @Mock
    private TagDAO tagDAO;

    private TagService tagService;

    @Before
    public void init() {
        tagService = new TagServiceImpl(tagDAO);
    }

    @Test
    public void testCreate() throws Exception {
        Tag tag = new Tag(1L, "Test tag");
        when(tagDAO.insert(tag)).thenReturn(1L);
        assertTrue(tagService.create(tag) == 1L);
        verify(tagDAO, times(1)).insert(tag);
    }

    @Test(expected = ServiceException.class)
    @SuppressWarnings("unchecked")
    public void testCreateWithException() throws Exception {
        Tag testTag = new Tag(1L, "");
        when(tagDAO.insert(testTag)).thenThrow(DAOException.class);
        tagService.create(testTag);
        verify(tagDAO, times(1)).insert(testTag);
        verifyNoMoreInteractions(tagDAO);
    }

    @Test
    public void testUpdate() throws Exception {
        Tag tag = new Tag(1L, "Test tag");
        tag.setName("New name");
        tagService.update(tag);
        verify(tagDAO, times(1)).update(tag);
    }

    @Test
    public void testFindById() throws Exception {
        Tag tag = new Tag(1L, "Test tag");
        when(tagDAO.findById(1L)).thenReturn(tag);
        tagService.findById(1L);
        verify(tagDAO, times(1)).findById(1L);
        assertEquals(tagService.findById(1L), tag);
    }

    @Test(expected = ServiceException.class)
    public void testFindByIdWithException() throws Exception {
        Long id = 1L;
        doThrow(DAOException.class).when(tagDAO).findById(id);
        tagService.findById(id);
        verify(tagDAO, times(1)).findById(id);
        verifyNoMoreInteractions(tagDAO);
    }

    @Test
    public void testDelete() throws Exception {
        tagService.delete(1L);
        verify(tagDAO, times(1)).unlink(1L);
        verify(tagDAO, times(1)).delete(1L);
        verifyNoMoreInteractions(tagDAO);
    }

    @Test(expected = ServiceException.class)
    public void testUpdateWithException() throws Exception {
        Tag testTag = new Tag(1L, "");
        doThrow(DAOException.class).when(tagDAO).update(testTag);
        tagService.update(testTag);
        verify(tagDAO, times(1)).insert(testTag);
        verifyNoMoreInteractions(tagDAO);
    }

    @Test(expected = ServiceException.class)
    public void testDeleteWithException() throws Exception {
        Long id = 1L;
        doThrow(DAOException.class).when(tagDAO).delete(id);
        tagService.delete(id);
        verify(tagDAO, times(1)).delete(id);
        verifyNoMoreInteractions(tagDAO);
    }

    @Test
    public void testFindByNewsId() throws Exception {
        Tag tag1 = new Tag(1L, "John");
        when(tagDAO.findByNewsId(1L))
                .thenReturn(Collections.singletonList(tag1));
        assertTrue(tagService.findByNewsId(1L).contains(tag1));
        verify(tagDAO, times(1)).findByNewsId(1L);
        verifyNoMoreInteractions(tagDAO);
    }

    @Test(expected = ServiceException.class)
    public void testFindByNewsIdWithException() throws Exception {
        doThrow(DAOException.class).when(tagDAO).findByNewsId(1L);
        tagService.findByNewsId(1L);
        verify(tagDAO, times(1)).findByNewsId(1L);
        verifyNoMoreInteractions(tagDAO);
    }

    @Test
    public void testExists() throws Exception {
        Tag testTag = new Tag(1L, "John");
        when(tagDAO.findById(testTag.getId())).thenReturn(testTag);
        assertTrue(tagService.exists(testTag));
        verify(tagDAO, times(1)).findById(testTag.getId());
        verifyNoMoreInteractions(tagDAO);
    }

    @Test(expected = ServiceException.class)
    public void testExistsWithException() throws Exception {
        Tag testTag = new Tag(1L, "John");
        doThrow(DAOException.class)
                .when(tagDAO)
                .findById(testTag.getId());
        tagService.exists(testTag);
        verify(tagDAO, times(1)).findById(testTag.getId());
        verifyNoMoreInteractions(tagDAO);
    }
}
