package com.epam.newsmanagement.service;

import com.epam.newsmanagement.domain.ExtendedNews;
import com.epam.newsmanagement.domain.News;

import java.util.List;

/**
 * Service interface with methods
 * encapsulating business logic concerned with {@link ExtendedNews}s
 * and transaction management.
 */
@SuppressWarnings("unused")
public interface ExtendedNewsService {

    /**
     * Creates new news with all corresponding relations
     *
     * @param news     news to create.
     * @param authorId id of author to link with.
     * @param tagIds   ids fo tags to link with.
     * @return newsId of created news.
     * @throws ServiceException if an error occurred when performing operation
     */
    Long create(News news, Long authorId, List<Long> tagIds) throws ServiceException;

    /**
     * Retrieves an news with all corresponding relations.
     *
     * @param id id of news to retrieve.
     * @return an extended news with the given id with all relations.
     * @throws ServiceException if an error occurred when performing operation
     */
    ExtendedNews findById(Long id) throws ServiceException;

    /**
     * Retrieves list of all existing news with all relations.
     *
     * @return {@link List} of extended news.
     * @throws ServiceException if an error occurred when performing operation
     */
    List<ExtendedNews> findAll() throws ServiceException;

    /**
     * Updates given news and creates new relations with author and tags.
     *
     * @param news     news to create.
     * @param authorId id of author to link with.
     * @param tagIds   ids fo tags to link with.
     * @throws ServiceException if an error occurred when performing operation
     */
    void update(News news, Long authorId, List<Long> tagIds) throws ServiceException;
}