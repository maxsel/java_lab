package com.epam.newsmanagement.dao.jdbc;

import com.epam.newsmanagement.dao.DAOException;
import com.epam.newsmanagement.dao.UserDAO;
import com.epam.newsmanagement.domain.User;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import javax.inject.Inject;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * Data Access Object with methods
 * for accessing {@link User}s in the database.
 */
@Repository
@SuppressWarnings("Duplicates")
public class JdbcUserDAO implements UserDAO {

    private static final String SQL_INSERT =
            "INSERT INTO USERS(USER_ID, USER_NAME, LOGIN, PASSWORD)" +
            " VALUES (USERS_SEQ.nextval, ?, ?, ?)";
    private static final String SQL_UPDATE_BY_ID =
            "UPDATE USERS" +
            " SET USER_NAME = ?, LOGIN = ?, PASSWORD = ? WHERE USER_ID = ?";
    private static final String SQL_DELETE_BY_ID =
            "DELETE USERS WHERE USER_ID = ?";
    private static final String SQL_SELECT_BY_ID =
            "SELECT USER_ID, USER_NAME, LOGIN, PASSWORD" +
            " FROM USERS WHERE USER_ID = ?";
    private static final String SQL_SELECT_ALL =
            "SELECT USER_ID, USER_NAME, LOGIN, PASSWORD FROM USERS";
    private static final String SQL_DELETE_ROLE_BY_USER =
            "DELETE ROLES WHERE USER_ID = ?";
    private static final String COLUMN_USER_ID = "USER_ID";
    private static final String COLUMN_USER_NAME = "USER_NAME";
    private static final String COLUMN_LOGIN = "LOGIN";
    private static final String COLUMN_PASSWORD = "PASSWORD";

    private final DataSource dataSource;

    @Inject
    public JdbcUserDAO(DataSource dataSource) {
        Assert.notNull(dataSource, "DataSource must be not null!");
        this.dataSource = dataSource;
    }

    /**
     * Persists user in the database.
     *
     * @param user user to persist.
     * @return id of inserted user.
     * @throws DAOException if database error occurred
     */
    @Override
    public Long insert(User user) throws DAOException {
        if (user == null) return null;
        Long insertedId = null;
        String[] generated = {COLUMN_USER_ID};
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps =
                     connection.prepareStatement(SQL_INSERT, generated)) {
            ps.setString(1, user.getName());
            ps.setString(2, user.getLogin());
            ps.setString(3, user.getPassword());
            ps.executeUpdate();
            ResultSet generatedKeys = ps.getGeneratedKeys();
            if (generatedKeys.next()) {
                insertedId = generatedKeys.getLong(1);
            } else {
                throw new DAOException("Exception in JdbcUserDAO#insert(User);" +
                        " arguments: [" + user + "];" +
                        " message: No ID generated");
            }
        } catch (SQLException e) {
            throw new DAOException("Exception in JdbcUserDAO#insert(User);" +
                    " arguments: [" + user + "];" +
                    " nested exception: [" + e + "]");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return insertedId;
    }

    /**
     * Updates all fields of user in database by id.
     *
     * @param user user to update.
     * @throws DAOException if database error occurred
     */
    @Override
    public void update(User user) throws DAOException {
        if (user == null) return;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SQL_UPDATE_BY_ID)) {
            ps.setString(1, user.getName());
            ps.setString(2, user.getLogin());
            ps.setString(3, user.getPassword());
            ps.setLong(4, user.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Exception in JdbcUserDAO#update(User);" +
                    " arguments: [" + user + "];" +
                    " nested exception: [" + e + "]");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * Deletes user by id.
     *
     * @param id id of user to delete.
     * @throws DAOException if database error occurred
     */
    @Override
    public void delete(Long id) throws DAOException {
        if (id == null) return;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement delRole =
                     connection.prepareStatement(SQL_DELETE_ROLE_BY_USER);
             PreparedStatement delUser = 
                     connection.prepareStatement(SQL_DELETE_BY_ID)) {
            delRole.setLong(1, id);
            delRole.executeUpdate();
            delUser.setLong(1, id);
            delUser.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Exception in JdbcUserDAO#delete(Long);" +
                    " arguments: [" + id + "];" +
                    " nested exception: [" + e + "]");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * Retrieves all users from the needed table of the database.
     *
     * @return {@link List} of users.
     * @throws DAOException if database error occurred
     */
    @Override
    public List<User> findAll() throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SQL_SELECT_ALL);
             ResultSet rs = ps.executeQuery()) {
            return fetchUsersListFromResultSet(rs);
        } catch (SQLException e) {
            throw new DAOException("Exception in JdbcUserDAO#findAll();" +
                    " arguments: [" + "];" +
                    " nested exception: [" + e + "]");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * Retrieves user with the given id.
     *
     * @param id id of user to retrieve.
     * @return user with the given id.
     * @throws DAOException if database error occurred
     */
    @Override
    public User findById(Long id) throws DAOException {
        if (id == null) return null;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SQL_SELECT_BY_ID)) {
            ps.setLong(1, id);
            ResultSet rs = ps.executeQuery();
            return fetchUserFromResultSet(rs);
        } catch (SQLException e) {
            throw new DAOException("Exception in JdbcUserDAO#findById(Long);" +
                    " arguments: [" + id + "];" +
                    " nested exception: [" + e + "]");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    private User fetchUserFromResultSet(ResultSet rs)
            throws SQLException {
        User user = null;
        if (rs.next()) {
            user = new User();
            user.setId(rs.getLong(COLUMN_USER_ID));
            user.setName(rs.getString(COLUMN_USER_NAME));
            user.setLogin(rs.getString(COLUMN_LOGIN));
            user.setPassword(rs.getString(COLUMN_PASSWORD));
        }
        return user;
    }

    private List<User> fetchUsersListFromResultSet(ResultSet rs)
            throws SQLException {
        List<User> users = new LinkedList<>();
        while (rs.next()) {
            User user = new User();
            user.setId(rs.getLong(COLUMN_USER_ID));
            user.setName(rs.getString(COLUMN_USER_NAME));
            user.setLogin(rs.getString(COLUMN_LOGIN));
            user.setPassword(rs.getString(COLUMN_PASSWORD));
            users.add(user);
        }
        return users;
    }
}
