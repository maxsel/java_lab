package com.epam.newsmanagement.command;

import com.epam.newsmanagement.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

public class CommandFactory {
    private static final Logger LOG = LogManager.getLogger(CommandFactory.class);
    private static final String COMMAND_PARAMETER_NAME = "command";

    private CommandFactory() {
    }

    public static Command defineCommand(HttpServletRequest request)
            throws ServiceException {
        String command = request.getParameter(COMMAND_PARAMETER_NAME);
        if (command != null) {
            try {
                LOG.info("Command: " + command);
                return CommandEnum.valueOf(command.toUpperCase()).getCommand();
            } catch (IllegalArgumentException e) {
                throw new ServiceException("Wrong command value.", e);
            }
        } else {
            LOG.info("Null command, forward to shownewslist");
            return CommandEnum.SHOWNEWSLIST.getCommand();
        }
    }
}
