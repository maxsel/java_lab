package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.service.ServiceException;
import com.epam.newsmanagement.service.TagService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.inject.Inject;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/admin/tags")
public class TagController {
    private static final Logger LOG = LogManager.getLogger(TagController.class);

    private final TagService tagService;

    @Inject
    public TagController(TagService tagService) {
        Assert.notNull(tagService, "TagService must be not null!");
        this.tagService = tagService;
    }

    @ModelAttribute("tags")
    public List<Tag> populateTagsList() throws ServiceException {
        return tagService.findAll();
    }

    @ModelAttribute("tag")
    public Tag populateTagDTO() throws ServiceException {
        return new Tag();
    }

    @ModelAttribute("newTag")
    public Tag populateNewTagDTO() throws ServiceException {
        return new Tag();
    }

    @RequestMapping("/edit-tags")
    public String showEditTagsPage(Model model) {
        model.addAttribute("currentPage", "edit-tags");
        return "edit-tags";
    }

    @RequestMapping("/add")
    public String add(@Valid @ModelAttribute("newTag") Tag tag,
                      BindingResult bindingResult) {
        try {
            if (bindingResult.hasErrors()) {
                LOG.debug("There are binding errors:" + bindingResult.getAllErrors());
                return "edit-tags";
            }
            tagService.create(tag);
        } catch (ServiceException e) {
            LOG.error(e);
            return "error";
        }
        return "redirect:/admin/tags/edit-tags";
    }

    @RequestMapping(value = "/update")
    public String update(@Valid @ModelAttribute("tag") Tag tag,
                         BindingResult bindingResult){
        try {
            LOG.debug("Tag to update: " + tag);
            if (bindingResult.hasErrors()) {
                LOG.debug("There are binding errors:" + bindingResult.getAllErrors());
                return "edit-tags";
            }
            tagService.update(tag);
        } catch (ServiceException e) {
            LOG.error(e);
            return "error";
        }
        return "redirect:/admin/tags/edit-tags";
    }

    @RequestMapping("/{id}/delete")
    public String delete(@PathVariable("id") Long id, Model model) {
        try {
            tagService.delete(id);
        } catch (ServiceException e) {
            LOG.error(e);
            return "error";
        }
        return "redirect:/admin/tags/edit-tags";
    }
}
