package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class WelcomeController {
    private static final Logger LOG = LogManager.getLogger(WelcomeController.class);

    @RequestMapping(value = {"/", "/admin"})
    public String redirectToNewsList() {
        return "redirect:/admin/news-list";
    }
}
