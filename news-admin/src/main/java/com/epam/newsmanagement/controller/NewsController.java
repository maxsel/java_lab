package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.domain.ExtendedNews;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.SearchCriteria;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.service.*;
import com.sun.org.apache.bcel.internal.generic.NEW;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/admin/news")
public class NewsController {
    private static final Logger LOG = LogManager.getLogger(NewsController.class);

    private final ExtendedNewsService extendedNewsService;
    private final NewsService newsService;
    private final  AuthorService authorService;
    private final TagService tagService;
    private final CommentService commentService;

    @Inject
    public NewsController(ExtendedNewsService extendedNewsService,
                          NewsService newsService,
                          AuthorService authorService,
                          TagService tagService,
                          CommentService commentService) {
        Assert.notNull(extendedNewsService, "ExtendedNewsService must be not null!");
        Assert.notNull(newsService, "NewsService must be not null!");
        Assert.notNull(authorService, "AuthorService must be not null!");
        Assert.notNull(tagService, "TagService must be not null!");
        Assert.notNull(commentService, "CommentService must be not null!");
        this.extendedNewsService = extendedNewsService;
        this.newsService = newsService;
        this.authorService = authorService;
        this.tagService = tagService;
        this.commentService = commentService;
    }

    @RequestMapping("/{id}/view")
    public String showNews(@PathVariable("id") Long id, Model model, HttpSession session)
            throws ServiceException {
        ExtendedNews news = extendedNewsService.findById(id);
        if (news == null || news.getNews() == null) {
            throw new AbsentPageException("No news with such id: " + id);
        }
        model.addAttribute("news", extendedNewsService.findById(id));
        model.addAttribute("commentList", commentService.findByNewsId(id));

        // Get searchCriteria from session, or create, if it does not exist
        SearchCriteria searchCriteria = (SearchCriteria) session.getAttribute("searchCriteria");
        if (searchCriteria == null) {
            searchCriteria = new SearchCriteria();
            session.setAttribute("searchCriteria", searchCriteria);
        }
        model.addAttribute("searchCriteria", searchCriteria);
        Long prevNewsId = newsService.findPrev(id, searchCriteria);
        Long nextNewsId = newsService.findNext(id, searchCriteria);
        if (newsService.findById(prevNewsId) != null) {
            model.addAttribute("prevNewsId", prevNewsId);
        }
        if (newsService.findById(nextNewsId) != null) {
            model.addAttribute("nextNewsId", nextNewsId);
        }
        return "news";
    }

    @RequestMapping("/add")
    public String showAddNewsPage(Model model) throws ServiceException {
        model.addAttribute("operation", "add");
        model.addAttribute("currentPage", "edit-news");
        model.addAttribute("newsExtended", new ExtendedNews());
        model.addAttribute("authorList", authorService.findAll());
        model.addAttribute("tagList", tagService.findAll());
        return "edit-news";
    }

    @RequestMapping(value = "/add/save", method = RequestMethod.POST)
    public String saveNewlyCreatedNews(@Valid @ModelAttribute("extendedNews")
                                                   ExtendedNews extendedNews,
                                       BindingResult bindingResult)
            throws ServiceException {
        LOG.debug("ExtendedNews to create: " + extendedNews);
        long id = extendedNewsService.create(extendedNews.getNews(),
                extendedNews.getAuthor().getId(),
                extendedNews.getTags().stream().map(Tag::getId).collect(Collectors.toList()));
        extendedNews.getNews().setId(id);
        return "redirect:/admin/news/" + extendedNews.getNews().getId() + "/view";
    }

    @RequestMapping("/{id}/edit")
    public String showEditNewsPage(@PathVariable("id") Long id, Model model)
            throws ServiceException {
        model.addAttribute("operation", "edit");
        News news = newsService.findById(id);
        if (news == null) {
            throw new AbsentPageException("No news with such id: " + id);
        }
        model.addAttribute("newsExtended", extendedNewsService.findById(id));
        model.addAttribute("authorList", authorService.findAll());
        model.addAttribute("tagList", tagService.findAll());
        return "edit-news";
    }

    @RequestMapping(value = "/{id}/edit/save", method = RequestMethod.POST)
    public String saveNewsAfterEditing(@PathVariable("id") long id,
                                       @Valid @ModelAttribute("extendedNews") ExtendedNews extendedNews,
                                       BindingResult bindingResult)
            throws ServiceException {
        LOG.debug("ExtendedNews to update(id=" + id + "): " + extendedNews);
        if (bindingResult.hasErrors()) {
            LOG.debug("There are binding errors:" + bindingResult.getAllErrors());
        }
        extendedNewsService.update(extendedNews.getNews(),
                extendedNews.getAuthor().getId(),
                extendedNews.getTags().stream()
                        .map(Tag::getId)
                        .collect(Collectors.toList())
        );
        newsService.count();
        return "redirect:/admin/news/" + extendedNews.getNews().getId() + "/view";
    }

    @ExceptionHandler(ServiceException.class)
    public String serviceException(ServiceException ex, Model model) {
        LOG.error(ex);
        model.addAttribute("errmsg", "Internal error " + ex);
        return "error";
    }

    @ExceptionHandler(Exception.class)
    public String absentPage(Exception ex, Model model) {
        LOG.error(ex);
        model.addAttribute("errmsg", ex);
        return "error";
    }
}
