package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.inject.Inject;
import javax.validation.Valid;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

@Controller
@RequestMapping("/admin/authors")
public class AuthorController {
    private static final Logger LOG = LogManager.getLogger(AuthorController.class);

    private final AuthorService authorService;

    @Inject
    public AuthorController(AuthorService authorService) {
        Assert.notNull(authorService, "AuthorService must be not null!");
        this.authorService = authorService;
    }

    @ModelAttribute("authors")
    public List<Author> populateAuthorsList() throws ServiceException {
        return new LinkedList<>(authorService.findAll());
    }

    @ModelAttribute("author")
    public Author populateAuthorDTO() throws ServiceException {
        return new Author();
    }

    @ModelAttribute("newAuthor")
    public Author populateNewAuthorDTO() throws ServiceException {
        return new Author();
    }

    @RequestMapping("/edit-authors")
    public String showEditAuthorsPage(Model model) {
        model.addAttribute("currentPage", "edit-authors");
        return "edit-authors";
    }

    @RequestMapping("/add")
    public String add(@Valid @ModelAttribute("newAuthor") Author author,
                      BindingResult bindingResult) {
        try {
            if (bindingResult.hasErrors()) {
                LOG.debug("There are binding errors:" + bindingResult.getAllErrors());
                return "edit-authors";
            }
            authorService.create(author);
        } catch (ServiceException e) {
            LOG.error(e);
            return "error";
        }
        return "redirect:/admin/authors/edit-authors";
    }

    @RequestMapping(value = "/update")
    public String update(@Valid @ModelAttribute("author") Author author,
                         BindingResult bindingResult){
        try {
            LOG.debug("Author to update: " + author);
            if (bindingResult.hasErrors()) {
                LOG.debug("There are binding errors:" + bindingResult.getAllErrors());
                return "edit-authors";
            }
            authorService.update(author);
        } catch (ServiceException e) {
            LOG.error(e);
            return "error";
        }
        return "redirect:/admin/authors/edit-authors";
    }

    @RequestMapping("/{id}/expire")
    public String expire(@PathVariable("id") Long id) {
        try {
            authorService.expire(id, new Timestamp(System.currentTimeMillis()));
        } catch (ServiceException e) {
            LOG.error(e);
            return "error";
        }
        return "redirect:/admin/authors/edit-authors";
    }
}
