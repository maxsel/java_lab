package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.DAOException;
import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.service.ServiceException;
import com.epam.newsmanagement.service.TagService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.inject.Inject;
import java.util.List;

/**
 * Service with methods
 * encapsulating business logic concerned with {@link Tag}s
 * and transaction management.
 */
@Service
public class TagServiceImpl implements TagService {

    private static final Logger LOG = LogManager.getLogger(TagServiceImpl.class);

    private final TagDAO tagDAO;

    @Inject
    public TagServiceImpl(TagDAO tagDAO) {
        Assert.notNull(tagDAO, "TagDAO must be not null!");
        this.tagDAO = tagDAO;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long create(Tag tag) throws ServiceException {
        try {
            return tagDAO.insert(tag);
        } catch (DAOException e) {
            LOG.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Long> createAll(List<Tag> tags) throws ServiceException {
        try {
            return tagDAO.insertAll(tags);
        } catch (DAOException e) {
            LOG.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void update(Tag tag) throws ServiceException {
        try {
            tagDAO.update(tag);
        } catch (DAOException e) {
            LOG.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(rollbackFor = ServiceException.class)
    @SuppressWarnings("Duplicates")
    public void delete(Long id) throws ServiceException {
        try {
            tagDAO.unlink(id);
            tagDAO.delete(id);
        } catch (DAOException e) {
            LOG.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Tag> findAll() throws ServiceException {
        try {
            return tagDAO.findAll();
        } catch (DAOException e) {
            LOG.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Tag findById(Long id) throws ServiceException {
        try {
            return tagDAO.findById(id);
        } catch (DAOException e) {
            LOG.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Tag> findByNewsId(Long newsId) throws ServiceException {
        try {
            return tagDAO.findByNewsId(newsId);
        } catch (DAOException e) {
            LOG.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean exists(Tag tag) throws ServiceException {
        return tag != null && findById(tag.getId()) != null;
    }
}
