package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.domain.Comment;

import java.util.List;

/**
 * Data Access Object interface with methods
 * for accessing {@link Comment}s in the database.
 */
public interface CommentDAO extends GenericDAO<Long, Comment> {
    /**
     * Returns the {@link List} of comments
     * for the {@link com.epam.newsmanagement.domain.News} with the given id.
     *
     * @param newsId id of news.
     * @return list of comments for the news with the given id.
     * @throws DAOException if database error occurred
     */
    List<Comment> findByNewsId(Long newsId) throws DAOException;
}
