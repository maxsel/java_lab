package com.epam.newsmanagement.service;

import com.epam.newsmanagement.domain.Tag;

import java.util.List;

/**
 * Service interface with methods
 * encapsulating business logic concerned with {@link Tag}s
 * and transaction management.
 */
@SuppressWarnings("unused")
public interface TagService {

    /**
     * Creates new tag.
     *
     * @param tag tag to create.
     * @return id of created tag.
     * @throws ServiceException if an error occurred when performing operation
     */
    Long create(Tag tag) throws ServiceException;

    /**
     * Creates all tags from list.
     *
     * @param tags list of tags to create.
     * @return list of ids of created tags.
     * @throws ServiceException if an error occurred when performing operation
     */
    List<Long> createAll(List<Tag> tags) throws ServiceException;

    /**
     * Edits existing tag by id.
     *
     * @param tag tag to update.
     * @throws ServiceException if an error occurred when performing operation
     */
    void update(Tag tag) throws ServiceException;

    /**
     * Deletes tag by id.
     *
     * @param id id of tag to delete.
     * @throws ServiceException if an error occurred when performing operation
     */
    void delete(Long id) throws ServiceException;

    /**
     * Retrieves an tag with the given id.
     *
     * @param id id of tag to retrieve.
     * @return an tag with the given id.
     * @throws ServiceException if an error occurred when performing operation
     */
    Tag findById(Long id) throws ServiceException;

    /**
     * Retrieves list of all existing tags.
     *
     * @return {@link List} of tags.
     * @throws ServiceException if an error occurred when performing operation
     */
    List<Tag> findAll() throws ServiceException;

    /**
     * Returns list of all {@link Tag}s
     * for the {@link com.epam.newsmanagement.domain.News}
     * with the given id.
     *
     * @param newsId id of news.
     * @return tag of the news with the given id.
     * @throws ServiceException if an error occurred when performing operation
     */
    List<Tag> findByNewsId(Long newsId) throws ServiceException;

    /**
     * Defines if the given tag already exists.
     *
     * @param tag tag to define existence.
     * @return true if tag exists
     * @throws ServiceException if an error occurred when performing operation
     */
    boolean exists(Tag tag) throws ServiceException;
}
