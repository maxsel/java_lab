var prevName;
var prevId;

function startEditing(id) {
	console.log("startEditing(id=" + id + ")");

	stopEditing(prevId);
	prevName = $("#tag" + id).val();
	prevId = id;
	$("#tag" + id).removeAttr("disabled");

	$("#edit" + id).hide();
	$("#update" + id).show();
	$("#delete" + id).show();
	$("#cancel" + id).show();

	$("#cancel" + id).click(function() {
		stopEditing(id)
	});
}

function stopEditing(id) {
	console.log("stopEditing(id=" + id + ")");

	$("#tag" + id).val(prevName);
	$("#tag" + id).attr("disabled", "true");

	$("#edit" + id).show();
	$("#update" + id).hide();
	$("#delete" + id).hide();
	$("#cancel" + id).hide();
}