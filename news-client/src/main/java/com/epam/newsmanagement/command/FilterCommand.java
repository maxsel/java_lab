package com.epam.newsmanagement.command;

import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.SearchCriteria;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.ServiceException;
import com.epam.newsmanagement.service.TagService;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

class FilterCommand implements Command {
    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        NewsService newsService = (NewsService) context.getBean("newsService");
        AuthorService authorService = (AuthorService) context.getBean("authorService");
        TagService tagService = (TagService) context.getBean("tagService");

        List<Long> list = new ArrayList<>();
        String[] tagIds = request.getParameterValues("tagIds");
        if (null != tagIds) {
            for (String str : tagIds) {
                list.add(Long.valueOf(str));
            }
        }
        SearchCriteria searchCriteria =
                new SearchCriteria(Long.valueOf(request.getParameter("authorId")), list);
        request.getSession(true).setAttribute("newsList", newsService.findByCriteria(searchCriteria));
        request.getSession(true).setAttribute("authorList", authorService.findAll());
        request.getSession(true).setAttribute("tagList", tagService.findAll());
        request.getSession(true).setAttribute("searchCriteria", searchCriteria);

        return request.getContextPath() + "/news-client?command=shownewslist";
    }

    @Override
    public boolean needsRedirect() {
        return true;
    }
}
