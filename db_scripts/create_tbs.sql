CREATE TABLESPACE news_mgmt_tbs
  DATAFILE 'news_mgmt_tbs.dat' 
    SIZE 10M
    REUSE
    AUTOEXTEND ON NEXT 10M MAXSIZE 200M;

CREATE TEMPORARY TABLESPACE news_mgmt_tbs_temp
  TEMPFILE 'news_mgmt_tbs_temp.dbf'
    SIZE 5M
    AUTOEXTEND ON;