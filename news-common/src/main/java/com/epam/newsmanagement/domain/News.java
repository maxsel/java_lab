package com.epam.newsmanagement.domain;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

public class News implements Serializable {
    private static final long serialVersionUID = -9064173129296947448L;

    private Long id;
    @NotNull
    @Size(min = 3, max = 30)
    private String title;
    @NotNull
    @Size(min = 3, max = 100)
    private String shortText;
    @NotNull
    @Size(min = 3, max = 2000)
    private String fullText;
    private Timestamp creationDate;
    private Date modificationDate;

    public News() {
    }

    public News(Long id, String title, String shortText, String fullText,
                Timestamp creationDate, Date modificationDate) {
        this.id = id;
        this.title = title;
        this.shortText = shortText;
        this.fullText = fullText;
        this.creationDate = creationDate;
        this.modificationDate = modificationDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortText() {
        return shortText;
    }

    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    public String getFullText() {
        return fullText;
    }

    public void setFullText(String fullText) {
        this.fullText = fullText;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    @Override
    @SuppressWarnings("SimplifiableIfStatement")
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        News news = (News) o;

        if (getId() != null ? !getId().equals(news.getId()) : news.getId() != null)
            return false;
        if (getTitle() != null ? !getTitle().equals(news.getTitle()) : news.getTitle() != null)
            return false;
        if (getShortText() != null ? !getShortText().equals(news.getShortText()) : news.getShortText() != null)
            return false;
        if (getFullText() != null ? !getFullText().equals(news.getFullText()) : news.getFullText() != null)
            return false;
        if (getCreationDate() != null ? !getCreationDate().equals(news.getCreationDate()) : news.getCreationDate() != null)
            return false;
        return getModificationDate() != null ? getModificationDate().equals(news.getModificationDate()) : news.getModificationDate() == null;

    }

    @Override
    public int hashCode() {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + (getTitle() != null ? getTitle().hashCode() : 0);
        result = 31 * result + (getShortText() != null ? getShortText().hashCode() : 0);
        result = 31 * result + (getFullText() != null ? getFullText().hashCode() : 0);
        result = 31 * result + (getCreationDate() != null ? getCreationDate().hashCode() : 0);
        result = 31 * result + (getModificationDate() != null ? getModificationDate().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "News{" + "id=" + id +
                ", title='" + title +
                ", shortText='" + shortText +
                ", fullText='" + fullText +
                ", creationDate=" + creationDate +
                ", modificationDate=" + modificationDate +
                '}';
    }
}
