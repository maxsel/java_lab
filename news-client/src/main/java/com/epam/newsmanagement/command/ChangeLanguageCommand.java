package com.epam.newsmanagement.command;

import com.epam.newsmanagement.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Command that changes user's language in session
 */
class ChangeLanguageCommand implements Command {
    private static final String REGEX_PATTERN = "(^http://.+?)(?=/)";
    private static final String LANG_SESSION_ATTRIBUTE = "lang";
    private static final String LANG_REQUEST_PARAMETER = "lang";
    private static final String REFERER_REQUEST_HEADER = "referer";
    private static final String URL_DEFAULT = "/news-client?command=empty";
    private static Logger LOG = LogManager.getLogger(ChangeLanguageCommand.class);

    @Override
    public String execute(HttpServletRequest request)
            throws ServiceException {
        String requestLang = request.getParameter(LANG_REQUEST_PARAMETER);
        LOG.info(requestLang);
        request.getSession(true).setAttribute(LANG_SESSION_ATTRIBUTE, requestLang);
        String url = request.getHeader(REFERER_REQUEST_HEADER);
        if (url == null) {
            return URL_DEFAULT;
        }
        Matcher prefixMatcher = Pattern.compile(REGEX_PATTERN).matcher(url);
        if (prefixMatcher.find()) {
            url = url.substring(prefixMatcher.end());
        }
        return url;
    }

    @Override
    public boolean needsRedirect() {
        return true;
    }
}