package com.epam.newsmanagement.domain;

import java.util.List;

public class SearchCriteria {
    private Long authorId;
    private List<Long> tagsId;

    public SearchCriteria() {
    }

    public SearchCriteria(Long authorId, List<Long> tagsId) {
        this.authorId = authorId;
        this.tagsId = tagsId;
    }

    public Long getAuthorId() {
        return authorId;
    }

    @SuppressWarnings("unused")
    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public List<Long> getTagsId() {
        return tagsId;
    }

    @SuppressWarnings("unused")
    public void setTagsId(List<Long> tagsId) {
        this.tagsId = tagsId;
    }

    @Override
    @SuppressWarnings("SimplifiableIfStatement")
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SearchCriteria that = (SearchCriteria) o;

        if (getAuthorId() != null ? !getAuthorId().equals(that.getAuthorId()) : that.getAuthorId() != null)
            return false;
        return getTagsId() != null ? getTagsId().equals(that.getTagsId()) : that.getTagsId() == null;

    }

    @Override
    public int hashCode() {
        int result = getAuthorId() != null ? getAuthorId().hashCode() : 0;
        result = 31 * result + (getTagsId() != null ? getTagsId().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "SearchCriteria{" + "authorId=" + authorId +
                ", tagsId=" + tagsId +
                '}';
    }
}
