package com.epam.newsmanagement.domain;

import java.io.Serializable;
import java.sql.Timestamp;

public class Comment implements Serializable {
    private static final long serialVersionUID = -7019903294657811329L;

    private Long id;
    private Long newsId;
    private String text;
    private Timestamp creationDate;

    public Comment() {
    }

    public Comment(Long id, Long newsId, String text, Timestamp creationDate) {
        this.id = id;
        this.newsId = newsId;
        this.text = text;
        this.creationDate = creationDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNewsId() {
        return newsId;
    }

    public void setNewsId(Long newsId) {
        this.newsId = newsId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    @SuppressWarnings("SimplifiableIfStatement")
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Comment comment = (Comment) o;

        if (getId() != null ? !getId().equals(comment.getId()) : comment.getId() != null)
            return false;
        if (getNewsId() != null ? !getNewsId().equals(comment.getNewsId()) : comment.getNewsId() != null)
            return false;
        if (getText() != null ? !getText().equals(comment.getText()) : comment.getText() != null)
            return false;
        return getCreationDate() != null ? getCreationDate().equals(comment.getCreationDate()) : comment.getCreationDate() == null;
    }

    @Override
    public int hashCode() {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + (getNewsId() != null ? getNewsId().hashCode() : 0);
        result = 31 * result + (getText() != null ? getText().hashCode() : 0);
        result = 31 * result + (getCreationDate() != null ? getCreationDate().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Comment{" + "id=" + id +
                ", newsId=" + newsId +
                ", text='" + text +
                ", creationDate=" + creationDate +
                '}';
    }
}
