package com.epam.newsmanagement.service;

import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.SearchCriteria;

import java.util.List;

/**
 * Service interface with methods
 * encapsulating business logic concerned with {@link News}s
 * and transaction management.
 */
@SuppressWarnings("unused")
public interface NewsService {

    int NEWS_PER_PAGE = 3;

    /**
     * Creates new news.
     *
     * @param news news to create.
     * @return id of created news.
     * @throws ServiceException if an error occurred when performing operation
     */
    Long create(News news) throws ServiceException;

    /**
     * Edits existing news by id.
     *
     * @param news news to update.
     * @throws ServiceException if an error occurred when performing operation
     */
    void update(News news) throws ServiceException;

    /**
     * Deletes news by id.
     *
     * @param id id of news to delete.
     * @throws ServiceException if an error occurred when performing operation
     */
    void delete(Long id) throws ServiceException;

    /**
     * Retrieves list of all existing news.
     *
     * @return {@link List} of news.
     * @throws ServiceException if an error occurred when performing operation
     */
    List<News> findAll() throws ServiceException;

    /**
     * Retrieves an news with the given id.
     *
     * @param id id of news to retrieve.
     * @return an news with the given id.
     * @throws ServiceException if an error occurred when performing operation
     */
    News findById(Long id) throws ServiceException;

    /**
     * Returns the {@link List} of news satisfying the search parameters
     * (author and {@link List} of {@link com.epam.newsmanagement.domain.Tag}s),
     * encapsulated in {@link SearchCriteria} object.
     *
     * @param criteria {@link SearchCriteria} object.
     * @return list of news meeting the criteria.
     * @throws ServiceException if an error occurred when performing operation
     */
    List<News> findByCriteria(SearchCriteria criteria) throws ServiceException;

    /**
     * Returns the {@link List} of news satisfying the search parameters
     * (author and {@link List} of {@link com.epam.newsmanagement.domain.Tag}s),
     * encapsulated in {@link SearchCriteria} object; supports pagination.
     *
     * @param criteria {@link SearchCriteria} object.
     * @param page page index
     * @return list of news meeting the criteria.
     * @throws ServiceException if an error occurred when performing operation
     */
    List<News> findByCriteria(SearchCriteria criteria, int page) throws ServiceException;

    /**
     * Creates relation with news and {@link com.epam.newsmanagement.domain.Author}
     * with the given id's correspondingly.
     *
     * @param newsId   news' id.
     * @param authorId author's id.
     * @throws ServiceException if an error occurred when performing operation
     */
    void linkNewsAndAuthor(Long newsId, Long authorId) throws ServiceException;

    /**
     * Removes relation with news
     * and its {@link com.epam.newsmanagement.domain.Author}.
     *
     * @param newsId   news' id.
     * @throws ServiceException if an error occurred when performing operation
     */
    void unlinkNewsAndAuthor(Long newsId) throws ServiceException;

    /**
     * Creates relation with news and {@link com.epam.newsmanagement.domain.Tag}
     * with the given id's correspondingly.
     *
     * @param newsId news' id.
     * @param tagId  tag's id.
     * @throws ServiceException if an error occurred when performing operation
     */
    void linkNewsAndTag(Long newsId, Long tagId) throws ServiceException;

    /**
     * Removes relation with news and {@link com.epam.newsmanagement.domain.Tag}
     * with the given id's correspondingly.
     *
     * @param newsId news' id.
     * @param tagId  tag's id.
     * @throws ServiceException if an error occurred when performing operation
     */
    void unlinkNewsAndTag(Long newsId, Long tagId) throws ServiceException;

    /**
     * Removes relation with news with the given id
     * and all {@link com.epam.newsmanagement.domain.Tag}s with id's
     * from {@link List}.
     *
     * @param newsId    news' id.
     * @param tagIdList {@link List} of tags ids.
     * @throws ServiceException if an error occurred when performing operation
     */
    void linkNewsAndTags(Long newsId, List<Long> tagIdList) throws ServiceException;

    /**
     * Removes relation with news with the given id
     * and all its {@link com.epam.newsmanagement.domain.Tag}s with id's.
     *
     * @param newsId    news' id.
     * @throws ServiceException if an error occurred when performing operation
     */
    void unlinkNewsAndTags(Long newsId) throws ServiceException;

    /**
     * Return total count of news.
     *
     * @return count of news.
     * @throws ServiceException if an error occurred when performing operation
     */
    Long count() throws ServiceException;

    /**
     * Return total count of news, meeting given criteria.
     *
     * @param criteria criteria {@link SearchCriteria} object.
     * @return count of news.
     * @throws ServiceException if an error occurred when performing operation
     */
    Long count(SearchCriteria criteria) throws ServiceException;

    /**
     * Return id of previous news, meeting given criteria.
     *
     * @param criteria criteria {@link SearchCriteria} object.
     * @param id id of news to find previous for.
     * @return id of previous news.
     * @throws ServiceException if an error occurred when performing operation
     */
    Long findPrev(Long id, SearchCriteria criteria) throws ServiceException;

    /**
     * Return id of next news, meeting given criteria.
     *
     * @param criteria criteria {@link SearchCriteria} object.
     * @param id id of news to find next for.
     * @return id of next news.
     * @throws ServiceException if an error occurred when performing operation
     */
    Long findNext(Long id, SearchCriteria criteria) throws ServiceException;
}