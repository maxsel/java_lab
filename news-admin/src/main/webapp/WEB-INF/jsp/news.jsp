<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>

<script type="text/javascript"
        src="<c:url value="/resources/js/jquery.min.js" />">
</script>

<c:url var="rootUrl" value="/admin/news/${news.news.id}/comments" />

<div class="back-container">
    <div class="back" style="display: inline-block;">
        <a href="<c:url value="/admin/news-list/" />" class="edit-link">
            <fmt:message key="news.back"/>
        </a>
    </div>
</div>
<div class="news-container">
    <div class="news-content">
        <span class="news-title">
            <c:out value="${news.news.title}"/><br/>
        </span>
        <span class="news-author">
            (<spring:message code="news_list.by"/> <c:out value="${news.author.name}"/>)
        </span>
        <span class="news-date">
            <c:set var="format"><spring:message code="date.pattern" /></c:set>
            <fmt:formatDate value="${news.news.modificationDate}"
                            var="modificationDateString"
                            pattern="${format}"/>
            <c:out value="${modificationDateString}"/>
        </span>
        <br/>
        <span class="news-fulltext">
            <c:out value="${news.news.fullText}"/>
        </span>
        <c:forEach var="comment" items="${commentList}">
            <div class="comment">
                <div class="comment-date">
                    <fmt:formatDate value="${comment.creationDate}"
                                    var="commentDateString"
                                    pattern="${format}"/>
                    <c:out value="${commentDateString}"/>
                </div>
                <div class="comment-box">
                    <span class="delete-comment">
                        <a
                                class="del-comm-link"
                                href="<c:url value="/admin/news/${news.news.id}/comments/${comment.id}/delete" />">
                                <button class="del-comm-button">x</button>
                        </a><br/>
                    </span>
                    <span class="comment-text">
                        <c:out value="${comment.text}"/>
                    </span>
                </div>
            </div>
        </c:forEach>
        <div class="post-form" style="margin-top: 20px;width:51.5%">
            <form:form action="${rootUrl}/post">
                <textarea name="commentText"
                          class="post-comm-text" rows="4" cols="80"
                          minlength="3" maxlength="100"
                          title=""></textarea>
                <div class="post-button">
                    <input class="post-button-text" type="submit"
                           value="<spring:message code="news.post_comment" />" />
                </div>
            </form:form>
        </div>
    </div>
</div>
<div class="prev-next-container">
    <div class="prev-next">
        <c:if test="${not empty prevNewsId}">
            <span class="prev">
                <a href="<c:url value="/admin/news/${prevNewsId}/view" />">
                    < <fmt:message key="news.prev"/>
                </a>
            </span>
        </c:if>
        <c:if test="${not empty nextNewsId}">
            <span class="next">
                <a href="<c:url value="/admin/news/${nextNewsId}/view" />">
                    <fmt:message key="news.next"/> >
                </a>
            </span>
        </c:if>
    </div>
</div>