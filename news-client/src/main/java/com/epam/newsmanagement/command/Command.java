package com.epam.newsmanagement.command;

import com.epam.newsmanagement.service.ServiceException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.servlet.http.HttpServletRequest;

public interface Command {
    ApplicationContext context = new ClassPathXmlApplicationContext("spring-context.xml");

    String execute(HttpServletRequest request) throws ServiceException;

    default boolean needsRedirect() {
        return false;
    }
}