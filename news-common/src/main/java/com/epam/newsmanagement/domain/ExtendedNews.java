package com.epam.newsmanagement.domain;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

public class ExtendedNews implements Serializable {
    private static final long serialVersionUID = -2798604352822854045L;

    @NotNull
    private News news;
    @NotNull
    private Author author;
    private List<Comment> comments;
    private List<Tag> tags;

    public ExtendedNews() {
    }

    @SuppressWarnings("unused")
    public ExtendedNews(News news, Author author, List<Comment> comments, List<Tag> tag) {
        this.news = news;
        this.author = author;
        this.comments = comments;
        this.tags = tag;
    }

    public News getNews() {
        return news;
    }

    public void setNews(News news) {
        this.news = news;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    @Override
    @SuppressWarnings("SimplifiableIfStatement")
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ExtendedNews that = (ExtendedNews) o;

        if (getNews() != null ? !getNews().equals(that.getNews()) : that.getNews() != null)
            return false;
        if (getAuthor() != null ? !getAuthor().equals(that.getAuthor()) : that.getAuthor() != null)
            return false;
        if (getComments() != null ? !getComments().equals(that.getComments()) : that.getComments() != null)
            return false;
        return getTags() != null ? getTags().equals(that.getTags()) : that.getTags() == null;
    }

    @Override
    public int hashCode() {
        int result = getNews() != null ? getNews().hashCode() : 0;
        result = 31 * result + (getAuthor() != null ? getAuthor().hashCode() : 0);
        result = 31 * result + (getComments() != null ? getComments().hashCode() : 0);
        result = 31 * result + (getTags() != null ? getTags().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ExtendedNews{" + "news=" + news +
                ", author=" + author +
                ", comments=" + comments +
                ", tag=" + tags +
                '}';
    }
}
