package com.epam.newsmanagement.domain;

import java.io.Serializable;

public class Role implements Serializable {
    private static final long serialVersionUID = -551646280728812340L;

    private Long userId;
    private String name;

    public Role() {
    }

    public Role(Long userId, String name) {
        this.userId = userId;
        this.name = name;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    @SuppressWarnings("SimplifiableIfStatement")
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Role role = (Role) o;

        if (getUserId() != null ? !getUserId().equals(role.getUserId()) : role.getUserId() != null)
            return false;
        return getName() != null ? getName().equals(role.getName()) : role.getName() == null;

    }

    @Override
    public int hashCode() {
        int result = getUserId() != null ? getUserId().hashCode() : 0;
        result = 31 * result + (getName() != null ? getName().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Role{" + "userId=" + userId +
                ", name='" + name +
                '}';
    }
}
