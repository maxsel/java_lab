package com.epam.newsmanagement.service;

import com.epam.newsmanagement.domain.Author;

import java.sql.Timestamp;
import java.util.List;

/**
 * Service interface with methods
 * encapsulating business logic concerned with {@link Author}s
 * and transaction management.
 */
@SuppressWarnings("unused")
public interface AuthorService {

    /**
     * Creates new author.
     *
     * @param author author to create.
     * @return id of created author.
     * @throws ServiceException if an error occurred when performing operation
     */
    Long create(Author author) throws ServiceException;

    /**
     * Edits existing author by id.
     *
     * @param author author to update.
     * @throws ServiceException if an error occurred when performing operation
     */
    void update(Author author) throws ServiceException;

    /**
     * Deletes author by id.
     *
     * @param id id of author to delete.
     * @throws ServiceException if an error occurred when performing operation
     */
    void delete(Long id) throws ServiceException;

    /**
     * Retrieves list of all existing authors.
     *
     * @return {@link List} of authors.
     * @throws ServiceException if an error occurred when performing operation
     */
    List<Author> findAll() throws ServiceException;

    /**
     * Retrieves an author with the given id.
     *
     * @param id id of author to retrieve.
     * @return an author with the given id.
     * @throws ServiceException if an error occurred when performing operation
     */
    Author findById(Long id) throws ServiceException;

    /**
     * Returns the {@link Author} of the {@link com.epam.newsmanagement.domain.News}
     * with the given id.
     *
     * @param newsId id of news.
     * @return author of the news with the given id.
     * @throws ServiceException if an error occurred when performing operation
     */
    Author findByNewsId(Long newsId) throws ServiceException;

    /**
     * Defines if the given author already exists.
     *
     * @param author author to define existence.
     * @return true if author exists
     * @throws ServiceException if an error occurred when performing operation
     */
    boolean exists(Author author) throws ServiceException;

    /**
     * Expires the author with the given id.
     *
     * @param id id of author to expire.
     * @param time time of expiration
     * @throws ServiceException if an error occurred when performing operation
     */
    void expire(Long id, Timestamp time) throws ServiceException;
}