package com.epam.newsmanagement.command;

import com.epam.newsmanagement.service.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

class ResetCommand implements Command {
    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        HttpSession session = request.getSession(true);
        session.removeAttribute("newsList");
        session.removeAttribute("authorList");
        session.removeAttribute("tagList");
        session.removeAttribute("searchCriteria");

        return request.getContextPath() + "/news-client?command=shownewslist";
    }

    @Override
    public boolean needsRedirect() {
        return true;
    }
}
