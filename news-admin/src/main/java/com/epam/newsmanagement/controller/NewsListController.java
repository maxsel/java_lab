package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.domain.ExtendedNews;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.SearchCriteria;
import com.epam.newsmanagement.service.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;
import java.util.LinkedList;
import java.util.List;

@Controller
@RequestMapping("/admin/news-list")
public class NewsListController {
    private static final Logger LOG = LogManager.getLogger(NewsListController.class);

    private final ExtendedNewsService extendedNewsService;
    private final NewsService newsService;
    private final AuthorService authorService;
    private final TagService tagService;

    @Inject
    public NewsListController(ExtendedNewsService extendedNewsService,
                              NewsService newsService,
                              AuthorService authorService,
                              TagService tagService) {
        Assert.notNull(extendedNewsService, "ExtendedNewsService must be not null!");
        Assert.notNull(newsService, "NewsService must be not null!");
        Assert.notNull(authorService, "AuthorService must be not null!");
        Assert.notNull(tagService, "TagService must be not null!");
        this.extendedNewsService = extendedNewsService;
        this.newsService = newsService;
        this.authorService = authorService;
        this.tagService = tagService;
    }

    @RequestMapping("")
    public String showNewsList() {
        return "redirect:/admin/news-list/1";
    }

    @RequestMapping("/{page}")
    public String showPage(@PathVariable int page, Model model, HttpSession session) {
        try {
            // Get searchCriteria from session, or create, if it does not exist
            SearchCriteria searchCriteria = (SearchCriteria) session.getAttribute("searchCriteria");
            if (searchCriteria == null) {
                searchCriteria = new SearchCriteria();
                session.setAttribute("searchCriteria", searchCriteria);
            }

            List<ExtendedNews> extendedNewsList = new LinkedList<>();
            for (News news : newsService.findByCriteria(searchCriteria, page)) {
                extendedNewsList.add(extendedNewsService.findById(news.getId()));
            }
            model.addAttribute("searchCriteria", searchCriteria);
            long countNews = newsService.count(searchCriteria);
            model.addAttribute("countNews", countNews);
            long countPages = (long) Math.ceil(1.0*newsService.count(searchCriteria)/NewsService.NEWS_PER_PAGE);
            model.addAttribute("countPages", countPages);
            model.addAttribute("page", page);
            model.addAttribute("newsList", extendedNewsList);
            model.addAttribute("authorList", (authorService).findAll());
            model.addAttribute("tagList", (tagService).findAll());
            model.addAttribute("deleteList", new DeleteList());
            model.addAttribute("currentPage", "news-list");
            return "news-list";
        } catch (ServiceException e) {
            LOG.error(e);
            return "error";
        }
    }

    @RequestMapping("/delete")
    public String delete(@ModelAttribute("deleteList") DeleteList deleteList) {
        try {
            for (Long id : deleteList.getIds()) {
                newsService.delete(id);
            }
        } catch (ServiceException e) {
            LOG.error(e);
            return "error";
        }
        return "redirect:/admin/news-list";
    }

    @RequestMapping("/filter")
    public String filter(@ModelAttribute("searchCriteria") SearchCriteria searchCriteria,
                         HttpSession session) {
        session.setAttribute("searchCriteria", searchCriteria);
        return "redirect:/admin/news-list";
    }

    @RequestMapping("/reset")
    public String reset(HttpSession session) {
        session.setAttribute("searchCriteria", new SearchCriteria());

        return "redirect:/admin/news-list";
    }
}
