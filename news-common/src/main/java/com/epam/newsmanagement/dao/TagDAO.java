package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.domain.Tag;

import java.util.List;

/**
 * Data Access Object interface with methods
 * for accessing {@link Tag}s in the database.
 */
public interface TagDAO extends GenericDAO<Long, Tag> {
    /**
     * Inserts the list of {@link Tag}s into the database.
     *
     * @param tags {@link List} of tags to persist.
     * @return {@link List} of {@link Long}s representing id's
     * of newly inserted tags.
     * @throws DAOException if database error occurred
     */
    List<Long> insertAll(List<Tag> tags) throws DAOException;

    /**
     * Returns the {@link List} of tags from the database
     * for the {@link com.epam.newsmanagement.domain.News}
     * with the given id.
     *
     * @param newsId id of news.
     * @return list of tags for the news with the given id.
     * @throws DAOException if database error occurred
     */
    List<Tag> findByNewsId(Long newsId) throws DAOException;

    /**
     * Returns tag with the given name from the database.
     *
     * @param name name of tag to look for.
     * @return tag with the given name.
     * @throws DAOException if database error occurred
     */
    Tag findByName(String name) throws DAOException;

    /**
     * Removes all relations with the given tag's id.
     *
     * @param id id of tag.
     * @throws DAOException if database error occurred
     */
    void unlink(Long id) throws DAOException;
}
