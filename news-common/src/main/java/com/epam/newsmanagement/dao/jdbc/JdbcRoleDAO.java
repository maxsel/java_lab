package com.epam.newsmanagement.dao.jdbc;

import com.epam.newsmanagement.dao.DAOException;
import com.epam.newsmanagement.dao.RoleDAO;
import com.epam.newsmanagement.domain.Role;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import javax.inject.Inject;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * DataAccessObject with methods
 * for accessing {@link Role}s in the database.
 */
@Repository
@SuppressWarnings("Duplicates")
public class JdbcRoleDAO implements RoleDAO {

    private static final String SQL_INSERT =
            "INSERT INTO ROLES(USER_ID, ROLE_NAME) VALUES (?, ?)";
    private static final String SQL_UPDATE_BY_USER_ID =
            "UPDATE ROLES SET ROLE_NAME = ? WHERE USER_ID = ?";
    private static final String SQL_DELETE_ROLE =
            "DELETE ROLES WHERE USER_ID = ? AND ROLE_NAME = ?";
    private static final String SQL_DELETE_BY_NAME =
            "DELETE ROLES WHERE ROLE_NAME = ?";
    private static final String SQL_SELECT_BY_USER_ID =
            "SELECT USER_ID, ROLE_NAME FROM ROLES WHERE USER_ID = ?";
    private static final String SQL_SELECT_ALL =
            "SELECT USER_ID, ROLE_NAME FROM ROLES ORDER BY USER_ID, ROLE_NAME";
    private static final String COLUMN_USER_ID = "USER_ID";
    private static final String COLUMN_ROLE_NAME = "ROLE_NAME";

    private final DataSource dataSource;

    @Inject
    public JdbcRoleDAO(DataSource dataSource) {
        Assert.notNull(dataSource, "DataSource must be not null!");
        this.dataSource = dataSource;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void insert(Role role) throws DAOException {
        if (role == null) return;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SQL_INSERT)) {
            ps.setLong(1, role.getUserId());
            ps.setString(2, role.getName());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Exception in JdbcRoleDAO#insert(Role);" +
                    " arguments: [" + role + "];" +
                    " nested exception: [" + e + "]");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateByUserId(Role role) throws DAOException {
        if (role == null || role.getUserId() == null) return;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SQL_UPDATE_BY_USER_ID)) {
            ps.setString(1, role.getName());
            ps.setLong(2, role.getUserId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Exception in JdbcRoleDAO#updateByUserId(Role);" +
                    " arguments: [" + role + "];" +
                    " nested exception: [" + e + "]");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(Role role) throws DAOException {
        if (role == null || role.getUserId() == null || role.getName() == null)
            return;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SQL_DELETE_ROLE)) {
            ps.setLong(1, role.getUserId());
            ps.setString(2, role.getName());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Exception in JdbcRoleDAO#delete(Role);" +
                    " arguments: [" + role + "];" +
                    " nested exception: [" + e + "]");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteByName(String name) throws DAOException {
        if (name == null) return;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SQL_DELETE_BY_NAME)) {
            ps.setString(1, name);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Exception in JdbcRoleDAO#deleteByName(String);" +
                    " arguments: [" + name + "];" +
                    " nested exception: [" + e + "]");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Role> findAll() throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SQL_SELECT_ALL);
             ResultSet rs = ps.executeQuery()) {
            return fetchRolesListFromResultSet(rs);
        } catch (SQLException e) {
            throw new DAOException("Exception in JdbcRoleDAO#findAll();" +
                    " arguments: [" + "];" +
                    " nested exception: [" + e + "]");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Role findByUserId(Long userId) throws DAOException {
        if (userId == null) return null;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SQL_SELECT_BY_USER_ID)) {
            ps.setLong(1, userId);
            ResultSet rs = ps.executeQuery();
            return fetchRoleFromResultSet(rs);
        } catch (SQLException e) {
            throw new DAOException("Exception in JdbcRoleDAO#findByUserId(Long);" +
                    " arguments: [" + userId + "];" +
                    " nested exception: [" + e + "]");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    private Role fetchRoleFromResultSet(ResultSet rs)
            throws SQLException {
        Role role = null;
        if (rs.next()) {
            role = new Role();
            role.setUserId(rs.getLong(COLUMN_USER_ID));
            role.setName(rs.getString(COLUMN_ROLE_NAME));
        }
        return role;
    }

    private List<Role> fetchRolesListFromResultSet(ResultSet rs)
            throws SQLException {
        List<Role> list = new LinkedList<>();
        while (rs.next()) {
            Role role = new Role();
            role.setUserId(rs.getLong(COLUMN_USER_ID));
            role.setName(rs.getString(COLUMN_ROLE_NAME));
            list.add(role);
        }
        return list;
    }
}
