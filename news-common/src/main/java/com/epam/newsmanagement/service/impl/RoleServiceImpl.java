package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.DAOException;
import com.epam.newsmanagement.dao.RoleDAO;
import com.epam.newsmanagement.domain.Role;
import com.epam.newsmanagement.service.RoleService;
import com.epam.newsmanagement.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.util.Assert;

import javax.inject.Inject;
import java.util.List;

/**
 * Service with methods
 * encapsulating business logic concerned with {@link Role}s
 * and transaction management.
 */
public class RoleServiceImpl implements RoleService {

    private static final Logger LOG = LogManager.getLogger(RoleServiceImpl.class);

    private final RoleDAO roleDAO;

    @Inject
    public RoleServiceImpl(RoleDAO roleDAO) {
        Assert.notNull(roleDAO, "RoleDAO must be not null!");
        this.roleDAO = roleDAO;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void create(Role role) throws ServiceException {
        try {
            roleDAO.insert(role);
        } catch (DAOException e) {
            LOG.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateByUserId(Role role) throws ServiceException {
        try {
            roleDAO.updateByUserId(role);
        } catch (DAOException e) {
            LOG.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(Role role) throws ServiceException {
        try {
            roleDAO.delete(role);
        } catch (DAOException e) {
            LOG.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteByName(String name) throws ServiceException {
        try {
            roleDAO.deleteByName(name);
        } catch (DAOException e) {
            LOG.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Role> findAll() throws ServiceException {
        try {
            return roleDAO.findAll();
        } catch (DAOException e) {
            LOG.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Role findByUserId(Long userId) throws ServiceException {
        try {
            return roleDAO.findByUserId(userId);
        } catch (DAOException e) {
            LOG.error(e);
            throw new ServiceException(e);
        }
    }
}
