package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.dao.DAOException;
import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.ServiceException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings("Duplicates")
public class AuthorServiceImplTest {
    @Mock
    private AuthorDAO authorDAO;

    private AuthorService authorService;

    @Before
    public void init() {
        authorService = new AuthorServiceImpl(authorDAO);
    }


    @Test
    public void testCreate() throws Exception {
        Author testAuthor = new Author(1L, "John", null);
        when(authorDAO.insert(testAuthor)).thenReturn(1L);
        Long id = authorService.create(testAuthor);
        assertEquals(testAuthor.getId(), id);
        verify(authorDAO, times(1)).insert(testAuthor);
        verifyNoMoreInteractions(authorDAO);
    }

    @Test(expected = ServiceException.class)
    @SuppressWarnings("unchecked")
    public void testCreateWithException() throws Exception {
        Author testAuthor = new Author(1L, "", null);
        when(authorDAO.insert(testAuthor)).thenThrow(DAOException.class);
        authorService.create(testAuthor);
        verify(authorDAO, times(1)).insert(testAuthor);
        verifyNoMoreInteractions(authorDAO);
    }

    @Test
    public void testUpdate() throws Exception {
        authorService.update(new Author());
        verify(authorDAO, times(1)).update(new Author());
        verifyNoMoreInteractions(authorDAO);
    }

    @Test(expected = ServiceException.class)
    public void testUpdateWithException() throws Exception {
        Author testAuthor = new Author(1L, "", null);
        doThrow(DAOException.class).when(authorDAO).update(testAuthor);
        authorService.update(testAuthor);
        verify(authorDAO, times(1)).insert(testAuthor);
        verifyNoMoreInteractions(authorDAO);
    }

    @Test
    public void testDelete() throws Exception {
        authorService.delete(1L);
        verify(authorDAO, times(1)).unlink(1L);
        verify(authorDAO, times(1)).delete(1L);
        verifyNoMoreInteractions(authorDAO);
    }

    @Test(expected = ServiceException.class)
    public void testDeleteWithException() throws Exception {
        Long id = 1L;
        doThrow(DAOException.class).when(authorDAO).delete(id);
        authorService.delete(id);
        verify(authorDAO, times(1)).delete(id);
        verifyNoMoreInteractions(authorDAO);
    }

    @Test
    public void testFindAll() throws Exception {
        Author author1 = new Author(1L, "John", null);
        Author author2 = new Author(2L, "James", null);
        List<Author> testAuthors = Arrays.asList(author1, author2);
        when(authorDAO.findAll()).thenReturn(testAuthors);
        List<Author> foundAuthors = authorService.findAll();
        assertEquals(testAuthors, foundAuthors);
        verify(authorDAO, times(1)).findAll();
        verifyNoMoreInteractions(authorDAO);
    }

    @Test(expected = ServiceException.class)
    public void testFindAllWithException() throws Exception {
        doThrow(DAOException.class).when(authorDAO).findAll();
        authorService.findAll();
        verify(authorDAO, times(1)).findAll();
        verifyNoMoreInteractions(authorDAO);
    }

    @Test
    public void testFindById() throws Exception {
        Author author1 = new Author(1L, "John", null);
        Author author2 = new Author(2L, "James", null);
        when(authorDAO.findById(1L)).thenReturn(author1);
        when(authorDAO.findById(2L)).thenReturn(author2);
        assertEquals(author1, authorService.findById(1L));
        verify(authorDAO, times(1)).findById(1L);
        verifyNoMoreInteractions(authorDAO);
    }

    @Test(expected = ServiceException.class)
    public void testFindByIdWithException() throws Exception {
        Long id = 1L;
        doThrow(DAOException.class).when(authorDAO).findById(id);
        authorService.findById(id);
        verify(authorDAO, times(1)).findById(id);
        verifyNoMoreInteractions(authorDAO);
    }

    @Test
    public void testFindByNewsId() throws Exception {
        Author author1 = new Author(1L, "John", null);
        when(authorDAO.findByNewsId(1L)).thenReturn(author1);
        assertEquals(author1, authorService.findByNewsId(1L));
        verify(authorDAO, times(1)).findByNewsId(1L);
        verifyNoMoreInteractions(authorDAO);
    }

    @Test(expected = ServiceException.class)
    public void testFindByNewsIdWithException() throws Exception {
        doThrow(DAOException.class).when(authorDAO).findByNewsId(1L);
        authorService.findByNewsId(1L);
        verify(authorDAO, times(1)).findByNewsId(1L);
        verifyNoMoreInteractions(authorDAO);
    }

    @Test
    public void testExists() throws Exception {
        Author testAuthor = new Author(1L, "John", null);
        when(authorDAO.findById(testAuthor.getId())).thenReturn(testAuthor);
        assertTrue(authorService.exists(testAuthor));
        verify(authorDAO, times(1)).findById(testAuthor.getId());
        verifyNoMoreInteractions(authorDAO);
    }

    @Test(expected = ServiceException.class)
    public void testExistsWithException() throws Exception {
        Author testAuthor = new Author(1L, "John", null);
        doThrow(DAOException.class)
                .when(authorDAO)
                .findById(testAuthor.getId());
        authorService.exists(testAuthor);
        verify(authorDAO, times(1)).findById(testAuthor.getId());
        verifyNoMoreInteractions(authorDAO);
    }

    @Test
    public void testExpire() throws Exception {
        Author testAuthor = new Author(1L, "John", null);
        when(authorDAO.findById(testAuthor.getId())).thenReturn(testAuthor);
        doNothing().when(authorDAO).update(testAuthor);
        authorService.expire(testAuthor.getId(),
                Timestamp.valueOf(LocalDateTime.now()));
        verify(authorDAO, times(1)).findById(testAuthor.getId());
        verify(authorDAO, times(1)).update(testAuthor);
        verifyNoMoreInteractions(authorDAO);
    }

    @Test(expected = ServiceException.class)
    public void testExpireWithException() throws Exception {
        Author testAuthor = new Author(1L, "John", null);
        when(authorDAO.findById(testAuthor.getId())).thenReturn(testAuthor);
        doThrow(DAOException.class).when(authorDAO).update(testAuthor);
        authorService.expire(testAuthor.getId(),
                Timestamp.valueOf(LocalDateTime.now()));
        verify(authorDAO, times(1)).findById(testAuthor.getId());
        verify(authorDAO, times(1)).update(testAuthor);
        verifyNoMoreInteractions(authorDAO);
    }
}
