<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<link rel="stylesheet"
      type="text/css"
      href="<c:url value="/resources/css/ui.dropdownchecklist.standalone.css" />" />

<script type="text/javascript"
        src="<c:url value="/resources/js/jquery.min.js" />">
</script>
<script
        type="text/javascript"
        src="<c:url value="/resources/js/jquery-ui.js" />">
</script>
<script type="text/javascript"
        src="<c:url value="/resources/js/ui.dropdownchecklist-1.4-min.js" />">
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#multi-select").dropdownchecklist(
            {
                width: 200,
                maxDropHeight: 100,
                emptyText: "<spring:message code="news_list.please_select_tags" />"
            });
    });
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#" + 'page' + ${page}).attr('style', 'background: #363636;border-color: #363636;color:white;');
    });
</script>

<c:url var="rootUrl" value="/admin/news-list/" />

<div class="content">
    <form:form action="${rootUrl}filter" modelAttribute="searchCriteria" method="POST">
            <table class="filter-table">
                <tr>
                    <td>
                        <form:select path="authorId">
                            <form:option value="0">
                                <spring:message code="news_list.please_select_author"/>
                            </form:option>
                            <c:forEach var="authorItem" items="${authorList}">
                                <form:option value="${authorItem.id}">
                                    <c:out value="${authorItem.name}"/>
                                </form:option>
                            </c:forEach>
                        </form:select>
                    </td>
                    <td>
                        <form:select path="tagsId" id="multi-select">
                            <form:options items="${tagList}"
                                          itemValue="id"
                                          itemLabel="name" />
                        </form:select>
                    </td>
                    <td>
                        <input type="submit" value="<spring:message code="news_list.filter" />">
                    </td>
                    <td>
                        <a href="${rootUrl}reset">
                            <button type="button">
                                <spring:message code="news-list.reset"/>
                            </button>
                        </a>
                    </td>
                </tr>
            </table>
        </form:form>

    <form:form action="${rootUrl}delete" modelAttribute="deleteList" method="POST">
        <table class="news-table">
            <tbody>
            <c:forEach var="news" items="${newsList}">
                <tr>
                    <td>
                        <div class="news-in-table">
                            <span class="news-header">
                                <span class="title">
                                    <a href="<c:url value="/admin/news/${news.news.id}/view" />">
                                        <c:out value="${news.news.title}"/>
                                    </a>
                                </span>
                                <span class="date">
                                    <c:set var="format">
                                        <spring:message code="date.pattern"/>
                                    </c:set>
                                    <fmt:formatDate
                                            value="${news.news.modificationDate}"
                                            var="modificationDateString"
                                            pattern="${format}"/>
                                    <c:out value="${modificationDateString}"/>
                                </span>
                                <span class="author">
                                    (<spring:message code="news_list.by"/> <c:out value="${news.author.name}"/>)
                                </span>
                            </span>
                            <br/>
                            <span class="news-text">
                                <c:out value="${news.news.shortText}"/>
                            </span>
                            <span class="news-info">
                                <span class="tags">
                                    <c:forEach var="tagItem" items="${news.tags}" varStatus="loop">
                                        <c:out value="${tagItem.name}"/><c:if test="${!loop.last}">, </c:if>
                                    </c:forEach>
                                </span>
                                <span class="comments">
                                    <spring:message code="news_list.comments"/>:
                                    (<c:out value="${fn:length(news.comments)}"/>)
                                </span>
                                <span class="edit-link">
                                    <a class="newslist-edit"
                                       href="<c:url value="/admin/news/${news.news.id}/edit" />">
                                        <spring:message code="news_list.edit"/>
                                    </a>
                                </span>
                                <span class="delete-checkbox">
                                    <form:checkbox path="ids" value="${news.news.id}" />
                                </span>
                            </span>
                        </div>
                    </td>
                </tr>
            </c:forEach>
            <tr class="delete-tr">
                <td>
                    <div>
                        <button type="submit" class="delete-news-btn">
                            <spring:message code="news_list.delete" />
                        </button>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </form:form>
</div>
<div class="pagination">
    <span class="helper-inline"></span>
        <c:forEach var="count" begin="1" end="${countPages}">
            <a href="${rootUrl}${count}" class="page-link">
                <button id="page${count}" class="page-button" type="button">${count}</button>
            </a>
        </c:forEach>
</div>
