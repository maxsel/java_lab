package com.epam.newsmanagement.domain;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.sql.Timestamp;

public class Author implements Serializable {
    private static final long serialVersionUID = -5524840352599936199L;

    private Long id;

    @NotNull
    @Size(min = 3, max = 30)
    private String name;

    private Timestamp expired;

    public Author() {
    }

    public Author(Long id, String name, Timestamp expired) {
        this.id = id;
        this.name = name;
        this.expired = expired;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timestamp getExpired() {
        return expired;
    }

    public void setExpired(Timestamp expired) {
        this.expired = expired;
    }

    @Override
    @SuppressWarnings("SimplifiableIfStatement")
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Author author = (Author) o;

        if (getId() != null ? !getId().equals(author.getId()) : author.getId() != null)
            return false;
        if (getName() != null ? !getName().equals(author.getName()) : author.getName() != null)
            return false;
        return getExpired() != null ? getExpired().equals(author.getExpired()) : author.getExpired() == null;
    }

    @Override
    public int hashCode() {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + (getName() != null ? getName().hashCode() : 0);
        result = 31 * result + (getExpired() != null ? getExpired().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Author{" + "id=" + id +
                ", name='" + name +
                ", expired=" + expired +
                '}';
    }
}
