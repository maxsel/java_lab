package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.domain.Role;

import java.util.List;

/**
 * DataAccessObject interface with methods
 * for accessing {@link Role}s in the database.
 */
public interface RoleDAO {
    /**
     * Persists a role in the database.
     *
     * @param role role to persist.
     * @throws DAOException if database error occurred
     */
    void insert(Role role) throws DAOException;

    /**
     * Updates all fields of role in database
     * by it's {@link com.epam.newsmanagement.domain.User}'s id.
     *
     * @param role role to update.
     * @throws DAOException if database error occurred
     */
    void updateByUserId(Role role) throws DAOException;

    /**
     * Deletes role from the database.
     *
     * @param role role to delete.
     * @throws DAOException if database error occurred
     */
    void delete(Role role) throws DAOException;

    /**
     * Deletes all records with the given name
     * in {@link Role}s table from the database.
     *
     * @param name name of role to delete.
     * @throws DAOException if database error occurred
     */
    void deleteByName(String name) throws DAOException;

    /**
     * Retrieves all roles from the database.
     *
     * @return {@link List} of all roles.
     * @throws DAOException if database error occurred
     */
    List<Role> findAll() throws DAOException;

    /**
     * Returns the role of the user with the given id.
     *
     * @param userId id of user.
     * @return role for the user with the given id.
     * @throws DAOException if database error occurred
     */
    Role findByUserId(Long userId) throws DAOException;
}
