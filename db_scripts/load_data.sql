delete from NEWS_TAG;
delete from TAG;
delete from NEWS_AUTHOR;
delete from AUTHOR;
delete from USERS;
delete from ROLES;
delete from COMMENTS;
delete from NEWS;

INSERT INTO "NEWSDB"."AUTHOR" (AUTHOR_ID, AUTHOR_NAME) VALUES (1, 'Martin Odersky');
INSERT INTO "NEWSDB"."AUTHOR" (AUTHOR_ID, AUTHOR_NAME) VALUES (2, 'Svetλana Bozhko');
INSERT INTO "NEWSDB"."AUTHOR" (AUTHOR_ID, AUTHOR_NAME) VALUES (3, 'Eugene Burmako');
INSERT INTO "NEWSDB"."AUTHOR" (AUTHOR_ID, AUTHOR_NAME) VALUES (4, 'Erik Meijer');
INSERT INTO "NEWSDB"."AUTHOR" (AUTHOR_ID, AUTHOR_NAME) VALUES (5, 'Yegor Bugayenko');
INSERT INTO "NEWSDB"."AUTHOR" (AUTHOR_ID, AUTHOR_NAME) VALUES (6, 'Alex Kamil');
INSERT INTO "NEWSDB"."AUTHOR" (AUTHOR_ID, AUTHOR_NAME) VALUES (7, 'Adam Fletcher');
INSERT INTO "NEWSDB"."AUTHOR" (AUTHOR_ID, AUTHOR_NAME) VALUES (8, 'Debasish Ghosh');
INSERT INTO "NEWSDB"."AUTHOR" (AUTHOR_ID, AUTHOR_NAME) VALUES (9, 'theiced');
INSERT INTO "NEWSDB"."AUTHOR" (AUTHOR_ID, AUTHOR_NAME) VALUES (10, 'sean cassidy');
INSERT INTO "NEWSDB"."AUTHOR" (AUTHOR_ID, AUTHOR_NAME) VALUES (11, 'Alexander Byndyu');
INSERT INTO "NEWSDB"."AUTHOR" (AUTHOR_ID, AUTHOR_NAME) VALUES (12, 'Marcel Molina');
INSERT INTO "NEWSDB"."AUTHOR" (AUTHOR_ID, AUTHOR_NAME) VALUES (13, 'Miran Lipovača');
INSERT INTO "NEWSDB"."AUTHOR" (AUTHOR_ID, AUTHOR_NAME) VALUES (14, 'H.Brydon');
INSERT INTO "NEWSDB"."AUTHOR" (AUTHOR_ID, AUTHOR_NAME) VALUES (15, 'Jan Fajfr');
INSERT INTO "NEWSDB"."AUTHOR" (AUTHOR_ID, AUTHOR_NAME) VALUES (16, 'Andrew McAfee');
INSERT INTO "NEWSDB"."AUTHOR" (AUTHOR_ID, AUTHOR_NAME) VALUES (17, 'Calvin Zito');
INSERT INTO "NEWSDB"."AUTHOR" (AUTHOR_ID, AUTHOR_NAME) VALUES (18, 'Chuck Hollis');
INSERT INTO "NEWSDB"."AUTHOR" (AUTHOR_ID, AUTHOR_NAME) VALUES (19, 'Chris Curran');
INSERT INTO "NEWSDB"."AUTHOR" (AUTHOR_ID, AUTHOR_NAME) VALUES (20, 'Chirag Mehta');

INSERT INTO "NEWSDB"."USERS" (USER_ID, USER_NAME, LOGIN, PASSWORD) VALUES (1, 'Martin Odersky', 'maxsel', '123456');
INSERT INTO "NEWSDB"."USERS" (USER_ID, USER_NAME, LOGIN, PASSWORD) VALUES (2, 'Svetλana Bozhko', 'Bozhko', 'qwerty');
INSERT INTO "NEWSDB"."USERS" (USER_ID, USER_NAME, LOGIN, PASSWORD) VALUES (3, 'Eugene Burmako', 'Burmako', 'qwerty');
INSERT INTO "NEWSDB"."USERS" (USER_ID, USER_NAME, LOGIN, PASSWORD) VALUES (4, 'Erik Meijer', 'Meijer', 'qwerty');
INSERT INTO "NEWSDB"."USERS" (USER_ID, USER_NAME, LOGIN, PASSWORD) VALUES (5, 'Yegor Bugayenko', 'Bugayenko', 'qwerty');
INSERT INTO "NEWSDB"."USERS" (USER_ID, USER_NAME, LOGIN, PASSWORD) VALUES (6, 'Alex Kamil', 'Kamil', 'qwerty');
INSERT INTO "NEWSDB"."USERS" (USER_ID, USER_NAME, LOGIN, PASSWORD) VALUES (7, 'Adam Fletcher', 'Fletcher', 'qwerty');
INSERT INTO "NEWSDB"."USERS" (USER_ID, USER_NAME, LOGIN, PASSWORD) VALUES (8, 'Debasish Ghosh', 'Ghosh', 'qwerty');
INSERT INTO "NEWSDB"."USERS" (USER_ID, USER_NAME, LOGIN, PASSWORD) VALUES (9, 'theiced', 'theiced', 'qwerty');
INSERT INTO "NEWSDB"."USERS" (USER_ID, USER_NAME, LOGIN, PASSWORD) VALUES (10, 'sean cassidy', 'cassidy', 'qwerty');
INSERT INTO "NEWSDB"."USERS" (USER_ID, USER_NAME, LOGIN, PASSWORD) VALUES (11, 'James Smith', 'Smith', 'yuiop');
INSERT INTO "NEWSDB"."USERS" (USER_ID, USER_NAME, LOGIN, PASSWORD) VALUES (12, 'John Johnson', 'Johnson', 'yuiop');
INSERT INTO "NEWSDB"."USERS" (USER_ID, USER_NAME, LOGIN, PASSWORD) VALUES (13, 'Robert Williams', 'Williams', 'yuiop');
INSERT INTO "NEWSDB"."USERS" (USER_ID, USER_NAME, LOGIN, PASSWORD) VALUES (14, 'Michael Jones', 'Jones', 'yuiop');
INSERT INTO "NEWSDB"."USERS" (USER_ID, USER_NAME, LOGIN, PASSWORD) VALUES (15, 'William Brown', 'Brown', 'yuiop');
INSERT INTO "NEWSDB"."USERS" (USER_ID, USER_NAME, LOGIN, PASSWORD) VALUES (16, 'David Davis', 'Davis', 'yuiop');
INSERT INTO "NEWSDB"."USERS" (USER_ID, USER_NAME, LOGIN, PASSWORD) VALUES (17, 'Richard Miller', 'Miller', 'yuiop');
INSERT INTO "NEWSDB"."USERS" (USER_ID, USER_NAME, LOGIN, PASSWORD) VALUES (18, 'Charles Wilson', 'Wilson', 'yuiop');
INSERT INTO "NEWSDB"."USERS" (USER_ID, USER_NAME, LOGIN, PASSWORD) VALUES (19, 'Joseph Moore', 'Moore', 'yuiop');
INSERT INTO "NEWSDB"."USERS" (USER_ID, USER_NAME, LOGIN, PASSWORD) VALUES (20, 'Thomas Taylor', 'Taylor', 'yuiop');

INSERT INTO "NEWSDB"."ROLES" (USER_ID, ROLE_NAME) VALUES (1, 'ROLE_ADMIN');
INSERT INTO "NEWSDB"."ROLES" (USER_ID, ROLE_NAME) VALUES (2, 'ROLE_ADMIN');
INSERT INTO "NEWSDB"."ROLES" (USER_ID, ROLE_NAME) VALUES (3, 'ROLE_ADMIN');
INSERT INTO "NEWSDB"."ROLES" (USER_ID, ROLE_NAME) VALUES (4, 'ROLE_ADMIN');
INSERT INTO "NEWSDB"."ROLES" (USER_ID, ROLE_NAME) VALUES (5, 'ROLE_ADMIN');
INSERT INTO "NEWSDB"."ROLES" (USER_ID, ROLE_NAME) VALUES (6, 'ROLE_ADMIN');
INSERT INTO "NEWSDB"."ROLES" (USER_ID, ROLE_NAME) VALUES (7, 'ROLE_ADMIN');
INSERT INTO "NEWSDB"."ROLES" (USER_ID, ROLE_NAME) VALUES (8, 'ROLE_ADMIN');
INSERT INTO "NEWSDB"."ROLES" (USER_ID, ROLE_NAME) VALUES (9, 'ROLE_ADMIN');
INSERT INTO "NEWSDB"."ROLES" (USER_ID, ROLE_NAME) VALUES (10, 'ROLE_ADMIN');
INSERT INTO "NEWSDB"."ROLES" (USER_ID, ROLE_NAME) VALUES (11, 'ROLE_USER');
INSERT INTO "NEWSDB"."ROLES" (USER_ID, ROLE_NAME) VALUES (12, 'ROLE_USER');
INSERT INTO "NEWSDB"."ROLES" (USER_ID, ROLE_NAME) VALUES (13, 'ROLE_USER');
INSERT INTO "NEWSDB"."ROLES" (USER_ID, ROLE_NAME) VALUES (14, 'ROLE_USER');
INSERT INTO "NEWSDB"."ROLES" (USER_ID, ROLE_NAME) VALUES (15, 'ROLE_USER');
INSERT INTO "NEWSDB"."ROLES" (USER_ID, ROLE_NAME) VALUES (16, 'ROLE_USER');
INSERT INTO "NEWSDB"."ROLES" (USER_ID, ROLE_NAME) VALUES (17, 'ROLE_USER');
INSERT INTO "NEWSDB"."ROLES" (USER_ID, ROLE_NAME) VALUES (18, 'ROLE_USER');
INSERT INTO "NEWSDB"."ROLES" (USER_ID, ROLE_NAME) VALUES (19, 'ROLE_USER');
INSERT INTO "NEWSDB"."ROLES" (USER_ID, ROLE_NAME) VALUES (20, 'ROLE_USER');

INSERT INTO "NEWSDB"."TAG" (TAG_ID, TAG_NAME) VALUES (1, 'java');
INSERT INTO "NEWSDB"."TAG" (TAG_ID, TAG_NAME) VALUES (2, 'scala');
INSERT INTO "NEWSDB"."TAG" (TAG_ID, TAG_NAME) VALUES (3, 'programming');
INSERT INTO "NEWSDB"."TAG" (TAG_ID, TAG_NAME) VALUES (4, 'functional');
INSERT INTO "NEWSDB"."TAG" (TAG_ID, TAG_NAME) VALUES (5, 'oop');
INSERT INTO "NEWSDB"."TAG" (TAG_ID, TAG_NAME) VALUES (6, 'devzen');
INSERT INTO "NEWSDB"."TAG" (TAG_ID, TAG_NAME) VALUES (7, 'razborpoletov');
INSERT INTO "NEWSDB"."TAG" (TAG_ID, TAG_NAME) VALUES (8, 'scaladays');
INSERT INTO "NEWSDB"."TAG" (TAG_ID, TAG_NAME) VALUES (9, 'rx');
INSERT INTO "NEWSDB"."TAG" (TAG_ID, TAG_NAME) VALUES (10, 'datascience');
INSERT INTO "NEWSDB"."TAG" (TAG_ID, TAG_NAME) VALUES (11, 'mooc');
INSERT INTO "NEWSDB"."TAG" (TAG_ID, TAG_NAME) VALUES (12, 'education');
INSERT INTO "NEWSDB"."TAG" (TAG_ID, TAG_NAME) VALUES (13, 'blog');
INSERT INTO "NEWSDB"."TAG" (TAG_ID, TAG_NAME) VALUES (14, 'nosql');
INSERT INTO "NEWSDB"."TAG" (TAG_ID, TAG_NAME) VALUES (15, 'justatag');
INSERT INTO "NEWSDB"."TAG" (TAG_ID, TAG_NAME) VALUES (16, 'bsuir');
INSERT INTO "NEWSDB"."TAG" (TAG_ID, TAG_NAME) VALUES (17, 'devby');
INSERT INTO "NEWSDB"."TAG" (TAG_ID, TAG_NAME) VALUES (18, 'twitter');
INSERT INTO "NEWSDB"."TAG" (TAG_ID, TAG_NAME) VALUES (19, 'quora');
INSERT INTO "NEWSDB"."TAG" (TAG_ID, TAG_NAME) VALUES (20, 'haskell');
INSERT INTO "NEWSDB"."TAG" (TAG_ID, TAG_NAME) VALUES (21, 'actors');
INSERT INTO "NEWSDB"."TAG" (TAG_ID, TAG_NAME) VALUES (22, 'storage');
INSERT INTO "NEWSDB"."TAG" (TAG_ID, TAG_NAME) VALUES (23, 'business');
INSERT INTO "NEWSDB"."TAG" (TAG_ID, TAG_NAME) VALUES (24, 'bigdata');
INSERT INTO "NEWSDB"."TAG" (TAG_ID, TAG_NAME) VALUES (25, 'lasttag');

INSERT INTO "NEWSDB"."NEWS" (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE)
  VALUES (1, 'MULTIVERSAL EQUALITY FOR SCALA', 
  'I have been working recently on making equality tests using == and != safer in Scala...',
  'I have been working recently on making equality tests using == and != safer in Scala. This has led to a Language Enhancement Proposal which I summarize in this blog.',
  TO_TIMESTAMP ('10-09-2015 14:10:10.123000', 'DD-MM-RRRR HH24:MI:SS.FF'), TO_DATE('15-01-2016 08:30:25', 'DD-MM-YYYY HH24:MI:SS'));
INSERT INTO "NEWSDB"."NEWS" (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE)
  VALUES (2, 'Immutability in Scala', 
  'So you’ve decided to write some Scala...',
  'So you’ve decided to write some Scala. You’ve been sold on the power and flexibility of the language, you’ve gotten over the initial hurdles of the new syntax, and you’re ready to start writing clean, functional code. Besides, you’ve already mastered a handful of other common languages. How hard can it be to pick up one more?',
  TO_TIMESTAMP ('25-05-2016 04:11:16.123000', 'DD-MM-RRRR HH24:MI:SS.FF'), TO_DATE('25-05-2016 14:11:16', 'DD-MM-YYYY HH24:MI:SS'));
INSERT INTO "NEWSDB"."NEWS" (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE)
  VALUES (3, 'London', 
  'Attended Google office in London...',
  'Attended Google office in London. Yep, its a very cool and nice place, but... open space.',
  TO_TIMESTAMP ('05-06-2016 14:21:16.123000', 'DD-MM-RRRR HH24:MI:SS.FF'), TO_DATE('05-06-2016 14:21:16', 'DD-MM-YYYY HH24:MI:SS'));
INSERT INTO "NEWSDB"."NEWS" (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE)
  VALUES (4, 'Scala Meta', 
  'Lured @odersky to my talk with promise to implement new macro annots...',
  'Lured @odersky to my talk with promise to implement new macro annots. Follow https://github.com/scalameta/paradise … to see if I can pull it off ',
  TO_TIMESTAMP ('15-05-2016 19:25:46.123000', 'DD-MM-RRRR HH24:MI:SS.FF'), TO_DATE('15-05-2016 19:25:46', 'DD-MM-YYYY HH24:MI:SS'));
INSERT INTO "NEWSDB"."NEWS" (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE)
  VALUES (5, 'Unicorns', 
  'Unicorns thrive on Rx...',
  'Unicorns thrive on Rx. Last week we released RxGroups 0.3.1 with a new API. If you use RxJava on Android, its worth taking a look: https://github.com/airbnb/RxGroups',
  TO_TIMESTAMP ('15-01-2016 09:29:06.123000', 'DD-MM-RRRR HH24:MI:SS.FF'), TO_DATE('15-01-2016 09:29:06', 'DD-MM-YYYY HH24:MI:SS'));
INSERT INTO "NEWSDB"."NEWS" (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE)
  VALUES (6, 'Microservices', 
  'Keynote about Microservices in real world...',
  'Keynote about Microservices in real world. Slides from #javaday #keynote about microservices. Everyone has heard about microservices.',
  TO_TIMESTAMP ('16-05-2016 12:29:06.100000', 'DD-MM-RRRR HH24:MI:SS.FF'), TO_DATE('16-05-2016 12:29:06', 'DD-MM-YYYY HH24:MI:SS'));
INSERT INTO "NEWSDB"."NEWS" (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE)
  VALUES (7, '11 Mistakes Conferences', 
  'I was talking yesterday with a few friends who were software conference organizers...',
  'I was talking yesterday with a few friends who were software conference organizers. They were asking about my opinion of the conferences I''ve recently attended.',
  TO_TIMESTAMP ('03-06-2016 02:29:06.100000', 'DD-MM-RRRR HH24:MI:SS.FF'), TO_DATE('05-06-2016 12:49:06', 'DD-MM-YYYY HH24:MI:SS'));
INSERT INTO "NEWSDB"."NEWS" (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE)
  VALUES (8, 'How I become a data scientist?', 
  'Strictly speaking, there is no such thing as "data science"...',
  'Strictly speaking, there is no such thing as "data science". Here are some resources I''ve collected about working with data, I hope you find them useful  (note: I''m an undergrad student, this is not an expert opinion in any way).',
  TO_TIMESTAMP ('04-06-2016 02:29:06.100000', 'DD-MM-RRRR HH24:MI:SS.FF'), TO_DATE('04-06-2016 02:29:06', 'DD-MM-YYYY HH24:MI:SS'));
INSERT INTO "NEWSDB"."NEWS" (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE)
  VALUES (9, 'You Say You Want An Education?', 
  'With the recent announcement of 17 new schools participating in...',
  'With the recent announcement of 17 new schools participating in the MOOC site Coursera.org, I thought it would be an interesting exercise to see if it was possible to design a reasonable computer science curriculum using just Coursera courses, where “reasonable” is a curriculum that roughly mirrors the coursework required for a four-year university computer science degree.',
  TO_TIMESTAMP ('05-06-2016 12:29:06.100000', 'DD-MM-RRRR HH24:MI:SS.FF'), TO_DATE('05-06-2016 12:29:06', 'DD-MM-YYYY HH24:MI:SS'));
INSERT INTO "NEWSDB"."NEWS" (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE)
  VALUES (10, '2011 - The year that was', 
  'The very first thing that strikes me as I start writing a personal account of 2011 as...',
  'The very first thing that strikes me as I start writing a personal account of 2011 as it was is how it has successfully infused some of the transformations in my regular chores of programming world.',
  TO_TIMESTAMP ('06-06-2016 22:20:06.100000', 'DD-MM-RRRR HH24:MI:SS.FF'), TO_DATE('06-06-2016 22:20:06', 'DD-MM-YYYY HH24:MI:SS'));
INSERT INTO "NEWSDB"."NEWS" (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE)
  VALUES (11, 'ИТ-техникум: а нужен ли БГУИР?', 
  'Ни для кого не секрет что высшего образования в Беларуси нет...',
  'Ни для кого не секрет что высшего образования в Беларуси нет, а если говорить про IT то и не было его никогда. Можно возразить что "выпускники БГУИР пользуются большим спросом на рынке труда" (c), но не надо забывать что спросом они пользуются на местных индофермах которым всё равно что продавать по головам.',
  TO_TIMESTAMP ('07-06-2016 22:00:06.100000', 'DD-MM-RRRR HH24:MI:SS.FF'), TO_DATE('07-06-2016 22:00:06', 'DD-MM-YYYY HH24:MI:SS'));
INSERT INTO "NEWSDB"."NEWS" (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE)
  VALUES (12, 'Better Java', 
  'Java is one of the most popular programming languages around, but no one seems to enjoy using it...',
  'Java is one of the most popular programming languages around, but no one seems to enjoy using it. Well, Java is actually an alright programming language, and since Java 8 came out recently, I decided to compile a list of libraries, practices, and tools to make using Java better.',
  TO_TIMESTAMP ('08-06-2016 10:02:06.000000', 'DD-MM-RRRR HH24:MI:SS.FF'), TO_DATE('08-06-2016 10:02:06', 'DD-MM-YYYY HH24:MI:SS'));
INSERT INTO "NEWSDB"."NEWS" (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE)
  VALUES (13, 'Принцип открытости/закрытости', 
  'Формулировка: программные сущности (классы, модули, функции и т.д.) должны быть...',
  'Формулировка: программные сущности (классы, модули, функции и т.д.) должны быть открыты для расширения, но закрыты для изменения. Какую цель мы преследуем, когда применяем этот принцип? Как известно программные проекты в течение свой жизни постоянно изменяются. Изменения могут возникнуть, например, из-за новых требований заказчика или пересмотра старых. В конечном итоге потребуется изменить код в соответствии с текущей ситуацией.',
  TO_TIMESTAMP ('09-06-2016 16:02:06.000000', 'DD-MM-RRRR HH24:MI:SS.FF'), TO_DATE('09-06-2016 16:02:06', 'DD-MM-YYYY HH24:MI:SS'));
INSERT INTO "NEWSDB"."NEWS" (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE)
  VALUES (14, 'What are good books?', 
  'Keep in mind that becoming expert in OO programming is largely achieved...',
  'Keep in mind that becoming expert in OO programming is largely achieved through writing and reading a lot of code. Over time, when you sit down to solve the next problem, you''ll naturally try to figure out how to avoid the stuff from the last code you wrote that was tedious, error prone and complicated.',
  TO_TIMESTAMP ('09-06-2016 19:02:06.000000', 'DD-MM-RRRR HH24:MI:SS.FF'), TO_DATE('09-06-2016 19:02:06', 'DD-MM-YYYY HH24:MI:SS'));
INSERT INTO "NEWSDB"."NEWS" (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE)
  VALUES (15, 'Learn You a Haskell!', 
  'Haskell is a purely functional programming language....',
  'Haskell is a purely functional programming language. In imperative languages you get things done by giving the computer a sequence of tasks and then it executes them. While executing them, it can change state. For instance, you set variable a to 5 and then do some stuff and then set it to something else.',
  TO_TIMESTAMP ('10-06-2016 06:42:06.000000', 'DD-MM-RRRR HH24:MI:SS.FF'), TO_DATE('10-06-2016 06:42:06', 'DD-MM-YYYY HH24:MI:SS'));
INSERT INTO "NEWSDB"."NEWS" (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE)
  VALUES (16, 'Actor Programming Model', 
  'Software developers are starting to wake up to the notion that concurrency and parallel...',
  'Software developers are starting to wake up to the notion that concurrency and parallel processing are becoming more important as the industry matures.  I recently studied the Actor Programming Model in a little bit of detail and am intrigued by its simplicity and robustness. The Actor Programming model is a concurrent programming technique which provides a powerful mechanism for encapsulating several concurrency features.',
  TO_TIMESTAMP ('12-06-2016 16:02:06.000000', 'DD-MM-RRRR HH24:MI:SS.FF'), TO_DATE('12-06-2016 16:02:06', 'DD-MM-YYYY HH24:MI:SS'));
INSERT INTO "NEWSDB"."NEWS" (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE)
  VALUES (17, 'Programming for Cloud', 
  'This post discusses the current state of art programming languages and...',
  'This post discusses the current state of art programming languages and the future of programming languages in general. The need for distributed software forces us to write complicated software. Which are the language constructs to help us with the challenge?',
  TO_TIMESTAMP ('13-06-2016 13:56:36.000000', 'DD-MM-RRRR HH24:MI:SS.FF'), TO_DATE('13-06-2016 13:56:36', 'DD-MM-YYYY HH24:MI:SS'));
INSERT INTO "NEWSDB"."NEWS" (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE)
  VALUES (18, 'Composable Data Fabric', 
  'There were announcements aplenty happening this week in Las Vegas...',
  'There were announcements aplenty happening this week in Las Vegas. During HPE Discover we announced our vision for where StoreVirtual storage is going within the software-defined data center and introduced a new term: composable data fabric.',
  TO_TIMESTAMP ('20-06-2016 05:35:30.000000', 'DD-MM-RRRR HH24:MI:SS.FF'), TO_DATE('20-06-2016 05:35:30', 'DD-MM-YYYY HH24:MI:SS'));
INSERT INTO "NEWSDB"."NEWS" (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE)
  VALUES (19, 'Windfall for Incumbents?', 
  'Guest post by Dean Nicolacakis, Principal, Co-leader FinTech Practice...',
  'Guest post by Dean Nicolacakis, Principal, Co-leader FinTech Practice, PwC In the next five years the financial services industry will look unrecognizable. In fact, workers are worried about losing their jobs, according to a new survey.',
  TO_TIMESTAMP ('26-06-2016 15:35:30.000000', 'DD-MM-RRRR HH24:MI:SS.FF'), TO_DATE('26-06-2016 15:35:30', 'DD-MM-YYYY HH24:MI:SS'));
INSERT INTO "NEWSDB"."NEWS" (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE)
  VALUES (20, 'Trying to be nice', 
  'No, I didn’t say this, but 56,033 developers in 173 countries who responded...',
  'No, I didn’t say this, but 56,033 developers in 173 countries who responded to recent Stack Overflow''s developer survey did.  I have always enjoyed going through these surveys to validate my several hypotheses and learn new things. I would strongly encourage you to go through the results from the most recent survey here.',
  TO_TIMESTAMP ('01-07-2016 12:22:37.050000', 'DD-MM-RRRR HH24:MI:SS.FF'), TO_DATE('01-07-2016 12:22:37', 'DD-MM-YYYY HH24:MI:SS'));

INSERT INTO "NEWSDB"."COMMENTS" (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
  VALUES (1, 1, 'Wow, it''s great!!!', TO_DATE('10-09-2015 15:10:10', 'DD-MM-YYYY HH24:MI:SS'));
INSERT INTO "NEWSDB"."COMMENTS" (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
  VALUES (2, 1, 'Yep, it is', TO_DATE('10-09-2015 16:10:10', 'DD-MM-YYYY HH24:MI:SS'));
INSERT INTO "NEWSDB"."COMMENTS" (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
  VALUES (3, 1, 'Amen', TO_DATE('10-09-2015 17:10:10', 'DD-MM-YYYY HH24:MI:SS'));
INSERT INTO "NEWSDB"."COMMENTS" (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
  VALUES (4, 2, 'Immutability!', TO_DATE('25-05-2016 14:11:16', 'DD-MM-YYYY HH24:MI:SS'));
INSERT INTO "NEWSDB"."COMMENTS" (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
  VALUES (5, 2, 'Cool', TO_DATE('25-05-2016 15:11:16', 'DD-MM-YYYY HH24:MI:SS'));
INSERT INTO "NEWSDB"."COMMENTS" (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
  VALUES (6, 2, 'Yeaa', TO_DATE('25-05-2016 16:11:16', 'DD-MM-YYYY HH24:MI:SS'));
INSERT INTO "NEWSDB"."COMMENTS" (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
  VALUES (7, 2, 'Ololo xD', TO_DATE('25-05-2016 17:11:16', 'DD-MM-YYYY HH24:MI:SS'));
INSERT INTO "NEWSDB"."COMMENTS" (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
  VALUES (8, 4, 'What is scalameta?', TO_DATE('15-05-2016 19:30:46', 'DD-MM-YYYY HH24:MI:SS'));
INSERT INTO "NEWSDB"."COMMENTS" (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
  VALUES (9, 6, 'Great, thank you', TO_DATE('16-05-2016 13:29:06', 'DD-MM-YYYY HH24:MI:SS'));
INSERT INTO "NEWSDB"."COMMENTS" (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
  VALUES (10, 6, 'Like microservices', TO_DATE('16-05-2016 14:29:06', 'DD-MM-YYYY HH24:MI:SS'));
INSERT INTO "NEWSDB"."COMMENTS" (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
  VALUES (11, 7, 'You rock!', TO_DATE('03-06-2016 12:29:06', 'DD-MM-YYYY HH24:MI:SS'));
INSERT INTO "NEWSDB"."COMMENTS" (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
  VALUES (12, 7, 'Like it', TO_DATE('04-06-2016 05:29:06', 'DD-MM-YYYY HH24:MI:SS'));
INSERT INTO "NEWSDB"."COMMENTS" (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
  VALUES (13, 9, 'Hate coursera', TO_DATE('05-06-2016 13:29:06', 'DD-MM-YYYY HH24:MI:SS'));
INSERT INTO "NEWSDB"."COMMENTS" (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
  VALUES (14, 11, 'Конечно', TO_DATE('07-06-2016 22:02:06', 'DD-MM-YYYY HH24:MI:SS'));
INSERT INTO "NEWSDB"."COMMENTS" (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
  VALUES (15, 11, 'Нет', TO_DATE('07-06-2016 22:10:06', 'DD-MM-YYYY HH24:MI:SS'));
INSERT INTO "NEWSDB"."COMMENTS" (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
  VALUES (16, 11, 'Боже какой бред, я устал', TO_DATE('07-06-2016 23:55:06', 'DD-MM-YYYY HH24:MI:SS'));
INSERT INTO "NEWSDB"."COMMENTS" (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
  VALUES (17, 16, 'Actors are cool', TO_DATE('12-06-2016 18:02:06', 'DD-MM-YYYY HH24:MI:SS'));
INSERT INTO "NEWSDB"."COMMENTS" (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
  VALUES (18, 20, 'Almost', TO_DATE('01-07-2016 13:22:37', 'DD-MM-YYYY HH24:MI:SS'));
INSERT INTO "NEWSDB"."COMMENTS" (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
  VALUES (19, 20, 'Done', TO_DATE('01-07-2016 14:22:37', 'DD-MM-YYYY HH24:MI:SS'));
INSERT INTO "NEWSDB"."COMMENTS" (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
  VALUES (20, 20, 'Last comment! Voila!', TO_DATE('01-07-2016 15:22:37', 'DD-MM-YYYY HH24:MI:SS'));

INSERT INTO "NEWSDB"."NEWS_AUTHOR" (NEWS_ID, AUTHOR_ID) VALUES (1, 1);
INSERT INTO "NEWSDB"."NEWS_AUTHOR" (NEWS_ID, AUTHOR_ID) VALUES (2, 2);
INSERT INTO "NEWSDB"."NEWS_AUTHOR" (NEWS_ID, AUTHOR_ID) VALUES (3, 3);
INSERT INTO "NEWSDB"."NEWS_AUTHOR" (NEWS_ID, AUTHOR_ID) VALUES (4, 4);
INSERT INTO "NEWSDB"."NEWS_AUTHOR" (NEWS_ID, AUTHOR_ID) VALUES (5, 5);
INSERT INTO "NEWSDB"."NEWS_AUTHOR" (NEWS_ID, AUTHOR_ID) VALUES (6, 6);
INSERT INTO "NEWSDB"."NEWS_AUTHOR" (NEWS_ID, AUTHOR_ID) VALUES (7, 7);
INSERT INTO "NEWSDB"."NEWS_AUTHOR" (NEWS_ID, AUTHOR_ID) VALUES (8, 8);
INSERT INTO "NEWSDB"."NEWS_AUTHOR" (NEWS_ID, AUTHOR_ID) VALUES (9, 9);
INSERT INTO "NEWSDB"."NEWS_AUTHOR" (NEWS_ID, AUTHOR_ID) VALUES (10, 10);
INSERT INTO "NEWSDB"."NEWS_AUTHOR" (NEWS_ID, AUTHOR_ID) VALUES (11, 10);
INSERT INTO "NEWSDB"."NEWS_AUTHOR" (NEWS_ID, AUTHOR_ID) VALUES (12, 10);
INSERT INTO "NEWSDB"."NEWS_AUTHOR" (NEWS_ID, AUTHOR_ID) VALUES (13, 13);
INSERT INTO "NEWSDB"."NEWS_AUTHOR" (NEWS_ID, AUTHOR_ID) VALUES (14, 13);
INSERT INTO "NEWSDB"."NEWS_AUTHOR" (NEWS_ID, AUTHOR_ID) VALUES (15, 15);
INSERT INTO "NEWSDB"."NEWS_AUTHOR" (NEWS_ID, AUTHOR_ID) VALUES (16, 16);
INSERT INTO "NEWSDB"."NEWS_AUTHOR" (NEWS_ID, AUTHOR_ID) VALUES (17, 17);
INSERT INTO "NEWSDB"."NEWS_AUTHOR" (NEWS_ID, AUTHOR_ID) VALUES (18, 18);
INSERT INTO "NEWSDB"."NEWS_AUTHOR" (NEWS_ID, AUTHOR_ID) VALUES (19, 18);
INSERT INTO "NEWSDB"."NEWS_AUTHOR" (NEWS_ID, AUTHOR_ID) VALUES (20, 20);

INSERT INTO "NEWSDB"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES (1, 1);
INSERT INTO "NEWSDB"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES (2, 2);
INSERT INTO "NEWSDB"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES (2, 1);
INSERT INTO "NEWSDB"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES (3, 3);
INSERT INTO "NEWSDB"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES (3, 1);
INSERT INTO "NEWSDB"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES (4, 4);
INSERT INTO "NEWSDB"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES (4, 1);
INSERT INTO "NEWSDB"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES (5, 5);
INSERT INTO "NEWSDB"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES (5, 1);
INSERT INTO "NEWSDB"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES (6, 6);
INSERT INTO "NEWSDB"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES (7, 7);
INSERT INTO "NEWSDB"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES (8, 8);
INSERT INTO "NEWSDB"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES (8, 1);
INSERT INTO "NEWSDB"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES (9, 9);
INSERT INTO "NEWSDB"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES (10, 10);
INSERT INTO "NEWSDB"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES (11, 11);
INSERT INTO "NEWSDB"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES (12, 12);
INSERT INTO "NEWSDB"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES (13, 10);
INSERT INTO "NEWSDB"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES (13, 1);
INSERT INTO "NEWSDB"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES (14, 14);
INSERT INTO "NEWSDB"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES (14, 2);
INSERT INTO "NEWSDB"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES (15, 15);
INSERT INTO "NEWSDB"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES (16, 16);
INSERT INTO "NEWSDB"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES (17, 17);
INSERT INTO "NEWSDB"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES (17, 2);
INSERT INTO "NEWSDB"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES (18, 18);
INSERT INTO "NEWSDB"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES (19, 19);
INSERT INTO "NEWSDB"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES (20, 20);
INSERT INTO "NEWSDB"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES (20, 17);
INSERT INTO "NEWSDB"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES (10, 1);
INSERT INTO "NEWSDB"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES (11, 1);
INSERT INTO "NEWSDB"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES (11, 2);
INSERT INTO "NEWSDB"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES (12, 2);

COMMIT;