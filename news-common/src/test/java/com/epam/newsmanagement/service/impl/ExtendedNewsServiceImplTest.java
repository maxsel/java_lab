package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.service.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ExtendedNewsServiceImplTest {
    @Mock
    private NewsService newsService;

    @Mock
    private AuthorService authorService;

    @Mock
    private TagService tagService;

    @Mock
    private CommentService commentService;

    @Mock
    private ExtendedNewsService extendedNewsService;

    @Before
    public void init() {
        extendedNewsService = new ExtendedNewsServiceImpl(newsService,
                authorService, tagService, commentService);
    }

    @Test
    public void testCreate() throws Exception {
        News news = new News(1L, "title", "shortText", "fullText", null, null);
        Long authorId = 4L;
        List<Long> tagIds = Arrays.asList(2L, 3L);
        when(newsService.create(news)).thenReturn(news.getId());
        extendedNewsService.create(news, authorId, tagIds);
        verify(newsService, times(1)).create(news);
        verify(newsService, times(1)).linkNewsAndAuthor(news.getId(), authorId);
        verify(newsService, times(1)).linkNewsAndTags(news.getId(), tagIds);
        verifyNoMoreInteractions(newsService);
        verifyNoMoreInteractions(authorService);
        verifyNoMoreInteractions(tagService);
        verifyNoMoreInteractions(commentService);
    }

    @Test
    public void testUpdate() throws Exception {
        News news = new News(1L, "title", "shortText", "fullText", null, null);
        Author oldAuthor = new Author(1L, "John", null);
        Long authorId = 4L;
        List<Tag> oldTags = Arrays.asList(new Tag(1L, "a"), new Tag(2L, "b"));
        List<Long> tagIds = Arrays.asList(2L, 3L);
        doNothing().when(newsService).update(news);
        doReturn(oldAuthor).when(authorService).findByNewsId(news.getId());
        doReturn(oldTags).when(tagService).findByNewsId(news.getId());
        extendedNewsService.update(news, authorId, tagIds);
        verify(newsService, times(1)).update(news);
        verify(authorService, times(1)).findByNewsId(news.getId());
        verify(tagService, times(1)).findByNewsId(news.getId());
        verify(newsService, times(1)).unlinkNewsAndAuthor(news.getId());
        verify(newsService, times(1)).linkNewsAndAuthor(news.getId(), authorId);
        verify(newsService, times(1)).unlinkNewsAndTags(news.getId());
        verify(newsService, times(1)).linkNewsAndTags(news.getId(), tagIds);
        verifyNoMoreInteractions(newsService);
        verifyNoMoreInteractions(authorService);
        verifyNoMoreInteractions(tagService);
        verifyNoMoreInteractions(commentService);
    }

    @Test
    public void testFindById() throws Exception {
        News news = new News(1L, "title", "shortText", "fullText", null, null);
        Author author = new Author(1L, "John", null);
        List<Tag> tags = Arrays.asList(new Tag(1L, "a"), new Tag(2L, "b"));
        List<Comment> comments = Collections.singletonList(new Comment());
        doReturn(news).when(newsService).findById(news.getId());
        doReturn(author).when(authorService).findByNewsId(news.getId());
        doReturn(tags).when(tagService).findByNewsId(news.getId());
        doReturn(comments).when(commentService).findByNewsId(news.getId());
        extendedNewsService.findById(news.getId());
        verify(newsService, times(1)).findById(news.getId());
        verify(authorService, times(1)).findByNewsId(news.getId());
        verify(tagService, times(1)).findByNewsId(news.getId());
        verify(commentService, times(1)).findByNewsId(news.getId());
        verifyNoMoreInteractions(newsService);
        verifyNoMoreInteractions(authorService);
        verifyNoMoreInteractions(tagService);
        verifyNoMoreInteractions(commentService);
    }

    @Test
    public void testFindAll() throws Exception {
        News news = new News(1L, "title", "shortText", "fullText", null, null);
        Author author = new Author(1L, "John", null);
        List<Tag> tags = Arrays.asList(new Tag(1L, "a"), new Tag(2L, "b"));
        List<Comment> comments = Collections.singletonList(new Comment());
        doReturn(Collections.singletonList(news)).when(newsService).findAll();
        doReturn(author).when(authorService).findByNewsId(news.getId());
        doReturn(tags).when(tagService).findByNewsId(news.getId());
        doReturn(comments).when(commentService).findByNewsId(news.getId());
        extendedNewsService.findAll();
        verify(newsService, times(1)).findAll();
        verify(authorService, times(1)).findByNewsId(news.getId());
        verify(tagService, times(1)).findByNewsId(news.getId());
        verify(commentService, times(1)).findByNewsId(news.getId());
        verifyNoMoreInteractions(newsService);
        verifyNoMoreInteractions(authorService);
        verifyNoMoreInteractions(tagService);
        verifyNoMoreInteractions(commentService);
    }

}
