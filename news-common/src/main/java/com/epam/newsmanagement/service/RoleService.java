package com.epam.newsmanagement.service;

import com.epam.newsmanagement.domain.Role;

import java.util.List;

/**
 * Service interface with methods
 * encapsulating business logic concerned with {@link Role}s
 * and transaction management.
 */
@SuppressWarnings("unused")
public interface RoleService {

    /**
     * Creates new role.
     *
     * @param role role to create.
     * @throws ServiceException if an error occurred when performing operation
     */
    void create(Role role) throws ServiceException;

    /**
     * Edits existing role by its user's id.
     *
     * @param role role to update.
     * @throws ServiceException if an error occurred when performing operation
     */
    void updateByUserId(Role role) throws ServiceException;

    /**
     * Deletes role
     *
     * @param role role to delete.
     * @throws ServiceException if an error occurred when performing operation
     */
    void delete(Role role) throws ServiceException;

    /**
     * Deletes all roles with the given name.
     *
     * @param name name of role to delete.
     * @throws ServiceException if an error occurred when performing operation
     */
    void deleteByName(String name) throws ServiceException;

    /**
     * Retrieves list of all existing roles.
     *
     * @return {@link List} of roles.
     * @throws ServiceException if an error occurred when performing operation
     */
    List<Role> findAll() throws ServiceException;

    /**
     * Returns the role of the user with the given id.
     *
     * @param userId id of user.
     * @return role for the user with the given id.
     * @throws ServiceException if an error occurred when performing operation
     */
    Role findByUserId(Long userId) throws ServiceException;
}
