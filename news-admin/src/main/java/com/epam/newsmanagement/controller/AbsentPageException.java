package com.epam.newsmanagement.controller;

public class AbsentPageException extends RuntimeException {
    private static final long serialVersionUID = 5829625105909863683L;

    public AbsentPageException() {
    }

    public AbsentPageException(String message) {
        super(message);
    }

    public AbsentPageException(String message, Throwable cause) {
        super(message, cause);
    }

    public AbsentPageException(Throwable cause) {
        super(cause);
    }

    public AbsentPageException(String message,
                               Throwable cause,
                               boolean enableSuppression,
                               boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
