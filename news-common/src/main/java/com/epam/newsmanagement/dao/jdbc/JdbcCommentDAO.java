package com.epam.newsmanagement.dao.jdbc;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.dao.DAOException;
import com.epam.newsmanagement.domain.Comment;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import javax.inject.Inject;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * Data Access Object with methods
 * for accessing {@link Comment}s in the database.
 */
@Repository
@SuppressWarnings("Duplicates")
public class JdbcCommentDAO implements CommentDAO {

    private static final String SQL_INSERT =
            "INSERT INTO COMMENTS(COMMENT_ID, COMMENT_TEXT, CREATION_DATE, NEWS_ID)" +
            " VALUES (COMMENTS_SEQ.nextval, ?, ?, ?)";
    private static final String SQL_UPDATE_BY_ID =
            "UPDATE COMMENTS" +
            " SET COMMENT_TEXT = ?, CREATION_DATE = ?, NEWS_ID = ?" +
            " WHERE COMMENT_ID = ?";
    private static final String SQL_DELETE_BY_ID =
            "DELETE COMMENTS WHERE COMMENT_ID = ?";
    private static final String SQL_SELECT_BY_ID =
            "SELECT COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE" +
            " FROM COMMENTS" +
            " WHERE COMMENT_ID = ?";
    private static final String SQL_SELECT_ALL =
            "SELECT COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE" +
            " FROM COMMENTS" +
            " ORDER BY CREATION_DATE";
    private static final String SQL_SELECT_BY_NEWS_ID =
            "SELECT COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE" +
            " FROM COMMENTS" +
            " WHERE NEWS_ID = ?" +
            " ORDER BY CREATION_DATE";
    private static final String COLUMN_COMMENT_ID = "COMMENT_ID";
    private static final String COLUMN_COMMENT_TEXT = "COMMENT_TEXT";
    private static final String COLUMN_CREATION_DATE = "CREATION_DATE";
    private static final String COLUMN_NEWS_ID = "NEWS_ID";

    private final DataSource dataSource;

    @Inject
    public JdbcCommentDAO(DataSource dataSource) {
        Assert.notNull(dataSource, "DataSource must be not null!");
        this.dataSource = dataSource;
    }

    /**
     * Persists comment in the database.
     *
     * @param comment comment to persist.
     * @return id of inserted comment.
     * @throws DAOException if database error occurred
     */
    @Override
    public Long insert(Comment comment) throws DAOException {
        if (comment == null) return null;
        Long insertedId = null;
        String[] generated = {COLUMN_COMMENT_ID};
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps =
                     connection.prepareStatement(SQL_INSERT, generated)) {
            ps.setString(1, comment.getText());
            ps.setTimestamp(2, comment.getCreationDate());
            ps.setLong(3, comment.getNewsId());
            ps.executeUpdate();
            ResultSet generatedKeys = ps.getGeneratedKeys();
            if (generatedKeys.next()) {
                insertedId = generatedKeys.getLong(1);
            } else {
                throw new DAOException("Exception in JdbcCommentDAO#insert(Comment);" +
                        " arguments: [" + comment + "];" +
                        " message: No ID generated");
            }
        } catch (SQLException e) {
            throw new DAOException("Exception in JdbcCommentDAO#insert(Comment);" +
                    " arguments: [" + comment + "];" +
                    " nested exception: [" + e + "]");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return insertedId;
    }

    /**
     * Updates all fields of comment in database by id.
     *
     * @param comment comment to update.
     * @throws DAOException if database error occurred
     */
    @Override
    public void update(Comment comment) throws DAOException {
        if (comment == null || comment.getId() == null) return;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps =
                     connection.prepareStatement(SQL_UPDATE_BY_ID)) {
            ps.setString(1, comment.getText());
            ps.setTimestamp(2, comment.getCreationDate());
            ps.setLong(3, comment.getNewsId());
            ps.setLong(4, comment.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Exception in JdbcCommentDAO#update(Comment);" +
                    " arguments: [" + comment + "];" +
                    " nested exception: [" + e + "]");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * Deletes comment by id.
     *
     * @param id id of comment to delete.
     * @throws DAOException if database error occurred
     */
    @Override
    public void delete(Long id) throws DAOException {
        if (id == null) return;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps =
                     connection.prepareStatement(SQL_DELETE_BY_ID)) {
            ps.setLong(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Exception in JdbcCommentDAO#delete(Long);" +
                    " arguments: [" + id + "];" +
                    " nested exception: [" + e + "]");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * Retrieves all comments from the needed table of the database.
     *
     * @return {@link List} of comments.
     * @throws DAOException if database error occurred
     */
    @Override
    public List<Comment> findAll() throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps =
                     connection.prepareStatement(SQL_SELECT_ALL);
             ResultSet rs = ps.executeQuery()) {
            return fetchCommentsListFromResultSet(rs);
        } catch (SQLException e) {
            throw new DAOException("Exception in JdbcCommentDAO#findAll();" +
                    " arguments: [" + "];" +
                    " nested exception: [" + e + "]");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * Retrieves comment with the given id.
     *
     * @param id id of comment to retrieve.
     * @return comment with the given id.
     * @throws DAOException if database error occurred
     */
    @Override
    public Comment findById(Long id) throws DAOException {
        if (id == null) return null;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps =
                     connection.prepareStatement(SQL_SELECT_BY_ID)) {
            ps.setLong(1, id);
            ResultSet rs = ps.executeQuery();
            return fetchCommentFromResultSet(rs);
        } catch (SQLException e) {
            throw new DAOException("Exception in JdbcCommentDAO#findById(Long);" +
                    " arguments: [" + id + "];" +
                    " nested exception: [" + e + "]");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Comment> findByNewsId(Long newsId) throws DAOException {
        if (newsId == null) return new LinkedList<>();
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps =
                     connection.prepareStatement(SQL_SELECT_BY_NEWS_ID)) {
            ps.setLong(1, newsId);
            ResultSet rs = ps.executeQuery();
            return fetchCommentsListFromResultSet(rs);
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    private Comment fetchCommentFromResultSet(ResultSet rs)
            throws SQLException {
        Comment comment = null;
        while (rs.next()) {
            comment = new Comment();
            comment.setId(rs.getLong(COLUMN_COMMENT_ID));
            comment.setText(rs.getString(COLUMN_COMMENT_TEXT));
            comment.setCreationDate(rs.getTimestamp(COLUMN_CREATION_DATE));
            comment.setNewsId(rs.getLong(COLUMN_NEWS_ID));
        }
        return comment;
    }

    private List<Comment> fetchCommentsListFromResultSet(ResultSet rs)
            throws SQLException {
        List<Comment> list = new LinkedList<>();
        while (rs.next()) {
            Comment comment = new Comment();
            comment.setId(rs.getLong(COLUMN_COMMENT_ID));
            comment.setText(rs.getString(COLUMN_COMMENT_TEXT));
            comment.setCreationDate(rs.getTimestamp(COLUMN_CREATION_DATE));
            comment.setNewsId(rs.getLong(COLUMN_NEWS_ID));
            list.add(comment);
        }
        return list;
    }


}
