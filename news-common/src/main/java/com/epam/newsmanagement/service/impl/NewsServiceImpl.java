package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.DAOException;
import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.SearchCriteria;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.inject.Inject;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.util.List;

/**
 * Service with methods
 * encapsulating business logic concerned with {@link News}s
 * and transaction management.
 */
@Service
public class NewsServiceImpl implements NewsService {

    private static final Logger LOG = LogManager.getLogger(NewsServiceImpl.class);

    private final NewsDAO newsDAO;

    @Inject
    public NewsServiceImpl(NewsDAO newsDAO) {
        Assert.notNull(newsDAO, "NewsDAO must be not null!");
        this.newsDAO = newsDAO;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long create(News news) throws ServiceException {
        try {
            // If news' creation date is NULL, set it to current date.
            if (news.getCreationDate() == null) {
                news.setCreationDate(Timestamp.from(Instant.now()));
            }
            news.setModificationDate(new Date(news.getCreationDate().getTime()));
            return newsDAO.insert(news);
        } catch (DAOException e) {
            LOG.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void update(News news) throws ServiceException {
        try {
            // If news' creation date is NULL, set it to current date.
            Date now = Date.valueOf(LocalDate.now());
            Date creationDate = news.getCreationDate() != null
                                    ? new Date(news.getCreationDate().getTime())
                                    : now;

            // If news' creation date, set by admin, is greater than current date
            // (i.e., it's set to the date in future), modification date is not
            // updated to be equal to modification date, so that
            // modification date was not greater than creation date.
            if (now.compareTo(creationDate) >= 0) {
                news.setModificationDate(now);
            } else {
                news.setModificationDate(creationDate);
            }
            newsDAO.update(news);
        } catch (DAOException e) {
            LOG.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(rollbackFor = ServiceException.class)
    public void delete(Long id) throws ServiceException {
        try {
            newsDAO.unlinkNewsAuthor(id);
            newsDAO.unlinkNewsTags(id);
            newsDAO.deleteNewsComments(id);
            newsDAO.delete(id);
        } catch (DAOException e) {
            LOG.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<News> findAll() throws ServiceException {
        try {
            return newsDAO.findAll();
        } catch (DAOException e) {
            LOG.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public News findById(Long id) throws ServiceException {
        try {
            return newsDAO.findById(id);
        } catch (DAOException e) {
            LOG.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<News> findByCriteria(SearchCriteria criteria)
            throws ServiceException {
        try {
            return newsDAO.findByCriteria(criteria);
        } catch (DAOException e) {
            LOG.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<News> findByCriteria(SearchCriteria criteria, int page)
            throws ServiceException {
        try {
            return newsDAO.findByCriteria(criteria, NEWS_PER_PAGE*(page-1)+1, NEWS_PER_PAGE);
        } catch (DAOException e) {
            LOG.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void linkNewsAndAuthor(Long newsId, Long authorId)
            throws ServiceException {
        try {
            newsDAO.linkNewsAndAuthor(newsId, authorId);
        } catch (DAOException e) {
            LOG.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void unlinkNewsAndAuthor(Long newsId) throws ServiceException {
        try {
            newsDAO.unlinkNewsAuthor(newsId);
        } catch (DAOException e) {
            LOG.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void linkNewsAndTag(Long newsId, Long tagId) throws ServiceException {
        try {
            newsDAO.linkNewsAndTag(newsId, tagId);
        } catch (DAOException e) {
            LOG.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void unlinkNewsAndTag(Long newsId, Long tagId) throws ServiceException {
        try {
            newsDAO.unlinkNewsAndTag(newsId, tagId);
        } catch (DAOException e) {
            LOG.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void linkNewsAndTags(Long newsId, List<Long> tagIds) throws ServiceException {
        try {
            newsDAO.linkNewsTags(newsId, tagIds);
        } catch (DAOException e) {
            LOG.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void unlinkNewsAndTags(Long newsId) throws ServiceException {
        try {
            newsDAO.unlinkNewsTags(newsId);
        } catch (DAOException e) {
            LOG.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long count() throws ServiceException {
        try {
            return newsDAO.count();
        } catch (DAOException e) {
            LOG.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long count(SearchCriteria criteria) throws ServiceException {
        try {
            return newsDAO.count(criteria);
        } catch (DAOException e) {
            LOG.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long findPrev(Long id, SearchCriteria criteria) throws ServiceException {
        try {
            return newsDAO.findPrev(id, criteria);
        } catch (DAOException e) {
            LOG.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long findNext(Long id, SearchCriteria criteria) throws ServiceException {
        try {
            return newsDAO.findNext(id, criteria);
        } catch (DAOException e) {
            LOG.error(e);
            throw new ServiceException(e);
        }
    }
}
