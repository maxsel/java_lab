<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>News Portal</title>
    <fmt:setLocale value="${sessionScope.lang}" scope="session"/>
    <fmt:setBundle basename="label"/>
    <link rel="stylesheet" type="text/css"
          href="<c:url value="/resources/css/styles.css" />"/>
</head>

<body>
<jsp:include page="header.jsp"/>
<table border="1" class="layout-table"
       style="margin-top: 5%;margin-bottom:1.9%;">
    <tr class="body-tr">
        <td class="body-td">
            <div class="back-container">
                <div class="back" style="display: inline-block;">
                    <form action="news-client" style="display: inline-block;" class="edit-link">
                        <input type="hidden" name="command" value="shownewslist">
                        <button type="submit" class="submitLink">
                            <fmt:message key="news.back"/>
                        </button>
                    </form>
                </div>
            </div>
            <div class="news-container">
                <div class="news-content">
                    <span class="news-title">
                        <c:out value="${news.news.title}"/><br/>
                    </span>
                    <span class="news-author">
                        (<fmt:message key="news_list.by"/> <c:out value="${news.author.name}"/>)
                    </span>
                    <span class="news-date">
                        <c:set var="format"><fmt:message key="date.pattern"/></c:set>
                        <fmt:formatDate value="${news.news.modificationDate}"
                                        var="modificationDateString"
                                        pattern="${format}"/>
                        <c:out value="${modificationDateString}"/>
                    </span>
                    <br/>
                    <span class="news-fulltext">
                        <c:out value="${news.news.fullText}"/>
                    </span>
                    <c:forEach var="comment" items="${news.comments}">
                        <div class="comment">
                            <div class="comment-date">
                                <fmt:formatDate value="${comment.creationDate}"
                                                var="commentDateString"
                                                pattern="${format}"/>
                                <c:out value="${commentDateString}"/>
                            </div>
                            <div class="comment-box">
                                <span class="comment-text">
                                    <c:out value="${comment.text}"/>
                                </span>
                            </div>
                        </div>
                    </c:forEach>
                    <div class="post-form" style="margin-top: 20px;width:51.5%">
                        <form action="/news-client" method="POST">
                            <input type="hidden" name="command" value="postcomment">
                            <textarea name="commentText"
                                      class="post-comm-text" rows="4" cols="80"
                                      minlength="3" maxlength="100"
                                      title=""></textarea>
                            <div class="post-button">
                                <input class="post-button-text"
                                       type="submit" formaction="news-client"
                                       value="<fmt:message key="news.post_comment" />"/>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="prev-next-container">
                <div class="prev-next">
                    <div class="prev" style="display: inline-block;">
                        <c:if test="${not empty prevNewsId}">
                            <form action="news-client" style="display: inline-block;" class="edit-link">
                                <input type="hidden" name="command" value="shownews">
                                <button type="submit"
                                        class="submitLink"
                                        name="newsId"
                                        value="<c:out value="${prevNewsId}" />" >
                                    < <fmt:message key="news.prev"/>
                                </button>
                            </form>
                        </c:if>
                    </div>
                    <div class="next" style="display: inline-block;">
                        <c:if test="${not empty nextNewsId}">
                            <form action="news-client" style="display: inline-block;" class="edit-link">
                                <input type="hidden" name="command" value="shownews">
                                <button type="submit"
                                        class="submitLink"
                                        name="newsId"
                                        value="<c:out value="${nextNewsId}" />" >
                                    <fmt:message key="news.next"/> >
                                </button>
                            </form>
                        </c:if>
                    </div>
                </div>
            </div>
        </td>
    </tr>
</table>
<jsp:include page="footer.jsp"/>
</body>
</html>