package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.command.Command;
import com.epam.newsmanagement.command.CommandFactory;
import com.epam.newsmanagement.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

public class Controller extends HttpServlet {

    private static final Logger LOG = LogManager.getLogger(Controller.class);
    private static final long serialVersionUID = -4486029843838216259L;

    public Controller() {
    }

    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request,
                                HttpServletResponse response)
            throws ServletException, IOException {
        try {
            Command command = CommandFactory.defineCommand(request);
            String page = command.execute(request);
            if (command.needsRedirect()) {
                response.sendRedirect(page);
            } else {
                getServletContext()
                        .getRequestDispatcher(page)
                        .forward(request, response);
            }
        } catch (ServiceException e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            LOG.error(sw.toString(), e);
            //request.setAttribute(REQUEST_ATTR_EXCEPTION, e.getClass().toString());
            request.setAttribute("message", e.getMessage());
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }
}