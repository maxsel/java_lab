package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.domain.User;

/**
 * Data Access Object interface with methods
 * for accessing {@link User}s in the database.
 */
public interface UserDAO extends GenericDAO<Long, User> {
}
