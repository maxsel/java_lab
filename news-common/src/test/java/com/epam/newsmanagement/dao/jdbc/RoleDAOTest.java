package com.epam.newsmanagement.dao.jdbc;

import com.epam.newsmanagement.dao.DAOException;
import com.epam.newsmanagement.dao.RoleDAO;
import com.epam.newsmanagement.domain.Role;
import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.annotation.ExpectedDataSet;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.unitils.spring.annotation.SpringBeanByType;

import static org.junit.Assert.assertEquals;

@DataSet("roledao/dataset.xml")
@SpringApplicationContext("test-context.xml")
public class RoleDAOTest extends UnitilsJUnit4 {

    @SpringBeanByType
    private RoleDAO roleDAO;

    @Test
    @ExpectedDataSet("roledao/dataset-expected-insert.xml")
    public void testInsert() throws DAOException {
        Role role = new Role(1L, "role4");
        roleDAO.insert(role);
    }

    @Test
    @ExpectedDataSet("roledao/dataset-expected-updateByUserId.xml")
    public void testUpdateByUserId() throws DAOException {
        Role role = roleDAO.findByUserId(1L);
        role.setName("Updated");
        roleDAO.updateByUserId(role);
    }

    @Test
    @ExpectedDataSet("roledao/dataset-expected-delete.xml")
    public void testDelete() throws DAOException {
        Role role = roleDAO.findByUserId(1L);
        roleDAO.delete(role);
    }

    @Test
    @ExpectedDataSet("roledao/dataset-expected-deleteByName.xml")
    public void testDeleteByName() throws DAOException {
        roleDAO.deleteByName("role1");
    }

    @Test
    public void testFindAll() throws DAOException {
        assertEquals(3, roleDAO.findAll().size());
    }

    @Test
    public void testFindByUserId() throws DAOException {
        assertEquals("role1", roleDAO.findByUserId(1L).getName());
    }
}
