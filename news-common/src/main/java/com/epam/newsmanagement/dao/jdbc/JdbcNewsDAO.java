package com.epam.newsmanagement.dao.jdbc;

import com.epam.newsmanagement.dao.DAOException;
import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.SearchCriteria;
import com.epam.newsmanagement.utils.SearchCriteriaUtils;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import javax.inject.Inject;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * Data Access Object with methods
 * for accessing {@link News}s in the database.
 */
@Repository
@SuppressWarnings("Duplicates")
public class JdbcNewsDAO implements NewsDAO {

    private static final String SQL_INSERT_NEWS =
            "INSERT INTO NEWS(NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE)" +
            " VALUES (NEWS_SEQ.nextval, ?, ?, ?, ?, ?)";
    private static final String SQL_INSERT_NEWS_AUTHOR =
            "INSERT INTO NEWS_AUTHOR(NEWS_ID, AUTHOR_ID) VALUES (?, ?)";
    private static final String SQL_INSERT_NEWS_TAG =
            "INSERT INTO NEWS_TAG(NEWS_ID, TAG_ID) VALUES (?, ?)";
    private static final String SQL_UPDATE_BY_ID =
            "UPDATE NEWS" +
            " SET TITLE = ?, SHORT_TEXT = ?, FULL_TEXT = ?, CREATION_DATE = ?, MODIFICATION_DATE = ?" +
            " WHERE NEWS_ID = ?";
    private static final String SQL_DELETE_BY_ID = "DELETE NEWS WHERE NEWS_ID = ?";
    private static final String SQL_SELECT_BY_ID =
            "SELECT NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE" +
            " FROM NEWS WHERE NEWS_ID = ?";
    private static final String SQL_DELETE_NEWS_AUTHOR =
            "DELETE FROM NEWS_AUTHOR WHERE NEWS_ID = ?";
    private static final String SQL_DELETE_NEWS_TAGS =
            "DELETE FROM NEWS_TAG WHERE NEWS_ID = ?";
    private static final String SQL_DELETE_NEWS_TAG =
            "DELETE FROM NEWS_TAG WHERE NEWS_ID = ? AND TAG_ID = ?";
    private static final String SQL_DELETE_NEWS_COMMENTS =
            "DELETE FROM COMMENTS WHERE NEWS_ID = ?";
    private static final String SQL_COUNT = "SELECT COUNT(*) FROM NEWS";
    private static final String COLUMN_NEWS_ID = "NEWS_ID";
    private static final String COLUMN_TITLE = "TITLE";
    private static final String COLUMN_SHORT_TEXT = "SHORT_TEXT";
    private static final String COLUMN_FULL_TEXT = "FULL_TEXT";
    private static final String COLUMN_CREATION_DATE = "CREATION_DATE";
    private static final String COLUMN_MODIFICATION_DATE = "MODIFICATION_DATE";
    private static final String COLUMN_NEXT = "NEXT";
    private static final String COLUMN_PREV = "PREV";

    private final DataSource dataSource;

    @Inject
    public JdbcNewsDAO(DataSource dataSource) {
        Assert.notNull(dataSource, "DataSource must be not null!");
        this.dataSource = dataSource;
    }

    /**
     * Persists a news in the database.
     *
     * @param news new to persist.
     * @return id of inserted item.
     * @throws DAOException if database error occurred
     */
    @Override
    public Long insert(News news) throws DAOException {
        if (news == null) return null;
        Long insertedId = null;
        String[] generated = {COLUMN_NEWS_ID};
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps =
                     connection.prepareStatement(SQL_INSERT_NEWS, generated)) {
            ps.setString(1, news.getTitle());
            ps.setString(2, news.getShortText());
            ps.setString(3, news.getFullText());
            ps.setTimestamp(4, news.getCreationDate());
            ps.setDate(5, news.getModificationDate());
            ps.executeUpdate();
            ResultSet generatedKeys = ps.getGeneratedKeys();
            if (generatedKeys.next()) {
                insertedId = generatedKeys.getLong(1);
            } else {
                throw new DAOException("Exception in JdbcNewsDAO#insert(News);" +
                        " arguments: [" + news + "];" +
                        " message: No ID generated");
            }
        } catch (SQLException e) {
            throw new DAOException("Exception in JdbcNewsDAO#insert(News);" +
                    " arguments: [" + news + "];" +
                    " nested exception: [" + e + "]");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return insertedId;
    }

    /**
     * Updates all fields of a news in database by id.
     *
     * @param news news to update.
     * @throws DAOException if database error occurred
     */
    @Override
    public void update(News news) throws DAOException {
        if (news == null || news.getId() == null) return;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps =
                     connection.prepareStatement(SQL_UPDATE_BY_ID)) {
            ps.setString(1, news.getTitle());
            ps.setString(2, news.getShortText());
            ps.setString(3, news.getFullText());
            ps.setTimestamp(4, news.getCreationDate());
            ps.setDate(5, news.getModificationDate());
            ps.setLong(6, news.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Exception in JdbcNewsDAO#update(News);" +
                    " arguments: [" + news + "];" +
                    " nested exception: [" + e + "]");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * Deletes news by id.
     *
     * @param id id of news to delete.
     * @throws DAOException if database error occurred
     */
    @Override
    public void delete(Long id) throws DAOException {
        if (id == null) return;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps =
                     connection.prepareStatement(SQL_DELETE_BY_ID)) {
            ps.setLong(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Exception in JdbcNewsDAO#delete(Long);" +
                    " arguments: [" + id + "];" +
                    " nested exception: [" + e + "]");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * Retrieves news with the given id.
     *
     * @param id id of news to retrieve.
     * @return news with the given id.
     * @throws DAOException if database error occurred
     */
    @Override
    public News findById(Long id) throws DAOException {
        if (id == null) return null;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps =
                     connection.prepareStatement(SQL_SELECT_BY_ID)) {
            ps.setLong(1, id);
            ResultSet rs = ps.executeQuery();
            return fetchNewsFromResultSet(rs);
        } catch (SQLException e) {
            throw new DAOException("Exception in JdbcNewsDAO#findByid(Long);" +
                    " arguments: [" + id + "];" +
                    " nested exception: [" + e + "]");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * Retrieves all news from the database.
     *
     * @return {@link List} of items, sorted by number of comments.
     * @throws DAOException if database error occurred
     */
    @Override
    public List<News> findAll() throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps =
                     SearchCriteriaUtils.prepareStatement(connection, null)) {
            ResultSet rs = ps.executeQuery();
            return fetchNewsListFromResultSet(rs);
        } catch (SQLException e) {
            throw new DAOException("Exception in JdbcNewsDAO#findAll();" +
                    " arguments: [" + "];" +
                    " nested exception: [" + e + "]");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<News> findByCriteria(SearchCriteria criteria)
            throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps =
                     SearchCriteriaUtils.prepareStatement(connection, criteria)) {
            ResultSet rs = ps.executeQuery();
            return fetchNewsListFromResultSet(rs);
        } catch (SQLException e) {
            throw new DAOException("Exception in JdbcNewsDAO#findByCriteria(SearchCriteria);" +
                    " arguments: [" + criteria + "];" +
                    " nested exception: [" + e + "]");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<News> findByCriteria(SearchCriteria criteria, int offset, int count)
            throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps =
                     SearchCriteriaUtils.prepareStatement(connection, criteria, offset, count)) {
            ResultSet rs = ps.executeQuery();
            return fetchNewsListFromResultSet(rs);
        } catch (SQLException e) {
            throw new DAOException("Exception in JdbcNewsDAO#findByCriteria(SearchCriteria,int,int);" +
                    " arguments: [" + criteria + ", " + offset + ", " + count + "];" +
                    " nested exception: [" + e + "]");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void linkNewsAndAuthor(Long newsId, Long authorId) throws DAOException {
        if (newsId == null || authorId == null) return;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps =
                     connection.prepareStatement(SQL_INSERT_NEWS_AUTHOR)) {
            ps.setLong(1, newsId);
            ps.setLong(2, authorId);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Exception in JdbcNewsDAO#linkNewsAndAuthor(Long,Long);" +
                    " arguments: [" + newsId + ", " + authorId + "];" +
                    " nested exception: [" + e + "]");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void unlinkNewsAuthor(Long newsId) throws DAOException {
        if (newsId == null) return;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps =
                     connection.prepareStatement(SQL_DELETE_NEWS_AUTHOR)) {
            ps.setLong(1, newsId);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Exception in JdbcNewsDAO#unlinkNewsAuthor(Long);" +
                    " arguments: [" + newsId + "];" +
                    " nested exception: [" + e + "]");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void linkNewsAndTag(Long newsId, Long tagId) throws DAOException {
        if (newsId == null || tagId == null) return;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps =
                     connection.prepareStatement(SQL_INSERT_NEWS_TAG)) {
            ps.setLong(1, newsId);
            ps.setLong(2, tagId);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Exception in JdbcNewsDAO#linkNewsAndTag(Long,Long);" +
                    " arguments: [" + newsId + ", " + tagId + "];" +
                    " nested exception: [" + e + "]");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void unlinkNewsAndTag(Long newsId, Long tagId) throws DAOException {
        if (newsId == null || tagId == null) return;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps =
                     connection.prepareStatement(SQL_DELETE_NEWS_TAG)) {
            ps.setLong(1, newsId);
            ps.setLong(2, tagId);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Exception in JdbcNewsDAO#unlinkNewsAndTag(Long,Long);" +
                    " arguments: [" + newsId + ", " + tagId + "];" +
                    " nested exception: [" + e + "]");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void linkNewsTags(Long newsId, List<Long> tagIds)
            throws DAOException {
        if (tagIds == null) return;
        for (Long tagId : tagIds) {
            linkNewsAndTag(newsId, tagId);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void unlinkNewsTags(Long newsId) throws DAOException {
        if (newsId == null) return;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps =
                     connection.prepareStatement(SQL_DELETE_NEWS_TAGS)) {
            ps.setLong(1, newsId);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Exception in JdbcNewsDAO#unlinkNewsTags(Long);" +
                    " arguments: [" + newsId + "];" +
                    " nested exception: [" + e + "]");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteNewsComments(Long newsId) throws DAOException {
        if (newsId == null) return;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps =
                     connection.prepareStatement(SQL_DELETE_NEWS_COMMENTS)) {
            ps.setLong(1, newsId);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Exception in JdbcNewsDAO#unlinkNewsTags(Long);" +
                    " arguments: [" + newsId + "];" +
                    " nested exception: [" + e + "]");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long count() throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SQL_COUNT)) {
            long count = 0;
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                count = rs.getLong(1);
            }
            return count;
        } catch (SQLException e) {
            throw new DAOException("Exception in JdbcNewsDAO#count();" +
                    " arguments: [" + "];" +
                    " nested exception: [" + e + "]");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long count(SearchCriteria criteria) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps =
                     SearchCriteriaUtils.prepareStatementForCount(connection, criteria)) {
            long count = 0;
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                count = rs.getLong(1);
            }
            return count;
        } catch (SQLException e) {
            throw new DAOException("Exception in JdbcNewsDAO#count(SearchCriteria);" +
                    " arguments: [" + criteria + "];" +
                    " nested exception: [" + e + "]");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long findPrev(Long id, SearchCriteria criteria) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps =
                     SearchCriteriaUtils.prepareStatementForNextPrev(connection, criteria, id)) {
            long prev = 0;
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                prev = rs.getLong(COLUMN_PREV);
            }
            return prev;
        } catch (SQLException e) {
            throw new DAOException("Exception in JdbcNewsDAO#findPrev(Long,SearchCriteria);" +
                    " arguments: [" + id + ", " + criteria + "];" +
                    " nested exception: [" + e + "]");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long findNext(Long id, SearchCriteria criteria) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps =
                     SearchCriteriaUtils.prepareStatementForNextPrev(connection, criteria, id)) {
            long next = 0;
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                next = rs.getLong(COLUMN_NEXT);
            }
            return next;
        } catch (SQLException e) {
            throw new DAOException("Exception in JdbcNewsDAO#findNext(Long,SearchCriteria);" +
                    " arguments: [" + id + ", " + criteria + "];" +
                    " nested exception: [" + e + "]");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    private News fetchNewsFromResultSet(ResultSet rs) throws SQLException {
        News news = null;
        if (rs.next()) {
            news = new News();
            news.setId(rs.getLong(COLUMN_NEWS_ID));
            news.setTitle(rs.getString(COLUMN_TITLE));
            news.setShortText(rs.getString(COLUMN_SHORT_TEXT));
            news.setFullText(rs.getString(COLUMN_FULL_TEXT));
            news.setCreationDate(rs.getTimestamp(COLUMN_CREATION_DATE));
            news.setModificationDate(rs.getDate(COLUMN_MODIFICATION_DATE));
        }
        return news;
    }

    private List<News> fetchNewsListFromResultSet(ResultSet rs) throws SQLException {
        List<News> list = new LinkedList<>();
        while (rs.next()) {
            News news = new News();
            news.setId(rs.getLong(COLUMN_NEWS_ID));
            news.setTitle(rs.getString(COLUMN_TITLE));
            news.setShortText(rs.getString(COLUMN_SHORT_TEXT));
            news.setFullText(rs.getString(COLUMN_FULL_TEXT));
            news.setCreationDate(rs.getTimestamp(COLUMN_CREATION_DATE));
            news.setModificationDate(rs.getDate(COLUMN_MODIFICATION_DATE));
            list.add(news);
        }
        return list;
    }
}