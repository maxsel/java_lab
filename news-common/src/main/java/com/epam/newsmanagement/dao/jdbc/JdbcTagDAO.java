package com.epam.newsmanagement.dao.jdbc;

import com.epam.newsmanagement.dao.DAOException;
import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.domain.Tag;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import javax.inject.Inject;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * Data Access Object with methods
 * for accessing {@link Tag}s in the database.
 */
@Repository
@SuppressWarnings("Duplicates")
public class JdbcTagDAO implements TagDAO {

    private static final String SQL_INSERT_TAG =
            "INSERT INTO TAG(TAG_ID, TAG_NAME) VALUES (TAG_SEQ.NEXTVAL, ?)";
    private static final String SQL_UPDATE_BY_ID =
            "UPDATE TAG SET TAG_NAME = ? WHERE TAG_ID = ?";
    private static final String SQL_DELETE_BY_ID =
            "DELETE TAG WHERE TAG_ID = ?";
    private static final String SQL_SELECT_ALL =
            "SELECT TAG_ID, TAG_NAME FROM TAG ORDER BY UPPER(TAG_NAME)";
    private static final String SQL_SELECT_BY_ID =
            "SELECT TAG_ID, TAG_NAME FROM TAG WHERE TAG_ID = ?";
    private static final String SQL_SELECT_BY_NAME =
            "SELECT TAG_ID, TAG_NAME FROM TAG WHERE TAG_NAME = ?";
    private static final String SQL_SELECT_BY_NEWS_ID =
            "SELECT TAG.TAG_ID, TAG.TAG_NAME FROM TAG " +
            " INNER JOIN NEWS_TAG ON TAG.TAG_ID = NEWS_TAG.TAG_ID " +
            " WHERE NEWS_ID = ?" +
            " ORDER BY TAG.TAG_NAME";
    private static final String SQL_DELETE_NEWS_TAGS =
            "DELETE FROM NEWS_TAG WHERE TAG_ID = ?";
    private static final String COLUMN_TAG_ID = "TAG_ID";
    private static final String COLUMN_TAG_NAME = "TAG_NAME";

    private final DataSource dataSource;

    @Inject
    public JdbcTagDAO(DataSource dataSource) {
        Assert.notNull(dataSource, "DataSource must be not null!");
        this.dataSource = dataSource;
    }

    /**
     * Persists tag in the database.
     *
     * @param tag tag to persist.
     * @return id of inserted tag.
     * @throws DAOException if database error occurred
     */
    @Override
    public Long insert(Tag tag) throws DAOException {
        if (tag == null) return null;
        Tag existing = findByName(tag.getName());
        if (existing != null) {
            return existing.getId();
        }
        Long insertedId = null;
        String[] generated = {COLUMN_TAG_ID};
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps =
                     connection.prepareStatement(SQL_INSERT_TAG, generated)) {
            ps.setString(1, tag.getName());
            ps.executeUpdate();
            ResultSet generatedKeys = ps.getGeneratedKeys();
            if (generatedKeys.next()) {
                insertedId = generatedKeys.getLong(1);
            } else {
                throw new DAOException("Exception in JdbcTagDAO#insert(Tag);" +
                        " arguments: [" + tag + "];" +
                        " message: No ID generated");
            }
        } catch (SQLException e) {
            throw new DAOException("Exception in JdbcTagDAO#insert(Tag);" +
                    " arguments: [" + tag + "];" +
                    " nested exception: [" + e + "]");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return insertedId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Long> insertAll(List<Tag> tags) throws DAOException {
        if (tags == null) return new LinkedList<>();
        List<Long> ids = new LinkedList<>();
        for (Tag tag : tags) {
            long id = insert(tag);
            ids.add(id);
        }
        return ids;
    }

    /**
     * Updates all fields of tag in database by id.
     *
     * @param tag tag to update.
     * @throws DAOException if database error occurred
     */
    @Override
    public void update(Tag tag) throws DAOException {
        if (tag == null) return;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps =
                     connection.prepareStatement(SQL_UPDATE_BY_ID)) {
            ps.setString(1, tag.getName());
            ps.setLong(2, tag.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Exception in JdbcTagDAO#update(Tag);" +
                    " arguments: [" + tag + "];" +
                    " nested exception: [" + e + "]");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * Deletes tag by id.
     *
     * @param id id of tag to delete.
     * @throws DAOException if database error occurred
     */
    @Override
    public void delete(Long id) throws DAOException {
        if (id == null) return;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps =
                     connection.prepareStatement(SQL_DELETE_BY_ID)) {
            ps.setLong(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Exception in JdbcTagDAO#delete(Long);" +
                    " arguments: [" + id + "];" +
                    " nested exception: [" + e + "]");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * Retrieves all tags from the needed table of the database.
     *
     * @return {@link List} of tags.
     * @throws DAOException if database error occurred
     */
    @Override
    public List<Tag> findAll() throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps =
                     connection.prepareStatement(SQL_SELECT_ALL);
             ResultSet rs = ps.executeQuery()) {
            return fetchTagsListFromResultSet(rs);
        } catch (SQLException e) {
            throw new DAOException("Exception in JdbcTagDAO#findAll();" +
                    " arguments: [" + "];" +
                    " nested exception: [" + e + "]");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * Retrieves tag with the given id.
     *
     * @param id id of tag to retrieve.
     * @return tag with the given id.
     * @throws DAOException if database error occurred
     */
    @Override
    public Tag findById(Long id) throws DAOException {
        if (id == null) return null;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps =
                     connection.prepareStatement(SQL_SELECT_BY_ID)) {
            ps.setLong(1, id);
            ResultSet rs = ps.executeQuery();
            return fetchTagFromResultSet(rs);
        } catch (SQLException e) {
            throw new DAOException("Exception in JdbcTagDAO#findAll(Long);" +
                    " arguments: [" + id + "];" +
                    " nested exception: [" + e + "]");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Tag> findByNewsId(Long newsId) throws DAOException {
        if (newsId == null) return new LinkedList<>();
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps =
                     connection.prepareStatement(SQL_SELECT_BY_NEWS_ID)) {
            ps.setLong(1, newsId);
            ResultSet rs = ps.executeQuery();
            return fetchTagsListFromResultSet(rs);
        } catch (SQLException e) {
            throw new DAOException("Exception in JdbcTagDAO#findByNewsId(Long);" +
                    " arguments: [" + newsId + "];" +
                    " nested exception: [" + e + "]");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Tag findByName(String name) throws DAOException {
        if (name == null) return null;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps =
                     connection.prepareStatement(SQL_SELECT_BY_NAME)) {
            ps.setString(1, name);
            ResultSet rs = ps.executeQuery();
            return fetchTagFromResultSet(rs);
        } catch (SQLException e) {
            throw new DAOException("Exception in JdbcTagDAO#findByName(String);" +
                    " arguments: [" + name + "];" +
                    " nested exception: [" + e + "]");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void unlink(Long id) throws DAOException {
        if (id == null) return;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps =
                     connection.prepareStatement(SQL_DELETE_NEWS_TAGS)) {
            ps.setLong(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Exception in JdbcTagDAO#unlink(Long);" +
                    " arguments: [" + id + "];" +
                    " nested exception: [" + e + "]");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    private Tag fetchTagFromResultSet(ResultSet rs)
            throws SQLException {
        Tag tag = null;
        if (rs.next()) {
            tag = new Tag();
            tag.setId(rs.getLong(COLUMN_TAG_ID));
            tag.setName(rs.getString(COLUMN_TAG_NAME));
        }
        return tag;
    }

    private List<Tag> fetchTagsListFromResultSet(ResultSet rs)
            throws SQLException {
        List<Tag> list = new LinkedList<>();
        while (rs.next()) {
            Tag tag = new Tag();
            tag.setId(rs.getLong(COLUMN_TAG_ID));
            tag.setName(rs.getString(COLUMN_TAG_NAME));
            list.add(tag);
        }
        return list;
    }
}
