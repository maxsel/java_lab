package com.epam.newsmanagement.command;

import com.epam.newsmanagement.domain.ExtendedNews;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.SearchCriteria;
import com.epam.newsmanagement.service.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.LinkedList;
import java.util.List;

public class ShowNewsListCommand implements Command {

    private static final Logger LOG = LogManager.getLogger(ShowNewsListCommand.class);

    public String execute(HttpServletRequest request) throws ServiceException {
        ExtendedNewsService extendedNewsService =
                (ExtendedNewsService) context.getBean("extendedNewsService");
        NewsService newsService = (NewsService) context.getBean("newsService");
        AuthorService authorService = (AuthorService) context.getBean("authorService");
        TagService tagService = (TagService) context.getBean("tagService");

        HttpSession session = request.getSession(true);
        SearchCriteria searchCriteria = (SearchCriteria) session.getAttribute("searchCriteria");
        if (searchCriteria == null) {
            searchCriteria = new SearchCriteria();
            session.setAttribute("searchCriteria", searchCriteria);
        }

        int page = 1;
        LOG.debug("Request param [page] = " + request.getParameter("page"));
        if (null != request.getParameter("page")) {
            page = Integer.parseInt(request.getParameter("page"));
        }
        LOG.debug("Page as processed int: " + page);
        List<ExtendedNews> extendedNewsList = new LinkedList<>();
        for (News news : newsService.findByCriteria(searchCriteria, page)) {
            extendedNewsList.add(extendedNewsService.findById(news.getId()));
        }

        request.setAttribute("page", page);
        long countPages = (long) Math.ceil(1.0*newsService.count(searchCriteria)/NewsService.NEWS_PER_PAGE);
        request.setAttribute("countPages", countPages);
        request.setAttribute("newsList", extendedNewsList);
        request.setAttribute("authorList", authorService.findAll());
        request.setAttribute("tagList", tagService.findAll());

        return "/pages/news-list.jsp";
    }
}
