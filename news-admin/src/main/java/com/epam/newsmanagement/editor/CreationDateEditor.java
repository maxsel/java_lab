package com.epam.newsmanagement.editor;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.beans.PropertyEditorSupport;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CreationDateEditor extends PropertyEditorSupport {
    private static final String DATE_FORMAT = "MM/dd/yyyy";
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);

    private static final Logger LOG = LogManager.getLogger(CreationDateEditor.class);

    static {
        dateFormat.setLenient(false);
    }

    // Converts a String to a Timestamp (when submitting form)
    @Override
    public void setAsText(String text) {
        Timestamp timestamp = null;
        try {
            Date parsedDate = dateFormat.parse(text);
            timestamp = new java.sql.Timestamp(parsedDate.getTime());
        } catch (ParseException e) {
            LOG.error(e);
        }
        this.setValue(timestamp);
    }

    // Converts a Timestamp to a String (when displaying form)
    @Override
    public String getAsText() {
        Timestamp timestamp = (Timestamp) this.getValue();
        return timestamp == null ? "" : dateFormat.format(timestamp);
    }
}