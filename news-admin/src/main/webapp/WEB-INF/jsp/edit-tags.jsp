<%--suppress XmlDuplicatedId --%>
<%--suppress XmlPathReference --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<script type="text/javascript"
        src="<c:url value="/resources/js/jquery.min.js" />"></script>
<script type="text/javascript"
        src="<c:url value="/resources/js/tags.js" />"></script>

<c:url value="/admin/tags" var="rootUrl"/>

<div class="items-container">
    <div class="items-content">
        <div class="items-fields">
            <table cellspacing="50">
                <tbody>
                <c:forEach items="${tags}" var="tagItem">
                <form:form method="post" action="${rootUrl}/update"
                           modelAttribute="tag">
                <tr>
                    <td><b> <spring:message code="edit_tags.tag"/>: </b></td>
                    <td>
                        <form:input path="id" value="${tagItem.id}"
                                    type="hidden"/>
                        <form:input
                                path="name" value="${tagItem.name}"
                                id="tag${tagItem.id}"
                                required="required" pattern=".{3,30}"
                                style="width:400" type="text" disabled="true"/>
                        <br/>
                        <form:errors path="name" cssClass="error"/>
                    </td>
                    <td class="edit-item">
                        <a href="javascript:startEditing(${tagItem.id})"
                           id="edit${tagItem.id}">
                            <spring:message code="edit_tags.edit"/>
                        </a>
                        <input id="update${tagItem.id}" name="tagIndex" type="submit" class="submitLink"
                               value="<spring:message code="edit_tags.update"/>" style="display: none" />
                        <a href="<c:url value="/admin/tags/${tagItem.id}/delete" />"
                           id="delete${tagItem.id}"
                           class="link-button" style="display: none">
                            <spring:message code="edit_tags.delete"/>
                        </a>
                        <a href="javascript:void(0);" id="cancel${tagItem.id}"
                           style="display: none">
                            <spring:message code="edit_tags.cancel"/>
                        </a>
                    </td>
                </tr>
                </form:form>
                </c:forEach>
                <tr class="add-item-tr">
                    <form:form method="post" action="${rootUrl}/add"
                               modelAttribute="newTag">
                        <td><b><spring:message code="edit_tags.add_tag"/>: </b>
                        </td>
                        <td>
                            <form:input path="name"
                                        style="width:400"
                                        required="required"
                                        pattern=".{3,30}"/>
                            <br/>
                            <form:errors path="name" cssClass="error"/>
                        </td>
                        <td class="edit-item">
                            <form:button class="submitLink">
                                <spring:message code="edit_tags.save"/>
                            </form:button>
                        </td>
                    </form:form>
                </tr>
            </table>
        </div>
    </div>
</div>