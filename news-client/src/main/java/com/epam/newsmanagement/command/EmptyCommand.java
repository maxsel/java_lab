package com.epam.newsmanagement.command;

import com.epam.newsmanagement.service.ServiceException;

import javax.servlet.http.HttpServletRequest;

public class EmptyCommand implements Command {
    public String execute(HttpServletRequest request) throws ServiceException {
        return "/index.jsp";
    }
}
