package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.domain.*;
import com.epam.newsmanagement.service.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ExtendedNewsServiceImpl implements ExtendedNewsService {

    private static final Logger LOG = LogManager.getLogger(ExtendedNewsServiceImpl.class);

    private final NewsService newsService;

    private final AuthorService authorService;

    private final TagService tagService;

    private final CommentService commentService;

    @Inject
    public ExtendedNewsServiceImpl(NewsService newsService,
                                   AuthorService authorService,
                                   TagService tagService,
                                   CommentService commentService) {
        Assert.notNull(newsService, "NewsService must be not null!");
        Assert.notNull(authorService, "AuthorService must be not null!");
        Assert.notNull(tagService, "TagService must be not null!");
        Assert.notNull(commentService, "CommentService must be not null!");
        this.newsService = newsService;
        this.authorService = authorService;
        this.tagService = tagService;
        this.commentService = commentService;
    }

    @Override
    @Transactional(rollbackFor = ServiceException.class)
    public Long create(News news, Long authorId, List<Long> tagIds)
            throws ServiceException {
        long newsId = newsService.create(news);
        newsService.linkNewsAndAuthor(newsId, authorId);
        newsService.linkNewsAndTags(newsId, tagIds);
        return newsId;
    }

    @Override
    public ExtendedNews findById(Long id)
            throws ServiceException {
        ExtendedNews extendedNews = new ExtendedNews();
        try {
            News news = newsService.findById(id);
            Author author = authorService.findByNewsId(id);
            List<Tag> tags = tagService.findByNewsId(id);
            List<Comment> comments = commentService.findByNewsId(id);
            extendedNews.setNews(news);
            extendedNews.setAuthor(author);
            extendedNews.setTags(tags);
            extendedNews.setComments(comments);
            return extendedNews;
        } catch (ServiceException e) {
            LOG.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public List<ExtendedNews> findAll() throws ServiceException {
        List<ExtendedNews> extendedNewsList = new LinkedList<>();
        try {
            List<News> newsList = newsService.findAll();
            for (News news : newsList) {
                ExtendedNews extendedNews = new ExtendedNews();
                Long id = news.getId();
                Author author = authorService.findByNewsId(id);
                List<Tag> tags = tagService.findByNewsId(id);
                List<Comment> comments = commentService.findByNewsId(id);
                extendedNews.setNews(news);
                extendedNews.setAuthor(author);
                extendedNews.setTags(tags);
                extendedNews.setComments(comments);
                extendedNewsList.add(extendedNews);
            }
            return extendedNewsList;
        } catch (ServiceException e) {
            LOG.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    @Transactional(rollbackFor = ServiceException.class)
    public void update(News news, Long authorId, List<Long> tagIds)
            throws ServiceException {
        newsService.update(news);
        Author oldAuthor = authorService.findByNewsId(news.getId());
        if (!oldAuthor.getId().equals(authorId)) {
            newsService.unlinkNewsAndAuthor(news.getId());
            newsService.linkNewsAndAuthor(news.getId(), authorId);
        }
        List<Tag> oldTags = tagService.findByNewsId(news.getId());
        if (!oldTags
                .stream()
                .map(Tag::getId)
                .collect(Collectors.toList())
                .equals(tagIds)) {
            newsService.unlinkNewsAndTags(news.getId());
            newsService.linkNewsAndTags(news.getId(), tagIds);
        }
    }
}
