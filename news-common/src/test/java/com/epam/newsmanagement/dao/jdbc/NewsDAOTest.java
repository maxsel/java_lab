package com.epam.newsmanagement.dao.jdbc;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.dao.DAOException;
import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.SearchCriteria;
import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.annotation.ExpectedDataSet;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.unitils.spring.annotation.SpringBeanByType;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@DataSet("newsdao/dataset.xml")
@SpringApplicationContext("test-context.xml")
public class NewsDAOTest extends UnitilsJUnit4 {

    @SpringBeanByType
    private NewsDAO newsDAO;

    @SpringBeanByType
    private AuthorDAO authorDAO;

    @Test
    @ExpectedDataSet("newsdao/dataset-expected-insert.xml")
    public void testInsert() throws DAOException {
        News news = new News(0L, "Inserted", "New short", "New full",
                Timestamp.valueOf("2016-06-10 10:10:10"), Date.valueOf("2016-06-11"));
        newsDAO.insert(news);
    }

    @Test
    @ExpectedDataSet("newsdao/dataset-expected-update.xml")
    public void testUpdate() throws DAOException {
        News news = newsDAO.findById(1L);
        news.setTitle("Updated");
        newsDAO.update(news);
    }

    @Test
    @ExpectedDataSet("newsdao/dataset-expected-delete.xml")
    public void testDelete() throws DAOException {
        newsDAO.delete(4L);
    }

    @Test(expected = DAOException.class)
    public void testDeleteWithConstraintViolation() throws DAOException {
        newsDAO.delete(1L);
    }

    @Test
    public void testFindAll() throws DAOException {
        assertEquals(4, newsDAO.findAll().size());
    }

    @Test
    public void testFindById() throws DAOException {
        assertEquals("Title 1", newsDAO.findById(1L).getTitle());
    }

    @Test
    public void testFindByCriteria() throws DAOException {
        Author author = authorDAO.findById(2L);
        SearchCriteria criteria =
                new SearchCriteria(author.getId(), Arrays.asList(3L, 4L));
        assertTrue(2L == newsDAO.findByCriteria(criteria).size());
    }

    @Test
    public void testFindByCriteriaPaged() throws DAOException {
        Author author = authorDAO.findById(2L);
        SearchCriteria criteria =
                new SearchCriteria(author.getId(), Arrays.asList(3L, 4L));
        assertTrue(1 == newsDAO.findByCriteria(criteria, 1, 1).size());
    }

    @Test
    @ExpectedDataSet("newsdao/dataset-expected-insertNewsAuthor.xml")
    public void testLinkNewsAndAuthor() throws DAOException {
        newsDAO.linkNewsAndAuthor(4L, 2L);
    }

    @Test
    @ExpectedDataSet("newsdao/dataset-expected-unlinkNewsAuthor.xml")
    public void testUnlinkNewsAuthor() throws DAOException {
        newsDAO.unlinkNewsAuthor(1L);
    }

    @Test
    @ExpectedDataSet("newsdao/dataset-expected-insertNewsTag.xml")
    public void testLinkNewsAndTag() throws DAOException {
        newsDAO.linkNewsAndTag(4L, 3L);
    }

    @Test
    @ExpectedDataSet("newsdao/dataset-expected-unlinkNewsAndTag.xml")
    public void testUnlinkNewsAndTag() throws DAOException {
        newsDAO.unlinkNewsAndTag(1L, 4L);
    }

    @Test
    @ExpectedDataSet("newsdao/dataset-expected-insertNewsTags.xml")
    public void testLinkNewsTags() throws DAOException {
        newsDAO.linkNewsTags(4L, Arrays.asList(3L, 4L));
    }

    @Test
    @ExpectedDataSet("newsdao/dataset-expected-unlinkNewsTags.xml")
    public void testUnlinkNewsTags() throws DAOException {
        newsDAO.unlinkNewsTags(1L);
    }

    @Test
    @ExpectedDataSet("newsdao/dataset-expected-deleteNewsComments.xml")
    public void testDeleteNewsComments() throws DAOException {
        newsDAO.deleteNewsComments(1L);
    }

    @Test
    public void testCount() throws DAOException {
        assertTrue(4L == newsDAO.count());
    }

    @Test
    public void testCountWithCriteria() throws DAOException {
        Author author = authorDAO.findById(2L);
        SearchCriteria criteria =
                new SearchCriteria(author.getId(), Arrays.asList(3L, 4L));
        assertTrue(2 == newsDAO.count(criteria));
    }

    @Test
    public void testFindPrev() throws DAOException {
        Author author = authorDAO.findById(2L);
        SearchCriteria criteria =
                new SearchCriteria(author.getId(), Arrays.asList(3L, 4L));
        assertTrue(2L == newsDAO.findPrev(3L, criteria));
    }

    @Test
    public void testFindNext() throws DAOException {
        Author author = authorDAO.findById(2L);
        SearchCriteria criteria =
                new SearchCriteria(author.getId(), Arrays.asList(3L, 4L));
        assertTrue(3L == newsDAO.findNext(2L, criteria));
    }
}
