package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.SearchCriteria;

import java.util.List;

/**
 * Data Access Object interface with methods
 * for accessing {@link News}s in the database.
 */
public interface NewsDAO extends GenericDAO<Long, News> {
    /**
     * Creates relation with news and {@link com.epam.newsmanagement.domain.Author}
     * with the given id's correspondingly.
     *
     * @param newsId   news' id.
     * @param authorId author's id.
     * @throws DAOException if database error occurred
     */
    void linkNewsAndAuthor(Long newsId, Long authorId) throws DAOException;

    /**
     * Removes relation with news
     * and its {@link com.epam.newsmanagement.domain.Author}.
     *
     * @param newsId   news' id.
     * @throws DAOException if database error occurred
     */
    void unlinkNewsAuthor(Long newsId) throws DAOException;

    /**
     * Creates relation with news and {@link com.epam.newsmanagement.domain.Tag}
     * with the given id's correspondingly.
     *
     * @param newsId news' id.
     * @param tagId  tag's id.
     * @throws DAOException if database error occurred
     */
    void linkNewsAndTag(Long newsId, Long tagId) throws DAOException;

    /**
     * Removes relation with news and {@link com.epam.newsmanagement.domain.Tag}
     * with the given id's correspondingly.
     *
     * @param newsId news' id.
     * @param tagId  tag's id.
     * @throws DAOException if database error occurred
     */
    void unlinkNewsAndTag(Long newsId, Long tagId) throws DAOException;

    /**
     * Removes relation with news with the given id
     * and all {@link com.epam.newsmanagement.domain.Tag}s with id's
     * from {@link List}.
     *
     * @param newsId    news' id.
     * @throws DAOException if database error occurred
     */
    void linkNewsTags(Long newsId, List<Long> tagIdList) throws DAOException;

    /**
     * Removes relation with news with the given id
     * and all its {@link com.epam.newsmanagement.domain.Tag}s.
     *
     * @param newsId    news' id.
     * @throws DAOException if database error occurred
     */
    void unlinkNewsTags(Long newsId) throws DAOException;

    /**
     * Deletes all {@link com.epam.newsmanagement.domain.Comment}s
     * for the news with the given id.
     *
     * @param newsId    news' id.
     * @throws DAOException if database error occurred
     */
    void deleteNewsComments(Long newsId) throws DAOException;

    /**
     * Returns the {@link List} of news satisfying the search parameters
     * (author and {@link List} of {@link com.epam.newsmanagement.domain.Tag}s),
     * encapsulated in {@link SearchCriteria} object.
     *
     * @param criteria {@link SearchCriteria} object.
     * @return list of news meeting the criteria.
     * @throws DAOException if database error occurred
     */
    List<News> findByCriteria(SearchCriteria criteria) throws DAOException;

    /**
     * Returns the {@link List} of news satisfying the search parameters
     * (author and {@link List} of {@link com.epam.newsmanagement.domain.Tag}s),
     * encapsulated in {@link SearchCriteria} object; supports pagination.
     *
     * @param criteria {@link SearchCriteria} object.
     * @param offset start pagination index
     * @param count size of page
     * @return list of news meeting the criteria.
     * @throws DAOException if database error occurred
     */
    List<News> findByCriteria(SearchCriteria criteria, int offset, int count) throws DAOException;

    /**
     * Return total count of news in the database.
     *
     * @return count of news.
     * @throws DAOException if database error occurred
     */
    Long count() throws DAOException;

    /**
     * Return total count of news in the database, meeting given criteria.
     *
     * @param criteria criteria {@link SearchCriteria} object.
     * @return count of news.
     * @throws DAOException if database error occurred
     */
    Long count(SearchCriteria criteria) throws DAOException;

    /**
     * Return id of previous news, meeting given criteria.
     *
     * @param criteria criteria {@link SearchCriteria} object.
     * @param id id of news to find previous for.
     * @return id of previous news.
     * @throws DAOException if database error occurred
     */
    Long findPrev(Long id, SearchCriteria criteria) throws DAOException;

    /**
     * Return id of next news, meeting given criteria.
     *
     * @param criteria criteria {@link SearchCriteria} object.
     * @param id id of news to find next for.
     * @return id of next news.
     * @throws DAOException if database error occurred
     */
    Long findNext(Long id, SearchCriteria criteria) throws DAOException;
}
