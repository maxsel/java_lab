<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/ui.dropdownchecklist.standalone.css" />" />

<script type="text/javascript"
        src="<c:url value="/resources/js/jquery.min.js" />"></script>
<script type="text/javascript"
        src="<c:url value="/resources/js/jquery-ui.js" />"></script>
<script type="text/javascript"
        src="<c:url value="/resources/js/ui.dropdownchecklist-1.4-min.js" />"></script>


<script type="text/javascript">
    $(document).ready(function() {
        $("#multi-select").dropdownchecklist(
            {
                width: 300,
                maxDropHeight: 100,
                emptyText: "Click to select tags"
            });
    });
</script>

<c:if test="${operation=='add'}">
    <c:url var="rootUrl" value="/admin/news/add"/>
</c:if>
<c:if test="${operation=='edit'}">
    <c:url var="rootUrl" value="/admin/news/${newsExtended.news.id}/edit"/>
</c:if>

<div class="edit-news-container">
    <div class="edit-news-content">
        <form:form action="${rootUrl}/save" modelAttribute="newsExtended" method="POST">
            <jsp:useBean id="now" class="java.util.Date"/>
            <form:input path="news.id" type="hidden" />
            <form:input path="news.modificationDate" type="hidden" />
            <div class="news-input-fields">
                <table>
                    <tr>
                        <td><spring:message code="edit-news.title"/>:</td>
                        <td><form:input path="news.title" required="required" pattern=".{3,30}"/></td>
                    </tr>
                    <tr>
                        <td><spring:message code="edit-news.date"/>:</td>
                        <td><form:input path="news.creationDate"
                                        pattern="(0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])[- \/.](19|20)\d\d" /></td>
                    </tr>
                    <tr>
                        <td><spring:message code="edit-news.brief"/>:</td>
                        <td><form:textarea path="news.shortText" rows="4" cols="80"
                                           minlength="3" maxlength="100" /></td>
                    </tr>
                    <tr>
                        <td><spring:message code="edit-news.content"/>:</td>
                        <td><form:textarea path="news.fullText" rows="17" cols="80"
                                           minlength="3" maxlength="2000"/></td>
                    </tr>
                </table>
            </div>
            <div class="news-dropdown-fields">
                <table>
                    <tr>
                        <td>
                            <form:select path="author" required="required">
                                <c:forEach items="${authorList}" var="authorItem">
                                    <c:choose>
                                        <c:when test="${newsExtended.author eq authorItem}">
                                            <c:if test="${authorItem.expired lt now && authorItem.id != newsExtended.author.id}">
                                                <form:option value="${authorItem.id}" selected="true" disabled="true">
                                                    <c:out value="${authorItem.name}" />
                                                </form:option>
                                            </c:if>
                                            <c:if test="${!(authorItem.expired lt now && authorItem.id != newsExtended.author.id)}">
                                                <form:option value="${authorItem.id}" selected="true">
                                                    <c:out value="${authorItem.name}" />
                                                </form:option>
                                            </c:if>
                                        </c:when>
                                        <c:otherwise>
                                            <c:if test="${authorItem.expired lt now && authorItem.id != newsExtended.author.id}">
                                                <form:option value="${authorItem.id}" disabled="true">
                                                    <c:out value="${authorItem.name}" />
                                                </form:option>
                                            </c:if>
                                            <c:if test="${!(authorItem.expired lt now && authorItem.id != newsExtended.author.id)}">
                                                <form:option value="${authorItem.id}">
                                                    <c:out value="${authorItem.name}" />
                                                </form:option>
                                            </c:if>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                            </form:select>
                            <form:errors path="author" cssClass="error"/>
                        </td>
                        <td>
                            <form:select path="tags" id="multi-select" multiple="true">
                                <form:options items="${tagList}"
                                              itemValue="id"
                                              itemLabel="name" />
                            </form:select>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="news-save">
                <input type="submit"
                       value="<spring:message code="edit-news.save" />"
                       class="news-save-button">
            </div>
            <br/>
        </form:form>
    </div>
</div>