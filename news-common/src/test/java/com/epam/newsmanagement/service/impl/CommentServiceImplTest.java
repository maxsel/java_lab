package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.dao.DAOException;
import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.ServiceException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings("Duplicates")
public class CommentServiceImplTest {
    @Mock
    private CommentDAO commentDAO;

    private CommentService commentService;

    @Before
    public void init() {
        commentService = new CommentServiceImpl(commentDAO);
    }

    @Test
    public void testCreate() throws Exception {
        when(commentDAO.insert(new Comment())).thenReturn(7L);
        commentService.create(new Comment());
        verify(commentDAO, times(1)).insert(new Comment());
        assertEquals(commentService.create(new Comment()), (Long) 7L);
    }

    @Test(expected = ServiceException.class)
    @SuppressWarnings("unchecked")
    public void testCreateWithException() throws Exception {
        Comment testComment = new Comment();
        when(commentDAO.insert(testComment)).thenThrow(DAOException.class);
        commentService.create(testComment);
        verify(commentDAO, times(1)).insert(testComment);
        verifyNoMoreInteractions(commentDAO);
    }

    @Test
    public void testDelete() throws Exception {
        commentService.delete(0L);
        verify(commentDAO, times(1)).delete(0L);
    }

    @Test(expected = ServiceException.class)
    public void testDeleteWithException() throws Exception {
        Long id = 1L;
        doThrow(DAOException.class).when(commentDAO).delete(id);
        commentService.delete(id);
        verify(commentDAO, times(1)).delete(id);
        verifyNoMoreInteractions(commentDAO);
    }

    @Test
    public void testUpdate() throws Exception {
        commentService.update(new Comment());
        verify(commentDAO, times(1)).update(new Comment());
        verifyNoMoreInteractions(commentDAO);
    }

    @Test(expected = ServiceException.class)
    public void testUpdateWithException() throws Exception {
        Comment testComment = new Comment();
        doThrow(DAOException.class).when(commentDAO).update(testComment);
        commentService.update(testComment);
        verify(commentDAO, times(1)).insert(testComment);
        verifyNoMoreInteractions(commentDAO);
    }

    @Test
    public void testFindById() throws Exception {
        Comment comment1 = new Comment(1L, 1L, "John", null);
        Comment comment2 = new Comment(2L, 1L, "James", null);
        when(commentDAO.findById(1L)).thenReturn(comment1);
        when(commentDAO.findById(2L)).thenReturn(comment2);
        assertEquals(comment1, commentService.findById(1L));
        verify(commentDAO, times(1)).findById(1L);
        verifyNoMoreInteractions(commentDAO);
    }

    @Test(expected = ServiceException.class)
    public void testFindByIdWithException() throws Exception {
        Long id = 1L;
        doThrow(DAOException.class).when(commentDAO).findById(id);
        commentService.findById(id);
        verify(commentDAO, times(1)).findById(id);
        verifyNoMoreInteractions(commentDAO);
    }

    @Test
    public void testFindByNewsId() throws Exception {
        Comment comment1 = new Comment(1L, 1L, "John", null);
        when(commentDAO.findByNewsId(1L))
                .thenReturn(Collections.singletonList(comment1));
        assertTrue(commentService.findByNewsId(1L).contains(comment1));
        verify(commentDAO, times(1)).findByNewsId(1L);
        verifyNoMoreInteractions(commentDAO);
    }

    @Test(expected = ServiceException.class)
    public void testFindByNewsIdWithException() throws Exception {
        doThrow(DAOException.class).when(commentDAO).findByNewsId(1L);
        commentService.findByNewsId(1L);
        verify(commentDAO, times(1)).findByNewsId(1L);
        verifyNoMoreInteractions(commentDAO);
    }
}
