package com.epam.newsmanagement.dao.jdbc;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.dao.DAOException;
import com.epam.newsmanagement.domain.Author;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import javax.inject.Inject;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * Data Access Object with methods
 * for accessing {@link Author}s in the database.
 */
@Repository
@SuppressWarnings("Duplicates")
public class JdbcAuthorDAO implements AuthorDAO {

    private static final String SQL_INSERT =
            "INSERT INTO AUTHOR(AUTHOR_ID, AUTHOR_NAME) VALUES (AUTHOR_SEQ.NEXTVAL, ?)";
    private static final String SQL_UPDATE_BY_ID =
            "UPDATE AUTHOR SET AUTHOR_NAME = ?, EXPIRED = ? WHERE AUTHOR_ID = ?";
    private static final String SQL_DELETE_BY_ID =
            "DELETE AUTHOR WHERE AUTHOR_ID = ?";
    private static final String SQL_SELECT_BY_ID =
            "SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED FROM AUTHOR WHERE AUTHOR_ID = ?";
    private static final String SQL_SELECT_ALL =
            "SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED FROM AUTHOR" +
            " ORDER BY UPPER(AUTHOR_NAME)";
    private static final String SQL_SELECT_BY_NEWS_ID =
            "SELECT A.AUTHOR_ID, A.AUTHOR_NAME, A.EXPIRED" +
            " FROM AUTHOR A" +
            " INNER JOIN NEWS_AUTHOR NA ON A.AUTHOR_ID = NA.AUTHOR_ID" +
            " WHERE NEWS_ID = ?";
    private static final String SQL_DELETE_NEWS_AUTHOR =
            "DELETE FROM NEWS_AUTHOR WHERE AUTHOR_ID = ?";
    private static final String COLUMN_AUTHOR_ID = "AUTHOR_ID";
    private static final String COLUMN_AUTHOR_NAME = "AUTHOR_NAME";
    private static final String COLUMN_EXPIRED = "EXPIRED";

    private final DataSource dataSource;

    @Inject
    public JdbcAuthorDAO(DataSource dataSource) {
        Assert.notNull(dataSource, "DataSource must be not null!");
        this.dataSource = dataSource;
    }

    /**
     * Persists an author in the database.
     *
     * @param author author to persist.
     * @return id of inserted author.
     * @throws DAOException if database error occurred
     */
    @Override
    public Long insert(Author author) throws DAOException {
        if (author == null) return null;
        Long insertedId = null;
        String[] generated = {COLUMN_AUTHOR_ID};
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps =
                     connection.prepareStatement(SQL_INSERT, generated)) {
            ps.setString(1, author.getName());
            ps.executeUpdate();
            ResultSet generatedKeys = ps.getGeneratedKeys();
            if (generatedKeys.next()) {
                insertedId = generatedKeys.getLong(1);
            } else {
                throw new DAOException("Exception in JdbcAuthorDAO#insert(Author);" +
                        " arguments: [" + author + "];" +
                        " message: No ID generated");
            }
        } catch (SQLException e) {
            throw new DAOException("Exception in JdbcAuthorDAO#insert(Author);" +
                    " arguments: [" + author + "];" +
                    " nested exception: [" + e + "]");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return insertedId;
    }

    /**
     * Updates all fields of an author in database by id.
     *
     * @param author author to update.
     * @throws DAOException if database error occurred
     */
    @Override
    public void update(Author author) throws DAOException {
        if (author == null || author.getId() == null) return;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SQL_UPDATE_BY_ID)) {
            ps.setString(1, author.getName());
            ps.setTimestamp(2, author.getExpired());
            ps.setLong(3, author.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Exception in JdbcAuthorDAO#update(Author);" +
                    " arguments: [" + author + "];" +
                    " nested exception: [" + e + "]");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * Deletes an author by id.
     *
     * @param id id of an author to delete.
     * @throws DAOException if database error occurred
     */
    @Override
    public void delete(Long id) throws DAOException {
        if (id == null) return;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SQL_DELETE_BY_ID)) {
            ps.setLong(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Exception in JdbcAuthorDAO#delete(Long);" +
                    " arguments: [" + id + "];" +
                    " nested exception: [" + e + "]");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * Retrieves all authors from the database.
     *
     * @return {@link List} of authors.
     * @throws DAOException if database error occurred
     */
    @Override
    public List<Author> findAll() throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SQL_SELECT_ALL);
             ResultSet rs = ps.executeQuery()) {
            return fetchAuthorsListFromResultSet(rs);
        } catch (SQLException e) {
            throw new DAOException("Exception in JdbcAuthorDAO#findAll();" +
                    " arguments: [" + "];" +
                    " nested exception: [" + e + "]");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * Retrieves an author with the given id.
     *
     * @param id id of an author to retrieve.
     * @return an author with the given id.
     * @throws DAOException if database error occurred
     */
    @Override
    public Author findById(Long id) throws DAOException {
        if (id == null) return null;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SQL_SELECT_BY_ID)) {
            ps.setLong(1, id);
            ResultSet rs = ps.executeQuery();
            return fetchAuthorFromResultSet(rs);
        } catch (SQLException e) {
            throw new DAOException("Exception in JdbcAuthorDAO#findById(Long);" +
                    " arguments: [" + id + "];" +
                    " nested exception: [" + e + "]");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Author findByNewsId(Long newsId) throws DAOException {
        if (newsId == null) return null;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps =
                     connection.prepareStatement(SQL_SELECT_BY_NEWS_ID)) {
            ps.setLong(1, newsId);
            ResultSet rs = ps.executeQuery();
            return fetchAuthorFromResultSet(rs);
        } catch (SQLException e) {
            throw new DAOException("Exception in JdbcAuthorDAO#findByNewsId(Long);" +
                    " arguments: [" + newsId + "];" +
                    " nested exception: [" + e + "]");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void unlink(Long id) throws DAOException {
        if (id == null) return;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps =
                     connection.prepareStatement(SQL_DELETE_NEWS_AUTHOR)) {
            ps.setLong(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Exception in JdbcAuthorDAO#unlink(Long);" +
                    " arguments: [" + id + "];" +
                    " nested exception: [" + e + "]");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    private Author fetchAuthorFromResultSet(ResultSet rs)
            throws SQLException {
        Author author = null;
        if (rs.next()) {
            author = new Author();
            author.setId(rs.getLong(COLUMN_AUTHOR_ID));
            author.setName(rs.getString(COLUMN_AUTHOR_NAME));
            author.setExpired(rs.getTimestamp(COLUMN_EXPIRED));
        }
        return author;
    }

    private List<Author> fetchAuthorsListFromResultSet(ResultSet rs)
            throws SQLException {
        List<Author> list = new LinkedList<>();
        while (rs.next()) {
            Author author = new Author();
            author.setId(rs.getLong(COLUMN_AUTHOR_ID));
            author.setName(rs.getString(COLUMN_AUTHOR_NAME));
            author.setExpired(rs.getTimestamp(COLUMN_EXPIRED));
            list.add(author);
        }
        return list;
    }
}
