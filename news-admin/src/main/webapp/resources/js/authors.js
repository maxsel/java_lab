var prevName;
var prevId;

function startEditing(id) {
    console.log("startEditing(id=" + id + ")");

    stopEditing(prevId);
    prevName = $("#author" + id).val();
    prevId = id;
    $("#author" + id).removeAttr("disabled");

    $("#edit" + id).hide();
    $("#update" + id).show();
    $("#expire" + id).show();
    $("#cancel" + id).show();

    $("#cancel" + id).click(function() {
        stopEditing(id)
    });
}

function stopEditing(id) {
    console.log("stopEditing(id=" + id + ")");

    $("#author" + id).val(prevName);
    $("#author" + id).attr("disabled", "true");

    $("#edit" + id).show();
    $("#update" + id).hide();
    $("#expire" + id).hide();
    $("#cancel" + id).hide();
}