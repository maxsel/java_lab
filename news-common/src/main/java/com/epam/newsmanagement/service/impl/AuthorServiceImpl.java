package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.dao.DAOException;
import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.inject.Inject;
import java.sql.Timestamp;
import java.util.List;

/**
 * Service with methods
 * encapsulating business logic concerned with {@link Author}s
 * and transaction management.
 */
@Service
public class AuthorServiceImpl implements AuthorService {

    private static final Logger LOG = LogManager.getLogger(AuthorServiceImpl.class);

    private final AuthorDAO authorDAO;

    @Inject
    public AuthorServiceImpl(AuthorDAO authorDAO) {
        Assert.notNull(authorDAO, "AuthorDAO must be not null!");
        this.authorDAO = authorDAO;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long create(Author author) throws ServiceException {
        try {
            return authorDAO.insert(author);
        } catch (DAOException e) {
            LOG.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void update(Author author) throws ServiceException {
        try {
            authorDAO.update(author);
        } catch (DAOException e) {
            LOG.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("Duplicates")
    @Transactional(rollbackFor = ServiceException.class)
    public void delete(Long id) throws ServiceException {
        try {
            authorDAO.unlink(id);
            authorDAO.delete(id);
        } catch (DAOException e) {
            LOG.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Author> findAll() throws ServiceException {
        try {
            return authorDAO.findAll();
        } catch (DAOException e) {
            LOG.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Author findById(Long id) throws ServiceException {
        try {
            return authorDAO.findById(id);
        } catch (DAOException e) {
            LOG.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Author findByNewsId(Long newsId) throws ServiceException {
        try {
            return authorDAO.findByNewsId(newsId);
        } catch (DAOException e) {
            LOG.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean exists(Author author) throws ServiceException {
        return author != null && findById(author.getId()) != null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void expire(Long id, Timestamp time) throws ServiceException {
        try {
            Author author = authorDAO.findById(id);
            author.setExpired(time);
            authorDAO.update(author);
        } catch (DAOException e) {
            LOG.error(e);
            throw new ServiceException(e);
        }
    }
}
