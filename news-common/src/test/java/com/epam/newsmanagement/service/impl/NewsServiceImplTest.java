package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.DAOException;
import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.SearchCriteria;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.ServiceException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings("Duplicates")
public class NewsServiceImplTest {
    @Mock
    private NewsDAO newsDAO;

    private NewsService newsService;

    @Before
    public void init() {
        newsService = new NewsServiceImpl(newsDAO);
    }

    @Test
    public void testCreate() throws Exception {
        News news = new News(1L, "title", "shortText", "fullText", null, null);
        when(newsDAO.insert(news)).thenReturn(news.getId());
        assertEquals(news.getId(), newsService.create(news));
        verify(newsDAO, times(1)).insert(news);
    }

    @Test(expected = ServiceException.class)
    @SuppressWarnings("unchecked")
    public void testCreateWithException() throws Exception {
        News news = new News(1L, "title", "shortText", "fullText", null, null);
        when(newsDAO.insert(news)).thenThrow(DAOException.class);
        newsService.create(news);
        verify(newsDAO, times(1)).insert(news);
        verifyNoMoreInteractions(newsDAO);
    }

    @Test
    public void testUpdate() throws Exception {
        News news = new News();
        newsService.update(news);
        verify(newsDAO, times(1)).update(news);
    }

    @Test(expected = ServiceException.class)
    public void testUpdateWithException() throws Exception {
        News news = new News();
        doThrow(DAOException.class).when(newsDAO).update(news);
        newsService.update(news);
        verify(newsDAO, times(1)).update(news);
        verifyNoMoreInteractions(newsDAO);
    }

    @Test
    public void testDelete() throws Exception {
        newsService.delete(0L);
        verify(newsDAO, times(1)).unlinkNewsAuthor(0L);
        verify(newsDAO, times(1)).unlinkNewsTags(0L);
        verify(newsDAO, times(1)).deleteNewsComments(0L);
        verify(newsDAO, times(1)).delete(0L);
        verifyNoMoreInteractions(newsDAO);
    }

    @Test(expected = ServiceException.class)
    public void testDeleteWithException() throws Exception {
        doThrow(DAOException.class).when(newsDAO).delete(0L);
        newsService.delete(0L);
        verify(newsDAO, times(1)).unlinkNewsAuthor(0L);
        verify(newsDAO, times(1)).unlinkNewsTags(0L);
        verify(newsDAO, times(1)).deleteNewsComments(0L);
        verify(newsDAO, times(1)).delete(0L);
        verifyNoMoreInteractions(newsDAO);
    }

    @Test
    public void testFindById() throws Exception {
        when(newsDAO.findById(0L)).thenReturn(new News());
        newsService.findById(0L);
        verify(newsDAO, times(1)).findById(0L);
        assertEquals(newsService.findById(0L), new News());
    }

    @Test(expected = ServiceException.class)
    public void testFindByIdWithException() throws Exception {
        Long id = 1L;
        doThrow(DAOException.class).when(newsDAO).findById(id);
        newsService.findById(id);
        verify(newsDAO, times(1)).findById(id);
        verifyNoMoreInteractions(newsDAO);
    }

    @Test
    public void testFindByCriteria() throws Exception {
        when(newsDAO.findByCriteria(new SearchCriteria())).thenReturn(new ArrayList<>());
        newsService.findByCriteria(new SearchCriteria());
        verify(newsDAO, times(1)).findByCriteria(new SearchCriteria());
        assertEquals(newsService.findByCriteria(new SearchCriteria()), new ArrayList<News>());
    }

    @Test(expected = ServiceException.class)
    @SuppressWarnings("unchecked")
    public void testFindByCriteriaWithException() throws Exception {
        when(newsDAO.findByCriteria(new SearchCriteria())).thenThrow(DAOException.class);
        newsService.findByCriteria(new SearchCriteria());
        verify(newsDAO, times(1)).findByCriteria(new SearchCriteria());
    }

    @Test
    public void testCount() throws Exception {
        when(newsDAO.count()).thenReturn(6L);
        newsService.count();
        verify(newsDAO, times(1)).count();
        assertTrue(newsService.count() == 6L);
    }


    @Test
    public void testLinkNewsAndAuthor() throws Exception {
        doNothing().when(newsDAO).linkNewsAndAuthor(1L, 1L);
        newsService.linkNewsAndAuthor(1L, 1L);
        verify(newsDAO).linkNewsAndAuthor(1L, 1L);
        verifyNoMoreInteractions(newsDAO);
    }

    @Test
    public void testLinkNewsAndTag() throws Exception {
        doNothing().when(newsDAO).linkNewsAndTag(1L, 1L);
        newsService.linkNewsAndTag(1L, 1L);
        verify(newsDAO).linkNewsAndTag(1L, 1L);
        verifyNoMoreInteractions(newsDAO);
    }
}
