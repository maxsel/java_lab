package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.dao.DAOException;
import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.inject.Inject;
import java.util.List;

/**
 * Service with methods
 * encapsulating business logic concerned with {@link Comment}s
 * and transaction management.
 */
@Service
public class CommentServiceImpl implements CommentService {

    private static final Logger LOG = LogManager.getLogger(CommentServiceImpl.class);

    private final CommentDAO commentDAO;

    @Inject
    public CommentServiceImpl(CommentDAO commentDAO) {
        Assert.notNull(commentDAO, "CommentDAO must be not null!");
        this.commentDAO = commentDAO;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long create(Comment comment) throws ServiceException {
        try {
            return commentDAO.insert(comment);
        } catch (DAOException e) {
            LOG.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void update(Comment comment) throws ServiceException {
        try {
            commentDAO.update(comment);
        } catch (DAOException e) {
            LOG.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(Long id) throws ServiceException {
        try {
            commentDAO.delete(id);
        } catch (DAOException e) {
            LOG.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Comment findById(Long newsId) throws ServiceException {
        try {
            return commentDAO.findById(newsId);
        } catch (DAOException e) {
            LOG.error(e);
            throw new ServiceException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Comment> findByNewsId(Long newsId) throws ServiceException {
        try {
            return commentDAO.findByNewsId(newsId);
        } catch (DAOException e) {
            LOG.error(e);
            throw new ServiceException(e);
        }
    }
}
