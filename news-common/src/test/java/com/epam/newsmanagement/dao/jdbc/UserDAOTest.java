package com.epam.newsmanagement.dao.jdbc;

import com.epam.newsmanagement.dao.DAOException;
import com.epam.newsmanagement.dao.UserDAO;
import com.epam.newsmanagement.domain.User;
import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.annotation.ExpectedDataSet;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.unitils.spring.annotation.SpringBeanByType;

import static org.junit.Assert.assertEquals;

@DataSet("userdao/dataset.xml")
@SpringApplicationContext("test-context.xml")
public class UserDAOTest extends UnitilsJUnit4 {

    @SpringBeanByType
    private UserDAO userDAO;

    @Test
    @ExpectedDataSet("userdao/dataset-expected-insert.xml")
    public void testInsert() throws DAOException {
        User user = new User(4L, "New user", "New login", "New passwd");
        userDAO.insert(user);
    }

    @Test
    @ExpectedDataSet("userdao/dataset-expected-update.xml")
    public void testUpdate() throws DAOException {
        User user = userDAO.findById(1L);
        user.setName("Updated");
        userDAO.update(user);
    }

    @Test
    @ExpectedDataSet("userdao/dataset-expected-delete.xml")
    public void testDelete() throws DAOException {
        userDAO.delete(4L);
    }

    @Test
    public void testFindAll() throws DAOException {
        assertEquals(4, userDAO.findAll().size());
    }

    @Test
    public void testFindById() throws DAOException {
        assertEquals("user1", userDAO.findById(1L).getName());
    }
}
