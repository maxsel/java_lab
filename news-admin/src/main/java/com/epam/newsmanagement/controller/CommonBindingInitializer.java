package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.editor.AuthorEditor;
import com.epam.newsmanagement.editor.CreationDateEditor;
import com.epam.newsmanagement.editor.TagEditor;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.InitBinder;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

@ControllerAdvice
public class CommonBindingInitializer {
    private static final String DATE_FORMAT = "MM/dd/yyyy";

    @InitBinder
    public void registerCustomEditors(WebDataBinder binder/*, WebRequest request*/) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT/*, request.getLocale()*/);
        dateFormat.setLenient(false);
        binder.registerCustomEditor(java.sql.Date.class, null, new CustomDateEditor(dateFormat, true));
        binder.registerCustomEditor(java.util.Date.class, null, new CustomDateEditor(dateFormat, true));
        //binder.registerCustomEditor(java.sql.Timestamp.class, null, new CustomDateEditor(dateFormat, true));
        binder.registerCustomEditor(Timestamp.class, "news.creationDate", new CreationDateEditor());
        binder.registerCustomEditor(Author.class, "author", new AuthorEditor());
        binder.registerCustomEditor(Tag.class, "tags", new TagEditor());
    }
}