package com.epam.newsmanagement.editor;

import com.epam.newsmanagement.domain.Author;

import java.beans.PropertyEditorSupport;

public class AuthorEditor extends PropertyEditorSupport {

    // Converts a String to an Author (when submitting form)
    @Override
    public void setAsText(String text) {
        Author author = new Author();
        author.setId(Long.valueOf(text));
        this.setValue(author);
    }

    // Converts an Author to a String (when displaying form)
    @Override
    public String getAsText() {
        Author author = (Author) this.getValue();
        return author == null ? "" : author.toString();
    }
}