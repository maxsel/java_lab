<%--suppress ALL --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/authors.js" />"></script>

<c:url var="rootUrl" value="/admin/authors" />

<div class="items-container">
    <div class="items-content">
        <div class="items-fields">
            <table cellspacing="50">
                <tbody>
                <jsp:useBean id="now" class="java.util.Date" />
                <c:forEach items="${authors}" var="authorItem">
                    <form:form action="${rootUrl}/update" method="post" modelAttribute="author">
                        <tr>
                            <td> <%--"Author:" message--%>
                                <b><spring:message code="edit_authors.author" />:</b>
                                <form:input path="id" value="${authorItem.id}" type="hidden" />
                                <form:input path="expired" value="${authorItem.expired}" type="hidden" />
                            </td>
                            <td>
                                <form:input path="name" value="${authorItem.name}" id="author${authorItem.id}"
                                            required="required" pattern=".{3,30}"
                                            style="width:400" type="text" disabled="true" />
                                <br/>
                                <form:errors path="name" cssClass="error"/>
                            </td>
                            <td class="edit-item">
                                <%--"edit" link--%>
                                <a href="javascript:startEditing(${authorItem.id})"
                                   id="edit${authorItem.id}"><spring:message code="edit_authors.edit" /></a>
                                <%--"update" button--%>
                                <input id="update${authorItem.id}" name="authorIndex" type="submit" class="submitLink"
                                    value="<spring:message code="edit_authors.update" />" style="display: none" />
                                <%--"expire" link to expire author NOW--%>
                                <c:if test="${!(authorItem.expired lt now)}">
                                    <a href="<c:url value="/admin/authors/${authorItem.id}/expire" />"
                                       id="expire${authorItem.id}" class="link-button"
                                       style="display: none" ><spring:message code="edit_authors.expire" /></a>
                                </c:if>
                                <%--if author already expired, then "(expired)" will be shown--%>
                                <c:if test="${authorItem.expired lt now}">
                                    <i style="color:gray">(<spring:message code="edit_authors.expired" />)</i>
                                </c:if>
                                <%--"cancel" link--%>
                                <a href="javascript:void(0);" id="cancel${authorItem.id}"
                                   style="display: none"><spring:message code="edit_authors.cancel" /></a>
                            </td>
                        </tr>
                    </form:form>
                </c:forEach>
                <tr class="add-item-tr"> <%--"Add author" form--%>
                    <form:form action="${rootUrl}/add" method="post" modelAttribute="newAuthor">
                        <td>
                            <b><spring:message code="edit_authors.add_author" />: </b>
                        </td>
                        <td>
                            <form:input path="name" style="width:400" required="required" pattern=".{3,30}" />
                            <br/>
                            <form:errors path="name" cssClass="error"/>
                        </td>
                        <td class="edit-item">
                            <form:button class="submitLink">
                                <spring:message code="edit_authors.save" />
                            </form:button>
                        </td>
                    </form:form>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>