package com.epam.newsmanagement.command;

import com.epam.newsmanagement.domain.ExtendedNews;
import com.epam.newsmanagement.domain.SearchCriteria;
import com.epam.newsmanagement.service.ExtendedNewsService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class ShowNewsCommand implements Command {
    public String execute(HttpServletRequest request) throws ServiceException {
        ExtendedNewsService extendedNewsService =
                (ExtendedNewsService) context.getBean("extendedNewsService");
        NewsService newsService = (NewsService) context.getBean("newsService");

        HttpSession session = request.getSession(true);

        Long newsId = Long.valueOf(request.getParameter("newsId"));
        ExtendedNews news = extendedNewsService.findById(newsId);

        SearchCriteria searchCriteria = (SearchCriteria) session.getAttribute("searchCriteria");
        if (searchCriteria == null) {
            searchCriteria = new SearchCriteria();
            session.setAttribute("searchCriteria", searchCriteria);
        }
        session.setAttribute("searchCriteria", searchCriteria);
        Long prevNewsId = newsService.findPrev(newsId, searchCriteria);
        Long nextNewsId = newsService.findNext(newsId, searchCriteria);
        if (newsService.findById(prevNewsId) != null) {
            request.setAttribute("prevNewsId", prevNewsId);
        }
        if (newsService.findById(nextNewsId) != null) {
            request.setAttribute("nextNewsId", nextNewsId);
        }

        request.getSession(true).setAttribute("news", news);

        return "/pages/news.jsp";
    }
}
