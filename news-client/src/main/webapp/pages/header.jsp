<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/styles.css" />" />

<fmt:setLocale value="${sessionScope.lang}" scope="session"/>
<fmt:setBundle basename="label"/>

<header style="position: fixed; top:0; width: 100%; background-color: white">
    <table border="1"
           style="width: 98.8%;border-collapse:collapse;border:solid;">
        <tr style="height:3%;border-top:solid;">
            <td>
                <div class="header">
                    <span class="title">
                        <h1>
                            <fmt:message key="header.text"/>
                        </h1>
                    </span>
                    <span class="lang">
                        <a href="<c:url value="/news-client?command=changelanguage&lang=en_US"/>">EN</a>&nbsp;
                        <a href="<c:url value="/news-client?command=changelanguage&lang=ru_RU"/>">RU</a>
                    </span>
                </div>
            </td>
        </tr>
    </table>
</header>