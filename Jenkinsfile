#!groovy

node {
   // Checkout from git
   // Needs previously set credentials (via Jenkins Credentials Plugin) with credentialsId='bitbucket'
   stage 'Checkout'
   git branch: 'task-2', credentialsId: 'bitbucket', url: 'https://bitbucket.org/maxsel/java_lab'

   stage 'Build'
   // Get Maven (implies that Maven installation with Name='maven-3.3.9' is configured)
   def mvnHome = tool 'maven-3.3.9'
   // reset LOGS_HOME environment variable for jenkins to use its own directory
   // and prevent access conflicts
   withEnv(["LOGS_HOME=${env.LOGS_HOME}/jenkins"]) {
      if(isUnix()) {
         echo "Unix-like node"
         sh "${mvnHome}/bin/mvn -Dmaven.test.failure.ignore clean install"
      } else {
         echo "Not Unix-like (possibly Windows) node"
         bat "${mvnHome}\\bin\\mvn -Dmaven.test.failure.ignore clean install"
      }
   }

   stage 'Deploy'
   // Undeploy, and then deploy.
   withEnv(["LOGS_HOME=${env.LOGS_HOME}/jenkins"]) {
      if(isUnix()) {
         sh "${mvnHome}/bin/mvn -Dmaven.test.skip=true tomcat7:undeploy"
         sh "${mvnHome}/bin/mvn -Dmaven.test.skip=true tomcat7:deploy"
      } else {
         bat "${mvnHome}\\bin\\mvn -Dmaven.test.skip=true tomcat7:undeploy"
         bat "${mvnHome}\\bin\\mvn -Dmaven.test.skip=true tomcat7:redeploy"
      }
   }
}