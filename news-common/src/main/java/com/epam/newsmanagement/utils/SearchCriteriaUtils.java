package com.epam.newsmanagement.utils;

import com.epam.newsmanagement.domain.SearchCriteria;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Utility class for creating {@link PreparedStatement} object
 * for the given {@link SearchCriteria} with search parameters.
 */
@SuppressWarnings("Duplicates")
public class SearchCriteriaUtils {

    @SuppressWarnings("unused")
    private final static Logger LOG = LogManager.getLogger(SearchCriteriaUtils.class);

    private final static String CLAUSE_START_QUERY =
            "SELECT NEWS.NEWS_ID, NEWS.SHORT_TEXT, NEWS.FULL_TEXT," +
                    " NEWS.TITLE, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE, " +
                    "COUNT(COMMENTS.COMMENT_ID) COMMENTS_COUNT " +
            " FROM NEWS " +
            "LEFT JOIN COMMENTS ON COMMENTS.NEWS_ID = NEWS.NEWS_ID ";
    private final static String CLAUSE_JOIN_AUTHOR =
            " INNER JOIN NEWS_AUTHOR ON NEWS_AUTHOR.NEWS_ID = NEWS.NEWS_ID ";
    private final static String CLAUSE_JOIN_TAG =
            " INNER JOIN NEWS_TAG ON NEWS_TAG.NEWS_ID = NEWS.NEWS_ID ";
    private final static String CLAUSE_WHERE_AUTHOR =
            " WHERE NEWS_AUTHOR.AUTHOR_ID = ? ";
    private final static String CLAUSE_AND_CLAUSE = " AND ";
    private final static String CLAUSE_WHERE_TAGS = " NEWS_TAG.TAG_ID IN (";
    private final static String CLAUSE_CLOSE_BRACKET = ") ";
    private final static String CLAUSE_GROUP_BY =
            " GROUP BY NEWS.NEWS_ID, NEWS.SHORT_TEXT, NEWS.FULL_TEXT," +
            " NEWS.TITLE, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE ";
    private final static String CLAUSE_HAVING_CLAUSE =
            " HAVING COUNT(DISTINCT NEWS_TAG.TAG_ID) = ? ";
    private final static String CLAUSE_ORDER_BY =
            " ORDER BY COUNT(COMMENTS.COMMENT_ID) DESC, NEWS.MODIFICATION_DATE DESC";
    private static final String CLAUSE_PAGINATION_ENCLOSING_START =
            "SELECT UNPAGED.NEWS_ID, UNPAGED.SHORT_TEXT, UNPAGED.FULL_TEXT," +
            " UNPAGED.TITLE, UNPAGED.CREATION_DATE, UNPAGED.MODIFICATION_DATE FROM ( " +
            "SELECT UNNUMBERED.NEWS_ID, UNNUMBERED.SHORT_TEXT, UNNUMBERED.FULL_TEXT," +
            " UNNUMBERED.TITLE, UNNUMBERED.CREATION_DATE, UNNUMBERED.MODIFICATION_DATE, ROWNUM NUM FROM ( ";
    private static final String CLAUSE_PAGINATION_ENCLOSING_END =
            ") UNNUMBERED " +
            ") UNPAGED " +
            "WHERE UNPAGED.NUM BETWEEN ? AND ?";
    private static final String CLAUSE_COUNT_ENCLOSING_START = "SELECT COUNT(*) FROM ( ";
    private static final String CLAUSE_COUNT_ENCLOSING_END = " ) ALL_NEWS";
    private static final String CLAUSE_NEXTPREV_ENCLOSING_START =
            "SELECT WITH_NEIGHBORS.PREV, WITH_NEIGHBORS.CURR, WITH_NEIGHBORS.NEXT " +
            "FROM ( " +
            "SELECT LAG(FILTERED.NEWS_ID) OVER(ORDER BY FILTERED.COMMENTS_COUNT DESC) PREV, " +
            "       FILTERED.NEWS_ID CURR, " +
            "       LEAD(FILTERED.NEWS_ID) OVER(ORDER BY FILTERED.COMMENTS_COUNT DESC) NEXT " +
            "FROM (";
    private static final String CLAUSE_NEXTPREV_ENCLOSING_END =
            ") FILTERED " +
            "ORDER BY FILTERED.COMMENTS_COUNT DESC " +
            ") WITH_NEIGHBORS " +
            "WHERE WITH_NEIGHBORS.CURR = ?";

    private static boolean searchByAuthor(SearchCriteria criteria) {
        return criteria != null && criteria.getAuthorId() != null
                && criteria.getAuthorId() != null && criteria.getAuthorId() != 0;
    }

    private static boolean searchByTags(SearchCriteria criteria) {
        return criteria != null && criteria.getTagsId() != null
                && !criteria.getTagsId().isEmpty();
    }

    private static String buildNewsQuery(SearchCriteria criteria) {
        boolean searchByAuthor = searchByAuthor(criteria);
        boolean searchByTags = searchByTags(criteria);
        StringBuilder sb = new StringBuilder(CLAUSE_START_QUERY);
        if (searchByAuthor) {
            sb.append(CLAUSE_JOIN_AUTHOR);
        }
        if (searchByTags) {
            sb.append(CLAUSE_JOIN_TAG);
        }
        if (searchByAuthor) {
            sb.append(CLAUSE_WHERE_AUTHOR);
        }
        if (searchByTags) {
            sb.append(CLAUSE_AND_CLAUSE);
            List<Long> tagsId = criteria.getTagsId();
            sb.append(CLAUSE_WHERE_TAGS);
            String idsMapped = tagsId.stream().map(t -> "?").collect(Collectors.joining(", "));
            sb.append(idsMapped);
            sb.append(CLAUSE_CLOSE_BRACKET);
        }
        sb.append(CLAUSE_GROUP_BY);
        if (searchByTags) {
            sb.append(CLAUSE_HAVING_CLAUSE);
        }
        sb.append(CLAUSE_ORDER_BY);
        return sb.toString();
    }

    private static String buildNewsQueryWithPagination(SearchCriteria criteria) {
        return CLAUSE_PAGINATION_ENCLOSING_START +
                buildNewsQuery(criteria) +
                CLAUSE_PAGINATION_ENCLOSING_END;
    }

    private static String buildCountQuery(SearchCriteria criteria) {
        return CLAUSE_COUNT_ENCLOSING_START +
                buildNewsQuery(criteria) +
                CLAUSE_COUNT_ENCLOSING_END;
    }

    private static String buildNextPrevQuery(SearchCriteria criteria) {
        return CLAUSE_NEXTPREV_ENCLOSING_START +
                buildNewsQuery(criteria) +
                CLAUSE_NEXTPREV_ENCLOSING_END;
    }


    /**
     * Creates {@link PreparedStatement} object
     * for the given {@link SearchCriteria}.
     *
     * @param connection connection to database
     * @param criteria   {@link SearchCriteria} object, encapsulating
     *                   parameters of search - author and list of tags.
     * @return {@link PreparedStatement} object for the given {@link SearchCriteria}
     * @throws SQLException if database error occurred
     */
    public static PreparedStatement prepareStatement(Connection connection,
                                                     SearchCriteria criteria)
            throws SQLException {
        String query = buildNewsQuery(criteria);

//        LOG.debug("SearchCriteria: " + criteria + ". Pagination is off. Query: \n" + query + "\n");

        PreparedStatement ps = connection.prepareStatement(query);
        boolean searchByAuthor = searchByAuthor(criteria);
        boolean searchByTags = searchByTags(criteria);
        int index = 1;
        if (searchByAuthor) {
            ps.setLong(index, criteria.getAuthorId());
            index++;
        }
        if (searchByTags) {
            List<Long> tagsId = criteria.getTagsId();
            for (int i = 0; i < tagsId.size(); i++) {
                ps.setLong(index + i, tagsId.get(i));
            }
            index += tagsId.size();
            ps.setLong(index, tagsId.size());
        }
        return ps;
    }

    /**
     * Creates {@link PreparedStatement} object
     * for the given {@link SearchCriteria}.
     * Supports pagination.
     *
     * @param connection connection to database
     * @param criteria   {@link SearchCriteria} object, encapsulating
     *                   parameters of search - author and list of tags.
     * @param offset start pagination index
     * @param count size of page
     * @return {@link PreparedStatement} object for the given {@link SearchCriteria}
     * @throws SQLException if database error occurred
     */
    public static PreparedStatement prepareStatement(Connection connection,
                                                     SearchCriteria criteria,
                                                     int offset, int count)
            throws SQLException {
        String query = buildNewsQueryWithPagination(criteria);

//        LOG.debug("SearchCriteria: " + criteria + ". Pagination is on. Query: \n" + query + "\n");

        PreparedStatement ps = connection.prepareStatement(query);
        boolean searchByAuthor = searchByAuthor(criteria);
        boolean searchByTags = searchByTags(criteria);
        int index = 1;
        if (searchByAuthor) {
            ps.setLong(index, criteria.getAuthorId());
            index++;
        }
        if (searchByTags) {
            List<Long> tagsId = criteria.getTagsId();
            for (int i = 0; i < tagsId.size(); i++) {
                ps.setLong(index + i, tagsId.get(i));
            }
            index += tagsId.size();
            ps.setLong(index++, tagsId.size());
        }
        ps.setLong(index++, offset);
        ps.setLong(index, offset + count - 1);
        return ps;
    }

    /**
     * Creates {@link PreparedStatement} object for the given {@link SearchCriteria}.
     * Result set from executing the statement will contain only one column.
     *
     * @param connection connection to database
     * @param criteria   {@link SearchCriteria} object, encapsulating
     *                   parameters of search - author and list of tags.
     * @return {@link PreparedStatement} object for the given {@link SearchCriteria}
     * @throws SQLException if database error occurred
     */
    public static PreparedStatement prepareStatementForCount(Connection connection,
                                                             SearchCriteria criteria)
            throws SQLException {
        String query = buildCountQuery(criteria);

//        LOG.debug("SearchCriteria: " + criteria + ". Query for count: \n" + query + "\n");

        PreparedStatement ps = connection.prepareStatement(query);
        boolean searchByAuthor = searchByAuthor(criteria);
        boolean searchByTags = searchByTags(criteria);
        int index = 1;
        if (searchByAuthor) {
            ps.setLong(index, criteria.getAuthorId());
            index++;
        }
        if (searchByTags) {
            List<Long> tagsId = criteria.getTagsId();
            for (int i = 0; i < tagsId.size(); i++) {
                ps.setLong(index + i, tagsId.get(i));
            }
            index += tagsId.size();
            ps.setLong(index/*++*/, tagsId.size());
        }
        return ps;
    }

    /**
     * Creates {@link PreparedStatement} object for the given {@link SearchCriteria}.
     * Result set from executing the statement will contain columns PREV, CURR, NEXT.
     *
     * @param connection connection to database
     * @param criteria   {@link SearchCriteria} object, encapsulating
     *                   parameters of search - author and list of tags.
     * @param id id of news to find predecessor and successor for.
     * @return {@link PreparedStatement} object for the given {@link SearchCriteria}
     * @throws SQLException if database error occurred
     */
    public static PreparedStatement prepareStatementForNextPrev(Connection connection,
                                                                SearchCriteria criteria,
                                                                long id)
            throws SQLException {
        String query = buildNextPrevQuery(criteria);

//        LOG.debug("SearchCriteria: " + criteria + ". Query for Next/Prev: \n" + query + "\n");

        PreparedStatement ps = connection.prepareStatement(query);
        boolean searchByAuthor = searchByAuthor(criteria);
        boolean searchByTags = searchByTags(criteria);
        int index = 1;
        if (searchByAuthor) {
            ps.setLong(index, criteria.getAuthorId());
            index++;
        }
        if (searchByTags) {
            List<Long> tagsId = criteria.getTagsId();
            for (int i = 0; i < tagsId.size(); i++) {
                ps.setLong(index + i, tagsId.get(i));
            }
            index += tagsId.size();
            ps.setLong(index++, tagsId.size());
        }
        ps.setLong(index, id);
        return ps;
    }

}
