package com.epam.newsmanagement.dao.jdbc;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.dao.DAOException;
import com.epam.newsmanagement.domain.Comment;
import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.annotation.ExpectedDataSet;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.unitils.spring.annotation.SpringBeanByType;

import java.sql.Timestamp;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@DataSet("commentdao/dataset.xml")
@SpringApplicationContext("test-context.xml")
public class CommentDAOTest extends UnitilsJUnit4 {

    @SpringBeanByType
    private CommentDAO commentDAO;

    @Test
    @ExpectedDataSet("commentdao/dataset-expected-insert.xml")
    public void testInsert() throws DAOException {
        Comment comment =
                new Comment(0L, 1L, "New comment",
                        Timestamp.valueOf("2016-06-20 18:48:05.123456"));
        commentDAO.insert(comment);
    }

    @Test
    @ExpectedDataSet("commentdao/dataset-expected-update.xml")
    public void testUpdate() throws DAOException {
        Comment comment = commentDAO.findById(1L);
        comment.setText("Updated");
        commentDAO.update(comment);
    }

    @Test
    @ExpectedDataSet("commentdao/dataset-expected-delete.xml")
    public void testDelete() throws DAOException {
        commentDAO.delete(1L);
    }

    @Test
    public void testFindAll() throws DAOException {
        assertEquals(3, commentDAO.findAll().size());
    }

    @Test
    public void testFindById() throws DAOException {
        assertEquals("Comment 1", commentDAO.findById(1L).getText());
    }

    @Test
    public void testFindByNewsId() throws DAOException {
        assertTrue(2L == commentDAO.findByNewsId(1L).size());
    }
}
