package com.epam.newsmanagement.dao.jdbc;

import com.epam.newsmanagement.dao.DAOException;
import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.domain.Tag;
import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.annotation.ExpectedDataSet;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.unitils.spring.annotation.SpringBeanByType;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@DataSet("tagdao/dataset.xml")
@SpringApplicationContext("test-context.xml")
public class TagDAOTest extends UnitilsJUnit4 {

    @SpringBeanByType
    private TagDAO tagDAO;

    @Test
    @ExpectedDataSet("tagdao/dataset-expected-insert.xml")
    public void testInsert() throws DAOException {
        Tag tag = new Tag(0L, "java");
        tagDAO.insert(tag);
        //assertTrue(tagDAO.insert(tag) > 0);
    }

    @Test
    @ExpectedDataSet("tagdao/dataset-expected-insertAll.xml")
    public void testInsertAll() throws DAOException {
        List<Tag> tags = Arrays.asList(new Tag(0L, "one"), new Tag(0L, "two"));
        tagDAO.insertAll(tags);
    }

    @Test
    @ExpectedDataSet("tagdao/dataset-expected-update.xml")
    public void testUpdate() throws DAOException {
        Tag tag = tagDAO.findById(1L);
        tag.setName("New tag");
        tagDAO.update(tag);
    }

    @Test
    @ExpectedDataSet("tagdao/dataset-expected-delete1.xml")
    public void testDelete() throws DAOException {
        tagDAO.delete(5L);
    }

    @Test
    public void testFindAll() throws DAOException {
        assertEquals(5, tagDAO.findAll().size());
    }

    @Test
    @ExpectedDataSet("tagdao/dataset-expected-delete2.xml")
    public void testDeleteWithUnlinking() throws DAOException {
        tagDAO.unlink(1L);
        tagDAO.delete(1L);
    }

    @Test(expected = DAOException.class)
    public void testDeleteWithConstraintViolation() throws DAOException {
        tagDAO.delete(1L);
    }

    @Test
    public void testFindById() throws DAOException {
        assertEquals("politics", tagDAO.findById(1L).getName());
    }

    @Test
    public void testFindByNewsId() throws DAOException {
        assertTrue(2L == tagDAO.findByNewsId(1L).size());
    }

    @Test
    public void testFindByName() throws DAOException {
        assertTrue(1L == tagDAO.findByName("politics").getId());
    }


}
