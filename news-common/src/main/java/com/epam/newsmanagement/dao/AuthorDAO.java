package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.domain.Author;

/**
 * Data Access Object interface with methods
 * for accessing {@link Author}s in the database.
 */
public interface AuthorDAO extends GenericDAO<Long, Author> {
    /**
     * Returns the {@link Author} of the {@link com.epam.newsmanagement.domain.News}
     * with the given id.
     *
     * @param newsId id of news.
     * @return author of the news with the given id.
     * @throws DAOException if database error occurred
     */
    Author findByNewsId(Long newsId) throws DAOException;

    /**
     * Removes all relations with the given author's id.
     *
     * @param id id of author.
     * @throws DAOException if database error occurred
     */
    void unlink(Long id) throws DAOException;
}
